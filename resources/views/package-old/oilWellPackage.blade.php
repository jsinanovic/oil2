@extends('layouts.blank')

@push('stylesheets')
        <!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- bootstrap-progressbar -->
<link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<!-- jVectorMap -->
<link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="nav toggle" style="width: 100% !important;  height: 60px;" >
        <span style="color: #73879C; font-size: 18px; margin-top: 50%; margin-left: 20px;">Oil wells</span>
        <table class="table table-responsive table-inverse">
            <thead>
            <td>Name</td>
            <td>TankID</td>
            <td>Active</td>
            <td>Add New</td>
            <td>Production Today</td>
            <td>+/- Yesterday/Today</td>
            <td>EDIT</td>
            <td>DELETE</td>
            </thead>
            <tbody>
            @foreach ($oilwells as $oilwell)
                <tr class="bg-info">
                    <td>{{$oilwell->name}}</td>
                    <td>{{$oilwell->tankId}}</td>
                    <td>{{$oilwell->active ? 'Yes' : 'No'}}</td>
                    <td><a style="color: Green !important;" href="/openoilwells">Add Oil wells</a> </td>
                    <td>{{ 2 * 2 }}</td>
                    <td>{{ 50 / 4 }} %</td>

                    <td><a style="color: Blue !important;" href="/removefrompackage/{{$oilwell->id}}">Remove Oil Wells from package</a> </td>
                    <td><a style="color: Blue !important;" href="/openOilwellTanks/{{$oilwell->id}}">See Oil Well Tanks</a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="ui link cards">
            @foreach ($oilwells as $oilwell)
                <div class="card">
                    <div class="image">
                        <img src="/images/noimage.png">
                    </div>
                    <div class="content">
                        <div class="header">{{$oilwell->name}}</div>
                        <div class="meta">
                            <a></a>
                        </div>
                        <div class="description">
                            Matthew is an interior designer living in New York.
                        </div>
                    </div>
                    <div class="extra content">
      <span class="right floated">
        Created at {{ $oilwell->created_at }}
      </span>
      <span>
        <i class="user icon"></i>
        <a  style="color: blue !important;" href="/openOilwellTanks/{{$oilwell->id}}">EDIT</a>
      </span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>



</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection

