@extends('layouts.blank')

@push('stylesheets')
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="nav toggle" style="width: 100% !important;  height: 60px;" >
        <span style="color: #73879C; font-size: 18px; margin-top: 50%; margin-left: 20px;">Your Packages</span>
    </div>
    <table class="table table-responsive table-inverse">
        <thead>
        <td>Name</td>
        <td>Open</td>
        </thead>
        <tbody>
        @foreach ($packages as $package)
            <tr class="bg-info">
                <td>{{$package->name}}</td>
                <td><a style="color: Blue !important;" href="/openPackage/{{$package->id}}">EDIT</a> </td>
            </tr>
        @endforeach

        {{--<select class="chosen-select" multiple="multiple" name="places[]" id="places">--}}
        {{--@foreach($oilWells as $oilWell)--}}
        {{--<option value="{{$oilWell->id}}">{{$oilWell->name}}</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}

        </tbody>
    </table>

    <div class="ui link cards">
        @foreach ($packages as $package)
            <div class="card">
                <div class="image">
                    <img src="/images/noimage.png">
                </div>
                <div class="content">
                    <div class="header">{{$package->name}}</div>
                    <div class="meta">
                        <a></a>
                    </div>
                    <div class="description">
                        Matthew is an interior designer living in New York.
                    </div>
                </div>
                <div class="extra content">
      <span class="right floated">
        Created at {{ $package->created_at }}
      </span>
      <span>
        <i class="user icon"></i>
        <a  style="color: blue !important;" href="/openPackage/{{$package->id}}">EDIT</a>
      </span>
                </div>
            </div>
        @endforeach
    </div>

    <section class="login_content" style="width: 40% !important;  float: left">
        <form class="form-horizontal" role="form" id="addPackageForm" method="POST">
            {{--<form method="post" action="{{ url('/api/v1/packages') }}">--}}
            {!! csrf_field() !!}

            <h1>Add Package</h1>


            <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Package Name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>

                @if ($errors->has('name'))
                    <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('oilWellId') ? ' has-error' : '' }}">
               <select class="chosen-select" multiple="multiple" name="oilWellId[]" id="oilWellId">
                 @foreach($oilWells as $oilWell)
                     <option value="{{$oilWell->id}}">{{$oilWell->name}}</option>
                 @endforeach
                </select>

                @if ($errors->has('oilWellId'))
                    <span class="help-block">
                  <strong>{{ $errors->first('oilWellId') }}</strong>
                </span>
                @endif
            </div>


            <div>
                <button id="btnInputData" class="btn btn-default submit" >Add Package Data</button>
            </div>
            <div id="info" style="text-align: center;"></div>
            <div class="clearfix"></div>
        </form>
    </section>



</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection