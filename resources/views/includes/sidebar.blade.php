<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><i class="fa fa-paw"></i> <span>Something</span></a>
        </div>
        
        <div class="clearfix"></div>
        
        <!-- menu profile quick info -->
        {{--<div class="profile">--}}
            {{--<div class="profile_pic">--}}
                {{--<img src="{{ Gravatar::src(Auth::user()->email) }}" alt="Avatar of {{ Auth::user()->name }}" class="img-circle profile_img">--}}
            {{--</div>--}}
            {{--<div class="profile_info">--}}
                {{--<span>Welcome,</span>--}}
                {{--<h2>{{ Auth::user()->name }}</h2>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- /menu profile quick info -->
        
        <br />
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="/dashboard">
                            <i class="fa fa-tachometer"></i>
                            {{--Oil wells--}}

                        </a>
                    </li>
                    <li>
                        <a href="/packages">
                            <i class="fa fa-user"></i>
                            {{--Oil wells--}}

                        </a>
                    </li>
                    <li>
                        <a href="/oil-wells">
                            <i class="fa fa-calculator"></i>
                            {{--Oil wells--}}

                        </a>
                    </li>
                    <li>
                        <a href="/input-data">
                            <i class="fa fa-pencil"></i>
                            {{--Input data--}}

                        </a>
                    </li>
                    <li>
                        <a href="/user-managment">
                            <i class="fa fa-user"></i>
                            {{--User--}}

                        </a>
                    </li>
                    <li>
                        <a href="/reporting">
                            <i class="fa fa-file-text-o"></i>
                            {{--reporting--}}
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    <li>
                        <a href="/comperative">
                            <i class="fa fa-files-o"></i>
                            {{--reporting--}}
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-line-chart"></i>
                            {{--reporting--}}
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-cog"></i>
                            {{--reporting--}}
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                    <li>
                        <a href="/help">
                            <i class="fa fa-life-ring"></i>
                            {{--reporting--}}
                            {{--<span class="label label-success pull-right">Flag</span>--}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
        
        <!-- /menu footer buttons -->
        {{--<div class="sidebar-footer hidden-small">--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="Settings">--}}
                {{--<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>--}}
            {{--</a>--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="FullScreen">--}}
                {{--<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>--}}
            {{--</a>--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="Lock">--}}
                {{--<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>--}}
            {{--</a>--}}
            {{--<a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}">--}}
                {{--<span class="glyphicon glyphicon-off" aria-hidden="true"></span>--}}
            {{--</a>--}}
        {{--</div>--}}
        <!-- /menu footer buttons -->
    </div>
</div>