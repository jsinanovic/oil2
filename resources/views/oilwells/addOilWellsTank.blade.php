@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <header><div class="arrow-left"></div> Add new oil well</header>
    <div id="section">
        <div class="add-new">
            {{--<form method="post" enctype="multipart/form-data" action="{{ url('/api/v1/addoilwells') }}">--}}
            <form class="form-horizontal" enctype="multipart/form-data" id="addNewTank" role="form" method="POST">

                {!! csrf_field() !!}

                <div class="form-row" style="display:  none;">
                    <p>OilWllId</p>
                    <input type="text" name="oilWellid" value="{{ $oilWellid }}" required>
                </div>

                <div class="form-row">
                    <p>Tank Id (Name)</p>
                    <input type="text" name="tankId" placeholder="Tank ID" required>
                </div>
                <!-- .form-row -->
                <!-- .form-row -->
                <div class="form-row">
                    <input type="text" name="active" placeholder="Active" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <input type="submit" value="Submit" class="btn">
                </div>
                <!-- .form-row -->
                <div id="info" style="text-align: center;"></div>
                <div class="clearfix"></div>
            </form>
        </div>
        <!-- .add-new -->
    </div>
    <!-- #section -->
@endsection