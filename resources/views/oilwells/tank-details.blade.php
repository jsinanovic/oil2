@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <header><div class="arrow-left"></div> Tank {{ $tank[0]['tankId'] }}<a style="margin-left: 100px;" href="#">Daily</a> | <a href="#">Monthly</a> | <a href="#">Yearly</a> <a class="btn col-right" href="#">Package Wells List</a></header>
    {{--<div id="section">--}}
        {{--<div class="active-wells">--}}
        {{--</div>--}}
        {{--<!-- .active-wells -->--}}
        {{--<div class="numbers">--}}
            {{--<ul class="none col-left">--}}
                {{--<img src="{{ asset("images/act-img.png") }}" alt="act-wells" style="width: 300px; height: auto; margin-right: 20px; margin-bottom: 20px;">--}}
                {{--<li>6/8 <span class="act">Active Oil Wells</span></li>--}}
            {{--</ul>--}}
            {{--<ul class="none col-left">--}}
                {{--<li>Oil</li>--}}
                {{--<li>Gas</li>--}}
            {{--</ul>--}}
            {{--<ul class="none col-left">--}}
                {{--<li>526 <span class="msr">bbl</span> <span class="prod">Oil production</span></li>--}}
                {{--<li>201 <span class="msr">Mcf</span> <span class="prod">Gas production</span></li>--}}
            {{--</ul>--}}
            {{--<ul class="none col-left">--}}
                {{--<li>526 <span class="msr">bbl</span> <span class="prod">Oil production</span></li>--}}
                {{--<li>201 <span class="msr">Mcf</span> <span class="prod">Gas production</span></li>--}}
            {{--</ul>--}}
            {{--<ul class="none col-left">--}}
                {{--<li>526 <span class="msr">bbl</span> <span class="prod">Oil production</span></li>--}}
                {{--<li>201 <span class="msr">Mcf</span> <span class="prod">Gas production</span></li>--}}
            {{--</ul>--}}
            {{--<ul class="none col-left">--}}
                {{--<li>526 <span class="msr">bbl</span> <span class="prod">Oil production</span></li>--}}
                {{--<li>201 <span class="msr">Mcf</span> <span class="prod">Gas production</span></li>--}}
            {{--</ul>--}}
            {{--<ul class="none col-left">--}}
                {{--<li>526 <span class="msr">bbl</span> <span class="prod">Oil production</span></li>--}}
                {{--<li>201 <span class="msr">Mcf</span> <span class="prod">Gas production</span></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        {{--<!-- .numbers -->--}}
        {{--<div class="charts col-left">--}}
            {{--<div class="wrapper">--}}
                {{--<h1>Gas Production</h1>--}}
                {{--<canvas id='c'></canvas>--}}
                {{--<div class="label">text</div>--}}
            {{--</div>--}}
            {{--<!-- .wrapper -->--}}
            {{--<p>Please mouse over the dots</p>--}}
        {{--</div>--}}
        {{--<!-- .charts -->--}}
        {{--<div class="charts col-left">--}}
            {{--<div class="wrapper">--}}
                {{--<h1>Oil Production</h1>--}}
                {{--<canvas id='c'></canvas>--}}
                {{--<div class="label">text</div>--}}
            {{--</div>--}}
            {{--<!-- .wrapper -->--}}
            {{--<p>Please mouse over the dots</p>--}}
        {{--</div>--}}
        {{--<!-- .charts -->--}}
    {{--</div>--}}
    <!-- #section -->
@endsection