@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Add New Oil Well</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-md-4">
        {{--<form method="post" enctype="multipart/form-data" action="{{ url('/api/v1/addoilwells') }}">--}}
         <form class="form-horizontal" enctype="multipart/form-data" id="addOilWellsForm" role="form" method="POST" >

                {!! csrf_field() !!}
            <div class="form-row">
                <p>Oil Well</p>
                <input type="text" value="" name="name" required>
            </div>
            <!-- .form-row -->
            <div class="form-row">
                <p>Country</p>
                <select name="country" required>
                    <option value="251">USA</option>
                    {{--@foreach ($countryes as $country)--}}
                        {{--<option value="{{$country->id}}">{{$country->countryName}}</option>--}}
                    {{--@endforeach--}}
                </select>
            </div>
            <!-- .form-row -->
            <div class="form-row">
                <p>State</p>
                <select name="state" required>
                    <option value="">Select state</option>
                    @foreach ($states as $state)
                        <option value="{{$state->name}}">{{$state->name}}</option>
                    @endforeach
                </select>
            </div>
            <!-- .form-row -->
            <div class="form-row">
                <p>Add Tanks inside this oil well</p>
                <button class="add_field_button">Add More Tanks</button>
             <div class="input_fields_wrap">
                 <div>
                     <input placeholder="Tank ID" type="text" name="oilWellTankId[]" required>
                 </div>
                <br/>
                 <select  class="form-control" name="oilWellTankActive[]" required>
                     <option value="">Select is tank id active!</option>
                     <option value="1">Yes</option>
                     <option value="0">No</option>
                 </select>

                 {{--<div><input placeholder="Is Active" type="text" name="oilWellTankActive[]"></div>
                 <br/>
                 <button class="add_field_button">Add More Tanks</button>
                 {{--<select name="WellId[]">--}}
                     {{--@foreach($oilwells as $oilWell)--}}
                         {{--<option value="{{$oilWell->id}}">{{$oilWell->name}}</option>--}}
                     {{--@endforeach--}}
                 {{--</select>--}}
             </div>
            </div>

            <!-- .form-row -->
            <div class="form-row">
                <p>Is Oil Well Active</p>
                <select class="form-control" name="active" required>
                    <option value="">Select is oil well active!</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>

            </div>
            <!-- .form-row -->
            <div class="form-row">
                <p>Upload Image</p>
                <input type="file" name="image" class="form-control" accept="image/*" required>
            </div>
            <!-- .form-row -->
            <div class="form-row">
                <input type="submit" value="Submit" class="btn">
            </div>
            <!-- .form-row -->
        </form>
    </div>
    <!-- .col-md-4 -->
  </div>
  <!-- .panel-inner -->



@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div> <div><input type="text" placeholder="Tank ID" name="oilWellTankId[]"></div>' +
                    '<select  class="form-control" name="oilWellTankActive[]" required><option value="">Select is tank id active!</option><option value="1">Yes</option><option value="0">No</option> </select><a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
