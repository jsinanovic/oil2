@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <header><div class="arrow-left"></div> Update oil well</header>
    <div id="section">
        <div class="add-new">
            {{--<form method="post" enctype="multipart/form-data" action="{{ url('/api/v1/addoilwells') }}">--}}
            <form class="form-horizontal" enctype="multipart/form-data" id="updateOilWellsForm" role="form" method="POST" >

                {!! csrf_field() !!}

                <div class="form-row" style="display: none">
                    <p>Oil well</p>
                    <input type="text" name="id" value="{{ $oilwell->id }}" >
                </div>

                <div class="form-row">
                    <p>Oil well</p>
                    <input type="text" name="name" value="{{ $oilwell->name }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Country</p>
                    <select class="form-control" name="country" >

                        @foreach ($countryes as $country)
                            <option value="{{$country->id}}" selected>{{$country->countryName}}</option>
                        @endforeach
                    </select>

                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>State</p>
                    <select class="form-control" name="state" >
                        @foreach ($states as $state)
                            <option value="{{$state->id}}" selected>{{$state->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input_fields_wrap">
                    <button class="add_field_button">Add More Tanks</button>
                    <div><input type="text" name="oilWellTankId[]"></div>
                    <div><input type="text" name="oilWellTankActive[]"></div>
                    {{--<select name="WellId[]">--}}
                    {{--@foreach($oilWells as $oilWell)--}}
                    {{--<option value="{{$oilWell->id}}">{{$oilWell->name}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                </div>

                <!-- .form-row -->
                {{--<div class="form-row">--}}
                    {{--<p>Tank ID</p>--}}
                    {{--<input type="text" name="tankid">--}}
                {{--</div>--}}
                <!-- .form-row -->
                <div class="form-row">
                    <p>Upload Image</p>
                    <input type="file" name="image" class="form-control" id="image" value="{{ $oilwell->image }}">
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Active</p>
                    <input type="text" name="active" placeholder="Active" value="{{ $oilwell->active }}">
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <input  id="btnRequest" type="submit" value="Update" class="btn">
                </div>
                <!-- .form-row -->
                <div id="info" style="text-align: center;"></div>
                <div class="clearfix"></div>
            </form>
        </div>
        <!-- .add-new -->
    </div>
    <!-- #section -->
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div> <div><input type="text" name="oilWellTankId[]"></div><div><input type="text" name="oilWellTankActive[]"></div><a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>