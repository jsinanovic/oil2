@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <div class="heading">
        <div class="arrow-left"></div>
        <h1 class="pull-left">
            Oil Well | {{ $oilwells[0]->name }}
        </h1>
        <input id="wellId"  name="wellId" value="{{$oilwells[0]->id}}" style="display: none;">
        <select id="oilWellSelect" name="oilWellSelect" class="select-header pull-left">
            <option value="">Filter By</option>
            <option value="1">Monthly</option>
            <option value="2">Year</option>
        </select>
    </div>

    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12 border-top">
        <div class="col-lg-2 act-img-wrapper">
            <div class="act-img">
                @if( isset($oilwells[0]->image) && $oilwells[0]->image != '' )
                    <img src="{{ asset("images/oilwells/".  $oilwells[0]->image) }}" alt="active-img">
                @else
                    <img src="{{asset("images/well.png")}}" alt="active-img">
                @endif


            </div>
            <!-- .active-img -->
            <div class="red-line pull-left" style="margin-top: 20px;">
                <h2 class="blue text-uppercase"><strong>{{ $oilwells[0]->active ? 'Active' : 'Inactive' }}</strong></h2>
            </div>
            <!-- .red-line -->
            <div class="red-line pull-right" style="margin-top: 20px;">
                <h2 style="margin: 0;">{{ $tankNumber }}/<span class="gray">{{ $tankNumber }}</span><br> <small>Tanks</small></h2>
            </div>
            <!-- .red-line -->
        </div>
        <!-- .col-lg-2 -->
        <div class="col-lg-10">
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <h1 class="red text-uppercase"><img style="width: 40px; vertical-align: middle;" src="{{asset("images/oil-img.png")}}" alt="oil-img"> Oil</h1>
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldata[0]->oliProduced }} <span>bbl</span></h1>
                        <p>Oil production</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldata[0]->oliProduced * 1.08}}</h1>
                        <p>Oil production ($)</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldata[0]->oilLvlFT }}</h1>
                        <p>Oil level in tanks</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldata[0]->oilLvlFT * 1.08 }}</h1>
                        <p>Oil level in tanks ($)</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
            </div>
            <!-- .col-lg-12 -->
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <h1 class="red text-uppercase"><img style="width: 40px; vertical-align: middle;" src="{{asset("images/gas-img.png")}}" alt="gas-img"> Gas</h1>
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldata[0]->gasProduced }} <span>bbl</span></h1>
                        <p>Gas production</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldata[0]->gasProduced * 1.08 }}</h1>
                        <p>Gas production  ($)</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldata[0]->gaslvl }}</h1>
                        <p>Gas level in tanks</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldata[0]->gaslvl *1.08 }}</h1>
                        <p>Gas level in tanks ($)</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
            </div>
            <!-- .col-lg-12 -->
        </div>
        <!-- .col-lg-10 -->
    </div>
    <!-- .col-lg-12 -->
    <div class="col-lg-12" style="padding: 40px 0;">
        <div class="col-lg-6">
            <header class="sub">
                <div class="col-xs-6">
                    <h2>Oil Production</h2>
                </div>
                <!-- .col-xs-6 -->
                <div class="col-xs-6">
                    <select>
                        <option value="barrels">Barrels</option>
                    </select>
                    {{--<select>--}}
                       {{--<option value="monthly">Monthly</option>--}}
                    {{--</select>--}}

                </div>
                <!-- .col-xs-6 -->
            </header>
            <div class="chart">
                <div class="owp-chart-11"></div>
            </div>
            <!-- .chart -->
        </div>
        <!-- .col-lg-6 -->
        <div class="col-lg-6">
            <header>
                <div class="col-xs-6">
                    <h2>Gas Production</h2>
                </div>
                <!-- .col-xs-6 -->
                <div class="col-xs-6">
                    <select>
                        <option value="Mfc">Mfc</option>
                    </select>
                    {{--<select>--}}
                       {{--<option value="monthly">Monthly</option>--}}
                    {{--</select>--}}
                </div>
                <!-- .col-xs-6 -->
            </header>
            <div class="chart">
                <div class="owp-chart-22"></div>
            </div>
            <!-- .chart -->
        </div>
        <!-- .col-lg-6 -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function() {

            // Oil well profile oil production chart
            new Chartist.Line('.owp-chart-11', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [ "{{ $oilwellproductionByMonth[0]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[1]['infos'][0]->oliProduced}}",
                        "{{ $oilwellproductionByMonth[2]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[3]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[4]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[5]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[6]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[7]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[8]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[9]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[10]['infos'][0]->oliProduced }}",
                        "{{ $oilwellproductionByMonth[11]['infos'][0]->oliProduced }}",
                    ]
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });

            // Oil well profile gas production chart
            new Chartist.Line('.owp-chart-22', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [ "{{ $oilwellproductionByMonth[0]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[1]['infos'][0]->gasProduced}}",
                        "{{ $oilwellproductionByMonth[2]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[3]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[4]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[5]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[6]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[7]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[8]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[9]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[10]['infos'][0]->gasProduced }}",
                        "{{ $oilwellproductionByMonth[11]['infos'][0]->gasProduced }}",
                    ]
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });

        });
    </script>
@endsection

