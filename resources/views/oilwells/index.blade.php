@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')


    <!-- #mobileMenu -->
    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Oil Wells</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12 border-top">
        <div class="wells-list">
            @foreach ($inputdatas as $oilwell)
            <div class="well-outer col-md-3">
                <div class="well text-center">
                    <span class="active-tag {{ $oilwell[0]->active ? '' : 'inactive' }}">{{ $oilwell[0]->active ? 'Active' : 'Inactive' }}</span>

                    @if( isset($oilwell[0]->thmb) && $oilwell[0]->thmb != '' )
                        <img src="{{ asset("images/oilwells/resized/".  $oilwell[0]->thmb) }}" class="img-responsive" alt="featured-img">
                    @else
                        <img src="{{ asset("images/well.png") }}" alt="well" class="img-responsive">
                    @endif
                    <h2 class="gray text-center">{{ $oilwell[0]->name }}</h2>
                    <div class="red-bttm"></div>
                    <h1 class="blue text-uppercase">${{ $oilwell[0]->oliProduced * 1.08}} <small>Production today</small></h1>
                    <p><i class="fa fa-long-arrow-up" aria-hidden="true"></i> G  | {{ $oilwell[0]->oliProduced }} bbl | {{ $oilwell[0]->gasProduced }} Mcf</p>
                    <a class="btn" href="/openOilwellProfile/{{ $oilwell[0]->id }}">More Info</a>
                </div>
                <!-- .well -->
            </div>
            <!-- .well-outer -->
            @endforeach

                <div class="well-outer col-md-3">
                    <div class="well text-center">
                        <a href="/addOilWell"><img src="{{ asset("images/plus.png") }}" alt="plus"></a>
                    </div>
                    <!-- .plus -->
                </div>

        </div>
        <!-- .wells-list -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->

    {{--<header><div class="arrow-left"></div> Oil wells</header>--}}
    {{--<div id="section">--}}
        {{--@foreach ($oilwells as $oilwell)--}}
            {{--<div class="package">--}}
                {{--<span class="status">{{$oilwell->active ? 'Active' : 'Inactive'}}</span>--}}
                {{--<div class="featured-img">--}}
                    {{--@if( isset($oilwell->image) && $oilwell->image != '' )--}}
                        {{--<img src="{{ asset("images/oilwells/".  $oilwell->image) }}" alt="featured-img">--}}
                    {{--@else--}}
                        {{--<img src="https://dummyimage.com/300x200/000/fff" alt="featured-img">--}}
                    {{--@endif--}}

                {{--</div>--}}
                {{--<!-- .featured-img -->--}}
                {{--<div class="package-info">--}}
                    {{--<h1>{{$oilwell->name}}</h1>--}}
                    {{--<div class="red-line"></div>--}}
                    {{--<span class="price">$25,000</span>--}}
                    {{--<p>5 Tanks</p>--}}
                    {{--<div class="btns"><a href="/openOilwellTanks/{{$oilwell->id}}">More Info</a></div>--}}
                {{--</div>--}}
                {{--<!-- .package-info -->--}}
            {{--</div>--}}

            {{--@endforeach--}}

                    {{--<!-- .package -->--}}
            {{--<div class="package">--}}
                {{--<div class="plus">--}}
                    {{--<a href="/addOilWell"><img src="{{ asset("images/plus.png") }}" alt="plus"></a>--}}
                {{--</div>--}}
                {{--<!-- .plus -->--}}
            {{--</div>--}}
    {{--</div>--}}
    <!-- #section -->
@endsection
