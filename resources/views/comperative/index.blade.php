@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Comparative Analysis</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-8 border-top">

        <div class="col-lg-4">
            <form  enctype="multipart/form-data" id="compareDataForm" role="form" method="GET" >
                {!! csrf_field() !!}

            <div class="form-row full">
                <p>Subject Well</p>
                <select id="oilwell0" type="oilwell0"  name="oilwell0" >
                    <option value="0">Select Oil Well Tank</option>
                    @foreach ($oilwells as $oilwell)
                        <option value="{{$oilwell->id}}">{{$oilwell->name}}</option>
                    @endforeach
                </select>
            </div>
            <!-- .form-row -->
            {{--<div class="form-row full">--}}
                {{--<p>Timeframe</p>--}}
                {{--<select name="timeframe">--}}
                    {{--<option value="daily">Daily</option>--}}
                    {{--<option value="weekly">Weekly</option>--}}
                    {{--<option value="">Monthly</option>--}}
                {{--</select>--}}
            {{--</div>--}}
            <!-- .form-row -->
            <div class="form-row full">
                <p>Outputs</p>
                <select><option value="dollars">Dollars</option></select>
            </div>
            <!-- .form-row -->
            <div class="numbers">
                <div class="number-lg red-line">
                    <p>Total production</p>
                    <h1 class="first-totalproduction">$0</h1>
                    <p>Total sales</p>
                    <h1 class="first-totalsales">$0</h1>
                </div>
                <!-- .number-lg -->
                <div class="number-sm red-line bg-img-2 oil-bg">
                    <p>Oil production</p>
                    <h1 class="border-bottom first-oilproduction">$0</h1>
                    <p>Average oil production</p>
                    <h1 class="border-bottom first-avg-oilproduction">$0</h1>
                    <p>Oil in stock</p>
                    <h1 class="first-oilstock">$0</h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line bg-img-2 gas-bg">
                    <p>Gas production</p>
                    <h1 class="border-bottom first-gasproduction">$0</h1>
                    <p>Average gas production</p>
                    <h1 class="border-bottom first-avg-gasproduction">$0</h1>
                    <p>Gas in stock</p>
                    <h1 class="first-gasstock">$0</h1>
                </div>
                <!-- .number-sm -->
            </div>
            <!-- .numbers -->
            </form>
        </div>



        <!-- .col-lg-4 -->
        <div class="col-lg-4">
            <form  enctype="multipart/form-data" id="compareDataForm" role="form" method="GET" >
                {!! csrf_field() !!}

            <div class="form-row full">
                <p>Compared To</p>
                <select id="oilwell1" type="oilwell1"name="oilwell1" >
                    <option value="0">Select Oil Well</option>
                    @foreach ($oilwells as $oilwell)
                        <option value="{{$oilwell->id}}">{{$oilwell->name}}</option>
                    @endforeach
                </select>
            </div>
            <!-- .form-row -->
            <div class="numbers">
                <div class="number-lg red-line">
                    <p>Total production</p>
                    <h1 class="second-totalproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Total sales</p>
                    <h1 class="second-totalsales" >$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></p>
                </div>
                <!-- .number-lg -->
                <div class="number-sm red-line bg-img-2 oil-bg">
                    <p>Oil production</p>
                    <h1 class="border-bottom second-oilproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Average oil production</p>
                    <h1 class="border-bottom second-avg-oilproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Oil in stock</p>
                    <h1 class="second-oilstock">$0 <small>0.00%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line bg-img-2 gas-bg">
                    <p>Gas production</p>
                    <h1 class="border-bottom second-gasproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Average gas production</p>
                    <h1 class="border-bottom second-avg-gasproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Gas in stock</p>
                    <h1 class="second-gasstock">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                </div>
                <!-- .number-sm -->
            </div>
            <!-- .numbers -->
            </form>
        </div>

        <!-- .col-lg-4 -->


        <div class="col-lg-4">
            <form enctype="multipart/form-data" id="compareDataForm" role="form" method="GET" >
                {!! csrf_field() !!}
            <div class="form-row full">
                <p>Compared To</p>
                <select id="oilwell2" type="oilwell2" name="oilwell2" >
                    <option value="0">Select Oil Well 2 </option>
                    @foreach ($oilwells as $oilwell)
                        <option value="{{$oilwell->id}}">{{$oilwell->name}}</option>
                    @endforeach
                </select>
            </div>
            <!-- .form-row -->
            <div class="numbers">
                <div class="number-lg red-line">
                    <p>Total production</p>
                    <h1 class="third-totalproduction" >$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Total sales</p>
                    <h1 class="third-totalsales">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                </div>
                <!-- .number-lg -->
                <div class="number-sm red-line bg-img-2 oil-bg">
                    <p>Oil production</p>
                    <h1 class="border-bottom third-oilproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Average oil production</p>
                    <h1 class="border-bottom third-avg-oilproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Oil in stock</p>
                    <h1 class="third-oilstock">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line bg-img-2 gas-bg">
                    <p>Gas production</p>
                    <h1 class="border-bottom third-gasproduction">$0 <small>0.00%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Average gas production</p>
                    <h1 class="border-bottom third-avg-gasproduction">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                    <p>Gas in stock</p>
                    <h1 class="third-gasstock">$0 <small>0.00% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                </div>
                <!-- .number-sm -->
            </div>
            <!-- .numbers -->
            </form>
        </div>
        <!-- .col-lg-4 -->

    </div>

    <!-- .col-lg-8 -->
    <div class="col-lg-8">
        <header>
            <div class="col-xs-6">
                <h2>Oil Production</h2>
            </div>
            <!-- .col-xs-6 -->
            <div class="col-xs-6">
                <select><option value="barrels">Barrels/Mfc</option></select>
                {{--<select><option value="daily">--}}{{--Daily--}}{{--</option></select>--}}
            </div>
            <!-- .col-xs-6 -->
        </header>
        <div class="chart">
            <div class="ca-chart11"></div>
        </div>
        <!-- .chart -->
    </div>
    <!-- .col-lg-8 -->
  </div>
  <!-- .panel-inner -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            // Dashboard pie chart 22

            // Comparative analysis chart
            new Chartist.Line('.ca-chart11', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });
        });
    </script>

@endsection


