@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Users Management</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12">
        <div class="col-lg-12">
            <div class="table-holder custom-panel padding-lr">
                <header class="table-header">
                    <h1>Users List</h1>
                </header>
                <!-- .table-header -->
                <table class="table">
                    <thead>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Permissions</th>
                    <th>Phone</th>
                    <th>City</th>
                    <th>ZIP</th>
                    <th>State</th>
                    <th>Packages</th>
                    <th>Investments</th>
                    <th>Years Invest</th>
                    <th>Edit User</th>
                    </thead>
                    @foreach($users as $user)
                        <tr>
                            <td><a href="{{$user->email}}">{{$user->email}}</a></td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->lname }}</td>
                            <td>
                                <div class="dropdown">
                                    <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                        {{ $user->role()->first()->name }}<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Pumper</a></li>
                                    </ul>
                                </div>
                                <!-- .dropdown -->
                            </td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->city }}</td>
                            <td>{{ $user->zip }}</td>
                            <td>{{ $user->state }}</td>
                            <td>{{ $user->package()->count() }}</td>
                            <td>${{ $user->investment()->first()->investment }}</td>
                            <td> {{ $user->investment()->first()->investmentYears }}</td>

                            <td><a href="/editUser/{{ $user->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- .table-holder -->
        </div>
        <!-- .col-lg-12 -->
    </div>
    <!-- .col-lg-12 -->
    <div class="col-lg-12">
        <div class="col-md-8 add-user">
            <div class="table-header">
                <h1>Add User</h1>
            </div>
            <!-- .form-row -->
            <div id="info" style="text-align: center; width: auto;"></div>
            <div class="clearfix"></div>
            <!-- .table-header -->
            <form class="form-horizontal" enctype="multipart/form-data" id="addUserForm" role="form" method="POST" >
                {{--<form method="post" action="{{ url('api/v1/addnewuser') }}">--}}
                {!! csrf_field() !!}
                <div class="col-lg-6">
                    <div class="form-row">
                        <p>Email</p>
                        <input type="email" value="" name="email" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>First Name</p>
                        <input type="text" value="" name="name" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Last Name</p>
                        <input type="text" value="" name="lname" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Permissions</p>
                        <select type="permissions" name="role" class="form-control" required>
                            <option value="">Select Role</option>
                            @foreach ($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>ZIP</p>
                        <input type="number" value="" name="zip" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>City</p>
                        <input type="text" value="" name="city" required>
                    </div>

                    <div class="form-row">
                        <p>Phone</p>
                        <input type="text" value="" name="phone" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>State</p>
                        <select name="state" required>
                            <option value="">Select state</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>
                    </div>
                    <!-- .form-row -->
                </div>
                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    <div class="form-row">
                        <p>Password</p>
                        <input type="password" value="" name="password" required>
                    </div>

                    <div class="form-row">
                        <p>Confirm Password</p>
                        <input type="password" name="password_confirmation" required>
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Oil Well Package</p>
                        <select type="role" class="form-control" name="packageid" required>
                            <option value="">Select Package</option>
                            @foreach ($packages as $package)
                                <option value="{{$package->id}}">{{$package->name}}</option>
                            @endforeach
                        </select>

                        {{--<input type="text" value="" name="oilwell">--}}
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Investment $</p>
                        <input type="number" value="" name="investment" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Type of Investment (yrs)</p>
                        <input type="number" name="investmentYears" value="" required>
                    </div>
                    <div class="form-row">
                        <p>Upload profile picture</p>
                        <input type="file" name="profile_pic" accept="image/*" required>
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <input class="btn" type="submit" value="Add a new user">
                    </div>


                    <!-- .form-row -->
                </div>
                <!-- .col-lg-6 -->
            </form>
        </div>
        <!-- .add-user -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->


    {{--<header><div class="arrow-left"></div> User Management</header>--}}
    {{--<div id="section">--}}
        {{--<div class="user-list">--}}
            {{--<table>--}}
                {{--<tr>--}}
                    {{--<th>Email</th>--}}
                    {{--<th>First Name</th>--}}
                    {{--<th>Last Name</th>--}}
                    {{--<th>Permissions</th>--}}
                    {{--<th>Phone</th>--}}
                    {{--<th>City</th>--}}
                    {{--<th>Zip Code</th>--}}
                    {{--<th>State</th>--}}
                    {{--<th>Packages</th>--}}
                    {{--<th>Investment</th>--}}
                    {{--<th>Years</th>--}}
                    {{--<th>Edit User</th>--}}
                {{--</tr>--}}
                {{--@foreach($users as $user)--}}
                {{--<tr>--}}
                    {{--<td>{{$user->email}}</td>--}}
                    {{--<td>{{$user->name}}</td>--}}
                    {{--<td>{{$user->email}}</td>--}}
                    {{--<td><select name="permissions"><option value="pumper">Pumper</option></select></td>--}}
                    {{--<td>{{$user->phone}}</td>--}}
                    {{--<td>{{$user->city}}</td>--}}
                    {{--<td>{{$user->zip}}</td>--}}
                    {{--<td>{{$user->state}}</td>--}}
                    {{--<td>Package}</td>--}}
                    {{--<td>Investment</td>--}}
                    {{--<td>Years</td>--}}
                    {{--<td href="#">Edit User</td>--}}
                {{--</tr>--}}
                    {{--@endforeach--}}
            {{--</table>--}}
        {{--</div>--}}
        {{--<!-- .user-list -->--}}
        {{--<div class="add-new">--}}
                {{--<form class="form-horizontal" enctype="multipart/form-data" id="addUserForm" role="form" method="POST" >--}}
                    {{--<form method="post" action="{{ url('api/v1/addnewuser') }}">--}}
                {{--{!! csrf_field() !!}--}}
                {{--<div class="form-row">--}}
                    {{--<p>Email</p>--}}
                    {{--<input type="text" name="email">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>First Name</p>--}}
                    {{--<input type="text" name="name">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Last Name</p>--}}
                    {{--<input type="text" name="lname">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Password</p>--}}
                    {{--<input type="text" name="password">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Confirm Password</p>--}}
                    {{--<input type="text" name="password_confirmation">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Phone</p>--}}
                    {{--<input type="text" name="phone">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>City</p>--}}
                    {{--<input type="text" name="city">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>ZIP Code</p>--}}
                    {{--<input type="text" name="zip">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>State/Province</p>--}}
                    {{--<input type="text" name="state">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Permissions</p>--}}
                    {{--<select type="role" class="form-control" name="role" >--}}
                        {{--<option value="0">Select Role</option>--}}
                        {{--@foreach ($roles as $role)--}}
                            {{--<option value="{{$role->id}}">{{$role->name}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Oil Well Package</p>--}}
                    {{--<p>Package A</p>--}}

                    {{--<input type="text" name="pack">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<select type="role" class="form-control" name="packageid" >--}}
                        {{--<option value="0">Select Package</option>--}}
                        {{--@foreach ($packages as $package)--}}
                            {{--<option value="{{$package->id}}">{{$package->name}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}

                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Investment ($)</p>--}}
                    {{--<input type="text" name="investment">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<p>Type of investment (yrs)</p>--}}
                    {{--<input type="text" name="investmentYears">--}}
                {{--</div>--}}
                {{--<div class="form-row">--}}
                    {{--<input type="submit" value="Add User">--}}
                {{--</div>--}}


                    {{--<!-- .form-row -->--}}
                    {{--<div id="info" style="text-align: center;"></div>--}}
                    {{--<div class="clearfix"></div>--}}
            {{--</form>--}}
        {{--</div>--}}
        {{--<!-- .add-new -->--}}
    {{--</div>--}}
    {{--<!-- #section -->--}}

@endsection
