@extends('layouts.blank')

@push('stylesheets')

@endpush
{{--src="{{ asset("images/profile/resized/".  Auth::user()->profile_pic ) }}"--}}
@section('main_container')

    <div class="panel-inner">
    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Users Management</h1>
    </div>

    <!-- .col-lg-12 -->
    <div class="col-lg-12">
        <div class="col-md-8 add-user">
            <div class="table-header">
                <h1>Edit User Profile</h1>
            </div>
            <!-- .form-row -->
            <div id="info" style="text-align: center; width: auto;"></div>
            <div class="clearfix"></div>
            <!-- .table-header -->
            <form class="form-horizontal" enctype="multipart/form-data" id="updateUserForm" role="form" method="POST" >
                {{--<form method="post" action="{{ url('api/v1/updatenewuser') }}">--}}
                {!! csrf_field() !!}
                <div class="col-lg-6">

                    <div class="form-row" style="display: none">
                        <input type="text" name="id" value="{{ $user[0]->id }}" >
                    </div>

                    <div class="form-row">
                        <p>Email</p>
                        <input type="email" value="{{ $user[0]->email }}" name="email" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>First Name</p>
                        <input type="text" value="{{ $user[0]->name }}" name="name" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Last Name</p>
                        <input type="text" value="{{ $user[0]->lname }}" name="lname" required>
                    </div>

                    <div class="form-row" style="display: none">
                        @if(isset($user[0]->role[0]))
                            <input type="number" value="{{ $user[0]->role[0]->id  }}" name="roleID" required>
                        @else
                            <input type="number" value="" name="roleID" required>
                        @endif
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Permissions</p>
                        <select type="permissions" name="role" class="form-control" required>
                            @if(isset($user[0]->role[0]))
                                <option value="{{ $user[0]->role[0]->id }}">{{ $user[0]->role[0]->name }}</option>
                            @else
                                <option value="">Select Role</option>
                            @endif

                            @foreach ($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select></div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>ZIP</p>
                        <input type="number" value="{{ $user[0]->zip }}" name="zip" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>City</p>
                        <input type="text" value="{{ $user[0]->city }}" name="city" required>
                    </div>

                    <div class="form-row">
                        <p>Phone</p>
                        <input type="text" value="{{ $user[0]->phone }}" name="phone" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>State</p>
                        <select name="state" required>
                            <option value="{{ $user[0]->state }}"> {{ $user[0]->state }} </option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>
                    </div>
                    <!-- .form-row -->
                </div>
                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    <div class="form-row" style="display: none">
                        <input type="passwordID" value="{{ $user[0]->password }}" name="password">
                    </div>

                    <div class="form-row">
                        <p>Password</p>
                        <input type="password" value="" name="password">
                    </div>

                    <div class="form-row">
                        <p>Confirm Password</p>
                        <input type="password" value=""  name="password_confirmation">
                    </div>

                    <div class="form-row" style="display: none">
                        @if(isset($user[0]->package[0]))
                            <input type="number" value="{{$user[0]->package[0]->id }}" name="packageID">
                         @else
                            <input type="number" value="" name="packageID">
                        @endif
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Oil Well Package</p>
                        <select type="role" class="form-control" name="packageid" >
                            @if(isset($user[0]->package[0] ))
                                <option value="{{ $user[0]->package[0]->id }}">{{ $user[0]->package[0]->name }}</option>
                            @else
                                <option value="">Select Package</option>
                            @endif

                            @foreach ($packages as $package)
                                <option value="{{$package->id}}">{{$package->name}}</option>
                            @endforeach
                        </select>

                        {{--<input type="text" value="" name="oilwell">--}}
                    </div>

                    <div class="form-row" style="display: none">
                        @if(isset ($user[0]->investment[0]))
                        <input type="number" value="{{ $user[0]->investment[0]->id }}" name="investmentID" required>
                            @else
                            <input type="number" value="" name="investmentID" required>
                        @endif
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Investment $</p>
                        @if( isset($user[0]->investment[0]))
                        <input type="number" value="{{ $user[0]->investment[0]->investment }}" name="investment" required>
                        @else
                            <input type="number" value="" name="investment" required>
                        @endif
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Type of Investment (yrs)</p>
                        @if( isset($user[0]->investment[0]))
                        <input type="number" name="investmentYears" value="{{ $user[0]->investment[0]->investmentYears }}" required>
                        @else
                            <input type="number" value="" name="investmentYears" required>
                        @endif
                    </div>

                    <div class="form-row" style="display: none">
                        <input name="profile_picID" value="{{ $user[0]->profile_pic }}">
                    </div>

                    <div class="form-row">
                        <p>Upload profile picture</p>
                        <input type="file" name="profile_pic" accept="image/*" value="{{ $user[0]->profile_pic }}">
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <input class="btn" type="submit" value="Update User">
                    </div>


                    <!-- .form-row -->
                </div>
                <!-- .col-lg-6 -->
            </form>
        </div>
        <!-- .add-user -->
    </div>
    </div>
    <!-- .col-lg-12 -->

@endsection