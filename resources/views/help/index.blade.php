@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Help & Support</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12 border-top border-bottom">
        <div class="col-lg-8">
            <div class="help">
                <h1>1) Who can invest in these projects?</h1>
                <p>– Anyone not a citizen of the USA.</p>
                <p>– Must invest a minimum of $50,000</p>
            </div>
            <!-- .help -->
            <div class="help">
                <h1>2) How do you source the projects?</h1>
                <p>We work directly with oil & gas operating companies, landmen (similar to realtors for the oil & gas industry), M&A firms, and auction houses.</p>
            </div>
            <!-- .help -->
            <div class="help">
                <h1>3) Are there fees to invest with the fund?</h1>
                <p>Yes. There are fees but they are built into the investment amount.</p>
            </div>
            <!-- .help -->
            <div class="help">
                <h1>4) What do I get when I invest in a project in the fund?</h1>
                <p>A prorated share of the working interest in each of the portfolio’s wells.</p>
            </div>
            <!-- .help -->
            <div class="help">
                <h1>5) What is available for your due diligence?</h1>
                <p>We have an extensive due diligence process for each asset we consider. Accordingly, we certainly understand the criticality of detailed due diligence and have organized the following items for your review:</p>
                <p>– Independent verification of management’s representations (contact with issuer’s customers; lenders,  vendors, lower-level employees, etc.)</p>
                <p>– Reviewing news articles and industry publications regarding the issuer, its market, and competition</p>
                <p>– Review the company’s internal documents such as operating plans, product literature, corporate records,  financial statements, contracts and lists of distributors and customers</p>
                <p>– Physical inspection of the company’s facilities</p>
                <p>– Contact with the issuer’s auditor and other experts knowledgeable about the company</p>
                <p>– Written company assurances as to the accuracy of records and financial statements</p>
                <p>– Contact with outside directors</p>
                <p>– Interviews of key personnel or customers</p>
                <p>– Updating due diligence as needed</p>
                <p>We will do our best to provide a detailed response to your questions within 2 business days.</p>
                <p>The Fund does not provide investment, legal or tax advice. Consult your own advisor.</p>
            </div>
            <!-- .help -->
            <div class="help">
                <h1>6) Are these investments risky?</h1>
                <p>Yes. Oil and gas investments inherently involve highly speculative activities in which results cannot be exactly predicted and which necessarily involve risk of loss. Legal and tax professionals should be consulted before any investment is considered in an oil and gas project.
                    Many private oil and gas projects encounter mechanical and geophysical risk. To mitigate this risk, some investors may choose to build a portfolio of companies to spread their risk across a number of different companies.
                </p>
            </div>
            <!-- .help -->
            <div class="help">
                <h1>7) How do I monitor the performance of my investment?</h1>
                <p>You can track your investment daily through our investor portal and and through our quarterly financial reports.</p>
            </div>
            <!-- .help -->
        </div>
        <!-- .col-lg-8 -->
        <div class="col-lg-4">
            <div class="custom-panel padding-lr">
                <h4>Contact our support team anytime!</h4>
                <p>Our support team is available to help 24/7<br> via email<br> <a href="mailto:help@energyassetfund.com">help@energyassetfund.com</a> or phone +1 312 300 7500</p>
            </div>
            <!-- .custom-panel -->
        </div>
        <!-- .col-lg-4 -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->

@endsection
