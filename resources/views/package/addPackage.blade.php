@extends('layouts.blank')

@push('stylesheets')
{{--<link href="css/style-z.css" rel="stylesheet"/>--}}
@endpush

@section('main_container')
    <!-- #mobileMenu -->
    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Add New Oil Well Package</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-md-4">
        <form class="form-horizontal" enctype="multipart/form-data" id="addPackageForm" role="form" method="POST">
            {{--<form method="post" action="{{ url('/api/v1/packages') }}">--}}
            {!! csrf_field() !!}

            <div class="form-row">
                <p>Oil Well Package</p>
                <input type="text" value="" name="name" required>
            </div>
            <!-- .form-row -->
            {{--<div class="form-row">--}}
                {{--<p>Oil Well</p>--}}
                {{--<input type="text" value="" name="oilwell">--}}
            {{--</div>--}}
            <!-- .form-row -->
            {{--<div class="form-row">--}}
                {{--<select>--}}
                    {{--<option value="reed">Reed</option>--}}
                {{--</select>--}}
            {{--</div>--}}
            <!-- .form-row -->
            <div class="form-row">
                <p>Upload Image</p>
                <input type="file" name="image" class="form-control" id="image" accept="image/*" required>
            </div>


            <div class="form-row" id="input_fields_wrap">
                <p>Oil Well</p>
                    <button class="add_field_button"> + Connect Oil Well with this package</button>
                <br/>
                {{--<div>--}}
                    {{--<input type="text" name="mytext[]">--}}
                {{--</div>--}}

                <select name="oilWellId[]" id="WellId" class="form-control">
                    <option value="0">Select oil well</option>
                    @foreach($oilWells as $oilWell)
                        <option value="{{$oilWell->id}}">{{$oilWell->name}}</option>
                    @endforeach

                </select>
            </div>

        {{--<div class="input_fields_wrap">--}}
            {{--<button class="add_field_button">Add More Fields</button>--}}
                {{--<div>--}}
                    {{--<input type="text" name="mytext[]">--}}
                {{--</div>--}}

                {{--<select name="WellId[]">--}}
                     {{--@foreach($oilWells as $oilWell)--}}
                        {{--<option value="{{$oilWell->id}}">{{$oilWell->name}}</option>--}}
                     {{--@endforeach--}}
                {{--</select>--}}
        {{--</div>--}}

            <!-- .form-row -->
            <div class="form-row">
                <input type="submit" value="Submit" class="btn">
            </div>
            <!-- .form-row -->
        </form>
    </div>
    <!-- .col-md-4 -->
  </div>
  <!-- .panel-inner -->

@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div> <select name="oilWellId[]" id="WellId" class="form-control"><option value="">Select oil well</option>@foreach($oilWells as $oilWell)<option value="{{$oilWell->id}}">{{$oilWell->name}}</option>@endforeach</select> <a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e) { //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
