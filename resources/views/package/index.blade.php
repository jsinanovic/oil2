@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <div class="heading">
        <div class="arrow-left"></div>
        <h1 class="pull-left">Oil Wells Packages</h1>
    </div>


    <!-- .heading -->
    <div class="panel-inner">
        <div id="info" style="text-align: center; width: auto;"></div>
    <div class="col-lg-12 border-top">
        <div class="wells-list">

            @foreach ($inputdatas as $package)

            <div class="well-outer col-md-3">
                <div class="well text-center">
                    @if( isset($package['data'][0]->thmb) && $package['data'][0]->thmb != '' )
                        <img src="{{ asset("images/catalog/resized/".  $package['data'][0]->thmb) }}" alt="mg-responsive">
                    @else
                        <img src="images/well.png" alt="well" class="img-responsive">
                    @endif
                    <h2 class="gray text-center">{{ $package['data'][0]->name }}</h2>
                    <div class="red-bttm"></div>
                    <h1 class="blue text-uppercase">${{ ($package['data'][0]->oliProduced * 1.08)  }}<small>Production today</small></h1>
                    <p class="text-uppercase">{{ $package['size'] }} oil wells</p>
                    <div class="btns">
                        <ul>
                            <li><a href="/openPackageProfile/{{ $package['data'][0]->id }}">Details</a></li>
                            <li><a href="/openPackage/{{ $package['data'][0]->id }}">Wells</a></li>
                        </ul>
                    </div>
                    <!-- .btns -->
                    <a href="/editPackage/{{ $package['data'][0]->id }}">Edit</a> | <a  value="{{ $package['data'][0]->id }}"
                                                                                       href="javascript:removeFunction({{ $package['data'][0]->id }});">Delete</a>
                </div>
                <!-- .well -->
            </div>
            <!-- .well-outer -->
            @endforeach

                <div class="well-outer col-md-3">
                    <div class="well text-center">
                        <a href="/addpackage"><img src="{{ asset("images/plus.png") }}" alt="plus"></a>
                    </div>
                    <!-- .plus -->
                </div>
                <!-- .well-outer -->

        </div>
        <!-- .wells-list -->
      </div>
      <!-- .col-lg-12 -->
    </div>
    <!-- .panel-inner -->
@endsection
