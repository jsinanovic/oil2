@extends('layouts.blank')

@push('stylesheets')
    {{--<link href="css/style-z.css" rel="stylesheet"/>--}}
@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1 class="pull-left">Oil Wells Packages | {{$packageInforamtions->first()->name }} oil wells list</h1>
        <a class="btn pull-right" href="#">Package detail view</a>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12 border-top">
        <div class="wells-list">
            @foreach ($inputdatas as $oilwell)
            <div class="well-outer col-md-3">
                <div class="well text-center">
                    <span class="active-tag">{{ $oilwell[0]->active ? 'Active' : 'Inactive' }}</span>
                    @if( isset($oilwell[0]->image) && $oilwell[0]->image != '' )
                        <img src="{{ asset("images/oilwells/".  $oilwell[0]->image) }}" alt="featured-img">
                    @else
                        <img src="images/well.png" alt="well" class="img-responsive">
                    @endif


                    <h2 class="gray text-center">{{ $oilwell[0]->name }}</h2>
                    <div class="red-bttm"></div>
                    <h1 class="blue text-uppercase">${{ $oilwell[0]->oliProduced * 1.08}} <small>Production today</small></h1>
                    <p><i class="fa fa-long-arrow-up" aria-hidden="true"></i> G 3% | {{ $oilwell[0]->oliProduced }} bbl | {{ $oilwell[0]->gasProduced }} Mcf</p>
                    <a class="btn" href="/openOilwellProfile/{{ $oilwell[0]->id }}">More Info</a>
                </div>
                <!-- .well -->
            </div>
            <!-- .well-outer -->
            @endforeach

        </div>
        <!-- .wells-list -->
    </div>
    <!-- .col-lg-12-->
  </div>
  <!-- .panel-inner -->






    {{--<header><div class="arrow-left"></div> Oil wells inside package | Package: -  oil wells list <a class="btn" href="#">Package detailed view</a></header>--}}
    {{--<div id="section">--}}

        {{--@foreach ($oilwells as $oilwell)--}}
            {{--<div class="package">--}}
                {{--<span class="status">{{$oilwell->active ? 'Active' : 'Inactive'}}</span>--}}
                {{--<div class="featured-img">--}}
                    {{--@if( isset($oilwell->image) && $oilwell->image != '' )--}}
                        {{--<img src="{{ asset("images/oilwells/".  $oilwell->image) }}" alt="featured-img">--}}
                    {{--@else--}}
                        {{--<img src="https://dummyimage.com/300x200/000/fff" alt="featured-img">--}}
                    {{--@endif--}}

                {{--</div>--}}
                {{--<!-- .featured-img -->--}}
                {{--<div class="package-info">--}}
                    {{--<h1>{{$oilwell->name}}</h1>--}}
                    {{--<div class="red-line"></div>--}}
                    {{--<span class="price">$25,000</span>--}}
                    {{--<p>5 Tanks</p>--}}
                    {{--<div class="btns"><a href="/openOilwellTanks/{{$oilwell->id}}">More Info</a></div>--}}
                {{--</div>--}}
                {{--<!-- .package-info -->--}}
            {{--</div>--}}

        {{--@endforeach--}}

                {{--<!-- .package -->--}}
            {{--<div class="package">--}}
                {{--<div class="plus">--}}
                    {{--<a href="/addOilWell"><img src="{{ asset("images/plus.png") }}" alt="plus"></a>--}}
                {{--</div>--}}
                {{--<!-- .plus -->--}}
            {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #section -->--}}

@endsection
