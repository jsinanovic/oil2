@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')


    <div class="heading">
        <div class="arrow-left"></div>
        <h1 class="pull-left">
            Oil Well Packages | {{ $oilwells[0]->name }}
        </h1>
        <input id="packageId"  name="packageId" value="{{$oilwells[0]->id}}" style="display: none;">
        <select id="packageSelect" name="packageSelect" class="select-header pull-left">
            <option value="">Filter By</option>
            <option value="1">Monthly</option>
            <option value="2">Year</option>
        </select>
        <a class="btn pull-right" href="#">Package Wells List</a>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12 border-top">
        <div class="col-lg-2 act-img-wrapper">
            <div class="act-img">
                    <img src="{{asset("images/well.png")}}" alt="active-img"></div>
            <!-- .active-img -->
            <div class="red-line pull-left" style="margin-top: 20px;">
                <h2 class="blue text-uppercase"><strong> Active</strong></h2>
            </div>
            <!-- .red-line -->
            <div class="red-line pull-right" style="margin-top: 20px;">
                <h2 style="margin: 0;">{{ $tanks }}/<span class="gray">{{ $tanks }}</span><br> <small>Tanks</small></h2>
            </div>
            <!-- .red-line -->
        </div>
        <!-- .col-lg-2 -->
        <div class="col-lg-10">
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <h1 class="red text-uppercase"><img style="width: 40px; vertical-align: middle;" src="{{asset("images/oil-img.png")}}"  alt="oil-img"> Oil</h1>
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldataPackage[0]->oliProduced }} <span>bbl</span></h1>
                        <p>Oil production</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldataPackage[0]->oliProduced  * 1.08}} </h1>
                        <p>Oil production $</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldataPackage[0]->oilLvlFT }}<span>bbl</span></h1>
                        <p>Oil level in tanks</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldataPackage[0]->oilLvlFT * 1.08}}</h1>
                        <p>Oil level in tanks ($)</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
            </div>
            <!-- .col-lg-12 -->
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <h1 class="red text-uppercase"><img style="width: 40px; vertical-align: middle;" src="{{asset("images/gas-img.png")}}" alt="gas-img"> Gas</h1>
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldataPackage[0]->gasProduced }}  <span>bbl</span></h1>
                        <p>Gas production</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldataPackage[0]->gasProduced * 1.08 }} </h1>
                        <p>Gas production $</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>{{ $oilwelldataPackage[0]->gaslvl }}<span>bbl</span></h1>
                        <p>Gas level in tanks</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
                <div class="col-lg-2">
                    <div class="number red-line">
                        <h1>${{ $oilwelldataPackage[0]->gaslvl * 1.08 }}</h1>
                        <p>Gas level in tanks ($)</p>
                    </div>
                    <!-- .number -->
                </div>
                <!-- .col-lg-2 -->
            </div>
            <!-- .col-lg-12 -->
        </div>
        <!-- .col-lg-10 -->
    </div>
    <!-- .col-lg-12 -->
    <div class="col-lg-12" style="padding: 40px 0;">
        <div class="col-lg-6">
            <header>
                <div class="col-xs-6">
                    <h2>Oil Production</h2>
                </div>
                <!-- .col-xs-6 -->
                <div class="col-xs-6">
                  <select>
                      <option value="barrels">Barrels</option>
                   </select>
                   {{--<select>--}}
                      {{--<option value="monthly">Monthly</option>--}}
                   {{--</select>--}}
                </div>
                <!-- .col-xs-6 -->
            </header>
            <div class="chart">
                <div class="owpd-chart-11"></div>
            </div>
            <!-- .chart -->
        </div>
        <!-- .col-lg-6 -->
        <div class="col-lg-6">
            <header>
                <div class="col-xs-6">
                    <h2>Gas Production</h2>
                </div>
                <!-- .col-xs-6 -->
                <div class="col-xs-6">
                  <select>
                     <option value="Mfc">Mfc</option>
                  </select>
                  {{--<select>--}}
                     {{--<option value="monthly">Monthly</option>--}}
                  {{--</select>--}}
                </div>
                <!-- .col-xs-6 -->
            </header>
            <div class="chart">
                <div class="owpd-chart-22"></div>
            </div>
            <!-- .chart -->
        </div>
        <!-- .col-lg-6 -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function() {

            // Oil well profile oil production chart
            new Chartist.Line('.owpd-chart-11', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [ "{{ $packageproductionByMonth[0]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[1]['infos'][0]->oliProduced}}",
                        "{{ $packageproductionByMonth[2]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[3]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[4]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[5]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[6]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[7]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[8]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[9]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[10]['infos'][0]->oliProduced }}",
                        "{{ $packageproductionByMonth[11]['infos'][0]->oliProduced }}",
                    ]
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });

            // Oil well profile gas production chart
            new Chartist.Line('.owpd-chart-22', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [ "{{ $packageproductionByMonth[0]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[1]['infos'][0]->gasProduced}}",
                        "{{ $packageproductionByMonth[2]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[3]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[4]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[5]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[6]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[7]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[8]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[9]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[10]['infos'][0]->gasProduced }}",
                        "{{ $packageproductionByMonth[11]['infos'][0]->gasProduced }}",
                    ]
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });

        });
    </script>
@endsection



