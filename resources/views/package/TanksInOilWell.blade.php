@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')


    <div class="heading">
        <div class="arrow-left"></div>
        <h1 class="pull-left">Tanks list | Oil Wells: -  Tanks list</h1>
        <a class="btn pull-right" href="#">Package detail view</a>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12 border-top">
        <div class="wells-list">
            @foreach ($oilwellsPTanks as $oilwellsPTank)
                <div class="well-outer col-md-3">
                    <div class="well text-center">
                        <span class="active-tag">{{$oilwellsPTank->active ? 'Active' : 'Inactive'}}</span>
                        {{--@if( isset($oilwellsPTank->image) && $oilwellsPTank->image != '' )--}}
                            {{--<img src="{{ asset("images/oilwells/".  $oilwellsPTank->image) }}" alt="featured-img">--}}
                        {{--@else--}}
                            {{--<img src="images/well.png" alt="well" class="img-responsive">--}}
                        {{--@endif--}}
                        <img src="https://dummyimage.com/300x200/000/fff" alt="featured-img">


                        <h2 class="gray text-center">{{$oilwellsPTank->tankId}}</h2>
                        <div class="red-bttm"></div>
                        <h1 class="blue text-uppercase">$25,612 <small>Production today</small></h1>
                        <p><i class="fa fa-long-arrow-up" aria-hidden="true"></i> G 3% | 30 bbl | 20 Mcf</p>
                        <a class="btns" href="/tank-details/{{$oilwellsPTank->id}}">More Info</a>
                    </div>
                    <!-- .well -->
                </div>
                <!-- .well-outer -->
            @endforeach

        </div>
        <!-- .wells-list -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->

    {{--<header><div class="arrow-left"></div> Tanks list | Oil Wells: -  Tanks list <a class="btn" href="#">Package detailed view</a></header>--}}
    {{--<div id="section">--}}

        {{--@foreach ($oilwellsPTanks as $oilwellsPTank)--}}
            {{--<div class="package">--}}
                {{--<span class="status">{{ $oilwellsPTank->active ? 'Active' : 'Inactive'}}</span>--}}
                {{--<div class="featured-img">--}}
                    {{--<img src="https://dummyimage.com/300x200/000/fff" alt="featured-img">--}}
                {{--</div>--}}
                {{--<!-- .featured-img -->--}}
                {{--<div class="package-info">--}}
                    {{--<h1>{{$oilwellsPTank->tankId}}</h1>--}}
                    {{--<div class="red-line"></div>--}}
                    {{--<span class="price">$25,000</span>--}}
                    {{--<p>Tanks id {{$oilwellsPTank->tankId}} </p>--}}
                    {{--<div class="btns"><a href="/tank-details/{{$oilwellsPTank->id}}">More Info</a></div>--}}
                {{--</div>--}}
                {{--<!-- .package-info -->--}}
            {{--</div>--}}

        {{--@endforeach--}}
                {{--<!-- .package -->--}}
            {{--<div class="package">--}}
                {{--<div class="plus">--}}
                    {{--<a href="/openOilWellTankForm/{{$oilwellid}}"><img src="{{ asset("images/plus.png") }}" alt="plus"></a>--}}
                {{--</div>--}}
                {{--<!-- .plus -->--}}
            {{--</div>--}}

    {{--</div>--}}

@endsection
