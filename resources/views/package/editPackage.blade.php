@extends('layouts.blank')

@push('stylesheets')
{{--<link href="css/style-z.css" rel="stylesheet"/>--}}
@endpush

@section('main_container')

    <div class="panel-inner">
        <div id="info" style="text-align: center;"></div>
        <div class="clearfix"></div>

        <div class="col-md-4">

        <form class="form-horizontal" enctype="multipart/form-data" id="updatePackageForm" role="form" method="POST" >
            {{--<form method="post" action="{{ url('/api/v1/packages') }}">--}}
            {!! csrf_field() !!}

            <div class="form-row" style="display: none">
                <p>id</p>
                <input type="text" name="id" value="{{ $package->id }}">
            </div>

            <div class="form-row" style="display: none">
                <p>id</p>
                <input type="text" name="imageID" value="{{ $package->image }}" >
            </div>

            <div class="form-row">
                <p>Oil well package</p>
                <input type="text" name="name" value="{{ $package->name }}" required>
            </div>
            <!-- .form-row -->


            <!-- .form-row -->
            <div class="form-row">
                <p>Upload image</p>
                <input type="file" name="images" class="form-control" id="images" value="" accept="image/*">
                {{--<input type="file" name="image" class="form-control" id="image" value="{{ $package->image }}">--}}
            </div>
            <div class="input_fields_wrap">
                <button class="add_field_button"> + Connect Oil Well with this package</button>
                <br/>
            </div>

            <!-- .form-row -->
            <div class="form-row">
                <input class="btn" type="submit" value="Update">
            </div>
            <!-- .form-row -->

        </form>
    </div>
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div> <select name="oilWellId[]" id="WellId">@foreach($oilWells as $oilWell)<option value="{{$oilWell->id}}">{{$oilWell->name}}</option>@endforeach <a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

