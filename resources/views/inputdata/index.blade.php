@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Input Data</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div id="info" style="text-align: center; width: auto"></div>
    <div class="clearfix"></div>

    <div class="col-lg-12 border-top border-bottom">
        <div class="col-lg-4">
            <!-- .form-row -->
            <h1 class="blue">Connel 81-H</h1>
            <form class="form-horizontal" enctype="multipart/form-data" id="addInputDataForm" role="form" method="POST" >
                {{--<form method="post" action="{{ url('api/v1/addinputdata') }}">--}}
                {!! csrf_field() !!}


                <div  style="width: 100%"  class="form-row">
                    <p>Date</p>
                    <select style="width: 30%; margin: 3px;  float: left;" name="dateDay" required>
                        <option value="">Day</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>


                    <select style="width: 30%; margin: 3px; float: left;" name="dateMonth" required>
                        <option value="">Month</option>
                        <option value="01">Jan</option>
                        <option value="02">Feb</option>
                        <option value="03">Mar</option>
                        <option value="04">Apr</option>
                        <option value="05">May</option>
                        <option value="06">Jun</option>
                        <option value="07">Jul</option>
                        <option value="07">Aug</option>
                        <option value="09">Sep</option>
                        <option value="10">Oct</option>
                        <option value="11">Nov</option>
                        <option value="12">Dec</option>
                    </select>


                    <select  style="width: 30%; margin: 3px; float: left;" name="dateYear" required>
                        <option value="">Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </div>

                {{--<div class="form-row">--}}
                    {{--<p>Date</p>--}}
                    {{--<input type="text" name="date" value="" data-provide="datepicker" required>--}}
                {{--</div>--}}
                <!-- .form-row -->
                <div class="form-row">
                    <p>Tank ID</p>
                    <select  class="form-control" name="tankId" required>
                        <option value="">Select Oil Well Tank</option>
                        @foreach ($oilWellsTanks as $oilWellsTank)
                            <option value="{{$oilWellsTank->id}}">{{$oilWellsTank->tankId}}</option>
                        @endforeach
                        {{--<option value="tankid">Tank ID</option>--}}
                    </select>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Oil level</p>
                    <input type="number" name="oilLvlFT" value="" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Oil sold (BBLS)</p>
                    <input type="number" name="oilSold" value="" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Gas level (Mcf)</p>
                    <input type="number" name="gaslvl" value="" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Gas sold (Mcf)</p>
                    <input type="number" name="gasSold" value="" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Water level</p>
                    <input type="number" name="waterlvl" value="" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Note</p>
                    <textarea name="note" required></textarea>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Active</p>
                    <select  class="form-control" name="active" required>
                        <option value="">Is Active</option>
                        <option value="1">YES</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <!-- .form-row -->
                {{--<div class="form-row">--}}
                    {{--<div class="output">--}}
                        {{--<div class="col-md-6">--}}
                            {{--<p>Oil produced on the day</p>--}}
                            {{--<h3>260.11 bbl</h3>--}}
                        {{--</div>--}}
                        {{--<!-- .col-md-6 -->--}}
                        {{--<div class="col-md-6">--}}
                            {{--<p>Gas produced on the day</p>--}}
                            {{--<h3>20.53 mcf</h3>--}}
                        {{--</div>--}}
                        {{--<!-- .col-md-6 -->--}}
                    {{--</div>--}}
                    {{--<!-- .output -->--}}
                {{--</div>--}}
                <!-- .form-row -->
                <div class="form-row">
                    <input class="btn pull-right border-top" type="submit" value="Input Data">
                </div>

            </form>
        </div>
        <!-- .col-lg-4 -->
        <div class="col-lg-8">
            <div class="col-lg-12">
                <div class="table-holder custom-panel padding-lr">
                    <header>
                        <h1 class="pull-left">Connel 81-H tanks list</h1>
                        <div class="pull-right text-uppercase">
                            {{--<a href="#"><i class="fa fa-pencil" aria-hidden="true"></i> Edit input data</a>--}}
                        </div>
                        <!-- .pull-right -->
                    </header>
                    <table class="table">
                        <thead>
                        <th>TANK ID</th>
                        <th>OIL LEVEL<br> (bbl)</th>
                        <th>OIL SOLD<br> (bbl)</th>
                        <th>GAS LEVEL<br> (Mcf)</th>
                        <th>GAS SOLD<br> (Mcf)</th>
                        <th>WATER LEVEL<br> (bbl)</th>
                        <th>OIL PRODUCED<br> (bbl)</th>
                        <th>GAS PRODUCED<br> (Mcf)</th>
                        <th>Edit</th>
                        </thead>

                        @foreach ($inputDatas as $inputData)
                            <tr>
                                <td>{{$inputData->tankId}}</td>
                                <td>{{$inputData->oilLvlFT}}</td>
                                <td>{{$inputData->oilSold}}</td>
                                <td>{{$inputData->gaslvl}}</td>
                                <td>{{$inputData->gasSold}}</td>
                                <td>{{$inputData->waterlvl }}</td>
                                <td>{{$inputData->oilLvlFT + $inputData->oilSold }}</td>
                                <td>{{$inputData->gaslvl + $inputData->gasSold  }}</td>
                                <td><a href="/input-data/{{ $inputData->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach

                    </table>
                </div>
                <!-- .table-holder -->
            </div>
            <!-- .col-lg-12 -->
            <div class="col-md-8">
                <div class="custom-panel padding-lr">
                    <h4>Monthly summary</h4>
                    <div class="report-row">
                        <p>Total Gas and Oil on Stock</p>
                        <h5 class="pull-right"><strong>{{ $oillvl +  $gaslvl }} bbl</strong></h5>
                    </div>
                    <!-- .report-row -->
                    <div class="report-row">
                        <p>Oil Sold</p>
                        <h5 class="pull-right">{{ $oilSold }} bbl</h5>
                    </div>
                    <!-- .report-row -->
                    <div class="report-row">
                        <p>Total Oil production</p>
                        <h5 class="pull-right">{{ $oilSold + $oillvl }} bbl</h5>
                    </div>
                    <!-- .report-row -->
                    <div class="report-row">
                        <p>Gas Sold</p>
                        <h5 class="pull-right">{{ $gasSold }} bbl</h5>
                    </div>
                    <!-- .report-row -->
                    <div class="report-row">
                        <p>Total Gas Production</p>
                        <h5 class="pull-right blue"><strong>{{ $gaslvl + $gasSold }} bbl</strong></h5>
                    </div>
                    <!-- .report-row -->
                </div>
                <!-- .custom-panel -->
            </div>
            <!-- .col-md-8 -->
            <div class="col-md-4">
                <div class="custom-panel padding-lr">
                    <h4>This well is overperforming!</h4>
                    <p>Compared to previous month, Connel 81-H produced 28% oil more!</p>
                </div>
                <!-- .custom-panel -->
            </div>
            <!-- .col-md-4 -->
        </div>
        <!-- .col-lg-8 -->
    </div>
    <!-- .col-lg-12 -->
  </div>




    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
    </script>
    <script type="text/javascript"
            src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
            src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript">
        $('#datetimepicker').datetimepicker({
            format: 'dd/MM/yyyy hh:mm:ss',
            language: 'pt-BR'
        });
    </script>

@endsection

