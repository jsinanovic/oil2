@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Input Data</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div id="info" style="text-align: center; width: auto"></div>
    <div class="clearfix"></div>

    <div class="col-lg-12 border-top border-bottom">
        <div class="col-lg-4">
            <!-- .form-row -->
            <h1 class="blue">Connel 81-H</h1>
            <form class="form-horizontal" enctype="multipart/form-data" id="updateInputDataForm" role="form" method="POST" >
                {{--<form method="post" action="{{ url('api/v1/addinputdata') }}">--}}
                {!! csrf_field() !!}

                <div class="form-row" style="display: none">
                    <input type="text" name="id" value="{{ $inputdata->id }}" >
                </div>

                <div class="form-row">
                    <p>Date</p>
                    <input type="text" name="date" value="{{ $inputdata->date }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Tank ID</p>
                    <select  class="form-control" name="tankId" disabled required>
                        <option value="{{ $inputdata->tankId }}">{{ $inputdata->tankId }}</option>
                        @foreach ($oilWellsTanks as $oilWellsTank)
                            <option value="{{$oilWellsTank->id}}">{{$oilWellsTank->tankId}}</option>
                        @endforeach
                        {{--<option value="tankid">Tank ID</option>--}}
                    </select>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Oil level</p>
                    <input type="number" name="oilLvlFT" value="{{ $inputdata->oilLvlFT }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Oil sold (BBLS)</p>
                    <input type="number" name="oilSold" value="{{ $inputdata->oilSold }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Gas level (Mcf)</p>
                    <input type="number" name="gaslvl" value="{{ $inputdata->gaslvl }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Gas sold (Mcf)</p>
                    <input type="number" name="gasSold" value="{{ $inputdata->gasSold }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Water level</p>
                    <input type="number" name="waterlvl" value="{{ $inputdata->waterlvl }}" required>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Note</p>
                    <textarea name="note" value="{{ $inputdata->note }}">{{ $inputdata->note }}</textarea>
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Active</p>
                    <select  class="form-control" name="active" required>
                        <option value="{{ $inputdata->active }}">{{ $inputdata->active ? 'YES' : 'NO'}}</option>
                        <option value="1">YES</option>
                        <option value="0">No</option>
                    </select>
                </div>

                <div class="form-row">
                    <input class="btn pull-right border-top" type="submit" value="Update Input Data">
                </div>

            </form>
        </div>
        <!-- .col-lg-4 -->

        <!-- .col-lg-8 -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->
@endsection
