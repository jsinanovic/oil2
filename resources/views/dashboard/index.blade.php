@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
<!-- #mobileMenu -->
 <div class="heading">
    <div class="arrow-left"></div>
    <h1>Dashboard</h1>
 </div>
 <!-- .heading -->
 <div class="panel-inner">
    <div class="col-lg-12 border-top">
       <div class="col-lg-12">
          <div class="col-lg-8">
             <div class="custom-panel" style="margin-top: 0;">
                <div class="col-lg-6">
                   <div class="col-md-6">
                      <div class="red-line">
                         <h1 class="red">{{ $ROI }} <small>+1.07% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                         <p class="text-uppercase">Return on Investment <i class="fa fa-info-circle" aria-hidden="true"></i></p>
                      </div>
                      <!-- .red-line -->
                      <p>Based on investment of ${{ $investments[0]->investment }} and projections for ROI in <strong>{{ $investments[0]->investmentYears }}  years</strong> for complete portfolio (all packages)!</p>
                   </div>
                   <!-- .col-md-6 -->
                   <div class="col-md-6">
                      <div class="chart">
                         <div class="dash-chart-11"></div>
                      </div>
                      <!-- .chart -->
                   </div>
                   <!-- .col-md-6 -->
                </div>
                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                   <div class="col-md-6">
                      <div class="red-line">
                         <h1 class="blue">${{ ( $data[0]->oilSold * 1.08) +  ( $data[0]->gasSold * 1.08 ) }}</h1>
                         <p class="text-uppercase">Total Income</p>
                      </div>
                      <!-- .red-line -->
                      <div class="red-line">
                         <h1 class="blue">{{  sprintf('%0.2f', ((($data[0]->oilSold * 1.08) +  ($data[0]->gasSold * 1.08 ))  * 100 ) / $investments[0]->investment  )}}%</h1>
                         <p class="text-uppercase">Income (%)</p>
                      </div>
                      <!-- .red-line -->
                   </div>
                   <!-- .col-md-6 -->
                   <div class="col-md-6">
                      <div class="chart">
                         <div class="dash-chart-22"></div>
                      </div>
                      <!-- .chart -->
                   </div>
                   <!-- .col-md-6 -->
                </div>
                <!-- .col-lg-6 -->
             </div>
             <!-- .custom-panel -->
          </div>
          <!-- .col-lg-8 -->
          <div>Invest More - Earn More!
                 Request  a cal from our sales representative. Check out your options for additional investments.
             </p>
             <a class="btn" href="tel:1234567890">Request a Call</a>
          </div>
          <!-- .col-lg-4 -->
       </div>
       <!-- .col-lg-12 -->
       <div class="col-lg-12">
          <div class="col-lg-6">
             <header class="sub-header">
                <div class="col-xs-8">
                   <h3 class="blue text-uppercase">Production ($)</h3>
                </div>
                <!-- .col-xs-8 -->
                <div class="col-xs-4">
                   {{--<select>--}}
                      {{--<option value="test">Oil</option>--}}
                   {{--</select>--}}
                    <select id="dashboardProductionSelect" name="dashboardProductionSelect" class="select-header pull-left">
                        <option value="">Filter By</option>
                       {{--<option value="0">Day</option>--}}
                        <option value="1">Monthly</option>
                        <option value="2">Year</option>
                    </select>
                   <i class="fa fa-calendar-o" aria-hidden="true"></i>
                </div>
                <!-- .col-xs-4 -->
             </header>
             <!-- .sub-header -->
             <div class="chart">
                <div class="dash-chart-33"></div>
             </div>
             <!-- .chart -->
             <div class="chart-report">
                <div class="col-md-6">
                   <div class="red-line bg-img oil-bg">
                      <h4 class="gray">Oil Production</h4>
                      <h1>${{ $data[0]->oilSold * 1.08 }}</h1>
                      <h2 class="top-border">{{ $data[0]->oilSold }} bbls</h2>
                   </div>
                   <!-- .red-line -->
                </div>
                <!-- .col-md-6 -->
                <div class="col-md-6">
                   <div class="red-line bg-img gas-bg">
                      <h4 class="gray">Gas Production</h4>
                      <h1>${{ $data[0]->gasSold * 1.08 }}</h1>
                      <h2 class="top-border">{{ $data[0]->gasSold }} Mcf</h2>
                   </div>
                   <!-- .red-line -->
                </div>
                <!-- .col-md-6 -->
             </div>
             <!-- .chart-report -->
          </div>
          <!-- .col-lg-6 -->
          <div class="col-lg-6">
             <header class="sub-header">
                <div class="col-xs-8">
                   <h3 class="blue text-uppercase">Sales ($)</h3>
                </div>
                <!-- .col-xs-8 -->
                <div class="col-xs-4">
                   {{--<select>--}}
                      {{--<option value="test">Gas</option>--}}
                   {{--</select>--}}
                    <select id="dashboardSalesSelect" name="dashboardSalesSelect" class="select-header pull-left">
                        <option value="">Filter By</option>
                        <option value="1">Monthly</option>
                        <option value="2">Year</option>
                    </select>
                   <i class="fa fa-calendar-o" aria-hidden="true"></i>
                </div>
                <!-- .col-xs-4 -->
             </header>
             <!-- .sub-header -->
             <div class="chart">
                <div class="dash-chart-44"></div>
             </div>
             <!-- .chart -->
             <div class="chart-report">
                <div class="col-md-6">
                   <div class="red-line">
                      <h4 class="gray">Total Sales</h4>
                      <h1 class="text-uppercase">${{ ($data[0]->oilSold * 1.08) +  ($data[0]->gasSold * 1.08 ) }} <small>sold</small></h1>
                      <h2 class="blue text-uppercase">97% <small>out of stock (%)</small></h2>
                   </div>
                   <!-- .red-line -->
                </div>
                <!-- .col-md-6 -->
                <div class="col-md-6">
                   <div class="red-line last-line bg-img oil-bg">
                      <h4 class="gray">Oil Sales</h4>
                      <h2>${{ $data[0]->oilSold * 1.08 }}</h2>
                      <h4 class="gray">Gas Sales</h4>
                      <h2>${{ $data[0]->gasSold * 1.08 }} </h2>
                   </div>
                   <!-- .red-line -->
                </div>
                <!-- .col-md-6 -->
             </div>
             <!-- .chart-report -->
          </div>
          <!-- .col-lg-6 -->
       </div>
       <!-- .col-lg-12 -->
    </div>
    <!-- .col-lg-12 -->
 </div>
 <!-- .panel-inner -->
 <div class="col-lg-12 bottom-section">
    <div class="col-lg-4 res-margin">
       <h4 class="blue">Make a Comparison</h4>
       <p>Check performance of each well with comparative analysis section.</p>
       <a class="btn" href="/comperative">Comparative Analysis</a>
    </div>
    <!-- .col-lg-4 -->
    <div class="col-lg-4 res-margin">
       <h4 class="blue">Export Your Report</h4>
       <p>Get the report in proper format to share it with stakeholders easily!</p>
       <a class="btn" href="/reporting">Reports Section</a>
    </div>
    <!-- .col-lg-4 -->
    <div class="col-lg-2 res-margin">
       <div class="red-line last-line bg-img oil-bg-gray">
          <p class="gray">Oil Market Price</p>
          <h2>${{ $settings[0]->currentOilRate }}/bbl</h2>
          {{--<h4>+1.07% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></h4>--}}
       </div>
       <!-- .red-line -->
    </div>
    <!-- .col-lg-4 -->
    <div class="col-lg-2 res-margin">
       <div class="red-line last-line bg-img gas-bg-gray">
          <p class="gray">Gas Market Price</p>
          <h2>${{ $settings[0]->currentGasRate }}/Mcf</h2>
          {{--<h4>+0.87% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></h4>--}}
       </div>
       <!-- .red-line -->
    </div>
    <!-- .col-lg-4 -->
 </div>
 <!-- .col-lg-12 -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        // Dashboard pie chart 22
        var string = "{{ ((($data[0]->oilSold * 1.08) +  ($data[0]->gasSold * 1.08 ))  * 100 ) / $investments[0]->investment   }}";

        var data = {series: [Number(string), 100] };

        var sum = function (a, b) {
            return a + b
        };

        new Chartist.Pie('.dash-chart-22', data, {
            labelInterpolationFnc: function (value) {
                return Math.round(value / data.series.reduce(sum) * 100) + '%';
            }
        });

        // Dashboard pie chart 22
        var d = "{{ $ROI }}";
        if(d < 0)
            var data1 = {series: [0.1, 100]};
        else
            var data1 = {series: [d, 100]};

        var sum = function (a, b) {
            return a + b
        };

        new Chartist.Pie('.dash-chart-11', data1, {
            labelInterpolationFnc: function (value) {
                return Math.round(value / data1.series.reduce(sum) * 100) + '%';
            }
        });

//       // Dashboard sales chart
//       new Chartist.Line('.dash-chart-44', {
//           labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
//           series: [
//               [12, 9, 7, 8, 5, 6, 6]
//           ]
//       }, {
//           fullWidth: true,
//           chartPadding: {
//               right: 40
//           }
//       });
//
//       // Dashboard sales chart
//       new Chartist.Line('.dash-chart-33', {
//           labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
//           series: [
//               [12, 9, 7, 8, 5, 6, 6]
//           ]
//       }, {
//           fullWidth: true,
//           chartPadding: {
//               right: 40
//           }
//       });


        // Oil well profile oil production chart
        new Chartist.Line('.dash-chart-33', {
            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
            series: [
                [ "{{ $productionByMonth[0]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[1]['infos'][0]->oliProduced * 1.08}}",
                    "{{ $productionByMonth[2]['infos'][0]->oliProduced * 1.08}}",
                    "{{ $productionByMonth[3]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[4]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[5]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[6]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[7]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[8]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[9]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[10]['infos'][0]->oliProduced  * 1.08}}",
                    "{{ $productionByMonth[11]['infos'][0]->oliProduced  * 1.08}}",
                ],
                [ "{{ $productionByMonth[0]['infos'][0]->gasProduced  *   1.08}}",
                    "{{ $productionByMonth[1]['infos'][0]->gasProduced *  1.08}}",
                    "{{ $productionByMonth[2]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[3]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[4]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[5]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[6]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[7]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[8]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[9]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[10]['infos'][0]->gasProduced  * 1.08}}",
                    "{{ $productionByMonth[11]['infos'][0]->gasProduced  * 1.08}}",
                ]
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                right: 40
            }
        });

        // Oil well profile gas production chart
        new Chartist.Line('.dash-chart-44', {
            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
            series: [
                [ "{{ $productionByMonth[0]['infos'][0]->oilSold * 1.08}}",
                    "{{ $productionByMonth[1]['infos'][0]->oilSold * 1.08}}",
                    "{{ $productionByMonth[2]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[3]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[4]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[5]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[6]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[7]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[8]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[9]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[10]['infos'][0]->oilSold  * 1.08}}",
                    "{{ $productionByMonth[11]['infos'][0]->oilSold  * 1.08}}",
                ],

                [ "{{ $productionByMonth[0]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[1]['infos'][0]->gasSold * 1.08}}",
                    "{{ $productionByMonth[2]['infos'][0]->gasSold * 1.08}}",
                    "{{ $productionByMonth[3]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[4]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[5]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[6]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[7]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[8]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[9]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[10]['infos'][0]->gasSold  * 1.08}}",
                    "{{ $productionByMonth[11]['infos'][0]->gasSold  * 1.08}}",
                ]
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                right: 40
            }
        });



    });
</script>


@endsection



