@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Admin Section</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
        <div id="info" style="text-align: center; width: auto"></div>
        <div class="clearfix"></div>

        <div class="col-lg-12 border-top border-bottom">
            <div class="col-lg-4">
                <form class="form-horizontal" enctype="multipart/form-data" id="addAdminForm" role="form" method="POST" >
                    {!! csrf_field() !!}
                    <div class="form-row">
                        <p>Notifications</p>
                        <textarea name="note" required></textarea>
                    </div>
                    <div class="form-row">
                        {{--<input class="btn pull-right border-top" type="submit" value="Save">--}}
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

