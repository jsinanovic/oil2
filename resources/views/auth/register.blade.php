<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>EAF - Register</title>
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="{{ asset("css/style-z.css") }}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body class="login-main">
<div class="wrapper" style="background-image: url(images/main.jpg)">
    <div class="logo">
        <a href="#" title="Investor Portal"><img src="{{asset("images/main-logo.png")}}" alt="logo"></a>
    </div>
    <!-- .logo -->
    <div class="form-row border-top">
        <h1 class="text-center">Register</h1>
    </div>

    <!-- .form-row -->
    <div class="container">
        <div id="info" style="text-align: center; width: auto"></div>
        <div class="clearfix"></div>
        <form class="form-horizontal" enctype="multipart/form-data" id="registerUserForm" role="form" method="POST" >
            {{--<form method="post" action="{{ url('api/v1/addnewuser') }}">--}}
            {!! csrf_field() !!}

            <div class="col-lg-12">
                <div class="col-lg-6">
                    <div class="form-row">
                        <p>Email</p>
                        <input type="email" value="" name="email" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>First Name</p>
                        <input type="text" value="" name="name" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Last Name</p>
                        <input type="text" value="" name="lname" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Permissions</p>
                        <select type="permissions" name="role" class="form-control" required>
                            <option value="">Select Role</option>
                            @foreach ($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>ZIP</p>
                        <input type="number" value="" name="zip" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>City</p>
                        <input type="text" value="" name="city" required>
                    </div>

                    <div class="form-row">
                        <p>Phone</p>
                        <input type="text" value="" name="phone" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>State</p>
                        <select name="state" required>
                            <option value="">Select state</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>
                    </div>
                    <!-- .form-row -->
                </div>
                <!-- .col-lg-6 -->
                <div class="col-lg-6">
                    <div class="form-row">
                        <p>Password</p>
                        <input type="password" value="" name="password" required>
                    </div>

                    <div class="form-row">
                        <p>Confirm Password</p>
                        <input type="password" name="password_confirmation" required>
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Oil Well Package</p>
                        <select type="role" class="form-control" name="packageid">
                            <option value="">Select Package</option>
                            @foreach ($packages as $package)
                                <option value="{{$package->id}}">{{$package->name}}</option>
                            @endforeach
                        </select>

                        {{--<input type="text" value="" name="oilwell">--}}
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Investment $</p>
                        <input type="number" value="" name="investment" required>
                    </div>
                    <!-- .form-row -->
                    <div class="form-row">
                        <p>Type of Investment (yrs)</p>
                        <input type="number" name="investmentYears" value="" required>
                    </div>
                    <div class="form-row">
                        <p>Upload profile picture</p>
                        <input type="file" name="profile_pic" accept="image/*" required>
                    </div>

                    <!-- .form-row -->
                    <div class="form-row">
                        <input class="btn" type="submit" value="Add a new user">
                    </div>


                    <!-- .form-row -->
                </div>
                <!-- .col-lg-6 -->
            </div>
            <!-- .col-lg-12 -->
        </form>
    </div>
    <!-- .login-reg-form -->
</div>
<!-- .wrapper -->
<footer>
    <p class="text-center">Copyright &copy; 2017</p>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/userRegister.js') }}"></script>
</body>
</html>
