<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>EAF - Login</title>
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="{{ asset("css/style-z.css") }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body class="login-main">
<div class="wrapper" style="background-image: url(images/main.jpg)">
    <div class="logo">
        <a href="#" title="Investor Portal"><img src="{{asset("images/main-logo.png")}}" alt="logo"></a>
    </div>
    <!-- .logo -->
    <div class="form-row border-top">
        <h1 class="text-center">Login</h1>
    </div>
    <!-- .form-row -->
    <div class="login-reg-form container">
        <div class="col-lg-12">
            <form method="post" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <div class="form-row">
                    <p>Enter your email address</p>
                    <input type="text" name="email" value="" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                         </span>
                    @endif
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <p>Enter your password</p>
                    <input type="password" name="password" value="" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                         </span>
                    @endif
                </div>
                <!-- .form-row -->
                <div class="form-row">
                    <input type="submit" class="btn pull-right" value="Login">
                </div>
                <!-- .form-row -->
            </form>
            <div class="clearfix"></div>
        </div>
        <!-- .col-lg-12 -->
    </div>
    <!-- .login-reg-form -->
</div>
<!-- .wrapper -->
<footer>
    <p class="text-center">Copyright &copy; 2017</p>
</footer>
<!-- jQuery lib -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
