@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')
    <div class="panel-inner">
        <div class="heading">
            <div class="arrow-left"></div>
            <h1>Users Settings</h1>
        </div>

        <!-- .col-lg-12 -->
        <div class="col-lg-12">
            <div class="col-md-8">

                <!-- .form-row -->
                <div id="info" style="text-align: center; width: auto;"></div>
                <div class="clearfix"></div>

        <form class="form-horizontal" enctype="multipart/form-data" id="addSettings" role="form" method="POST">
            {!! csrf_field() !!}

            <div class="form-row" style="display: none">
                <input type="number" name="settingsID" value="{{ $settings[0]->id }}">
            </div>

            <div class="form-row">
                <p>Currency</p>
                <select type="role" class="form-control" name="currency"  required>
                        <option value="$">$</option>
                </select>
                {{--<input type="text" name="currency">--}}
            </div>

            <div class="form-row">
                <p>Current Gas Rate</p>
                <input type="number" name="currentGasRate"  placeholder="Gas Rate bbl" value="{{ $settings[0]->currentGasRate }}" step="any" required>
            </div>

            <div class="form-row">
                <p>Current Oil Rate</p>
                <input type="number" name="currentOilRate" placeholder="Oli Rate bbl" value="{{ $settings[0]->currentOilRate }}" step="any" required>
            </div>

            <!-- .form-row -->
            <div class="form-row">
                <input type="submit" value="Add/Update Settings" class="btn">
            </div>

            <!-- .form-row -->
            <div id="info" style="text-align: center;"></div>
            <div class="clearfix"></div>
        </form>

            </div>
            </div>
    </div>
@endsection
