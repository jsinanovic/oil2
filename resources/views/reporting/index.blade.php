@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <div class="heading">
        <div class="arrow-left"></div>
        <h1>Reports</h1>
    </div>
    <!-- .heading -->
    <div class="panel-inner">
    <div class="col-lg-12">
        <div class="col-md-8">
            <div class="table-holder custom-panel padding-lr">
                <header>
                    <h1 class="pull-left">Oil Well Reports</h1>
                    <div class="pull-right">
                        <select class="pull-left move-right">
                            <option value="bbl">bbl/Mcf</option>
                        </select>
                        <p class="pull-left">
                            Export as
                            <select id="reportingExport" type="reportingExport" name="reportingExport" >
                                <option value="pdf">Select</option>
                                <option id="reportingExport" value="pdf">PDF</option>
                            </select>
                        </p>
                    </div>
                    <!-- .pull-right -->
                </header>
                <table class="table">
                    <thead>
                    <th>WELL PACKAGE</th>
                    <th>OIL STOCK(bbl)</th>
                    <th>OIL SOLD(bbl)</th>
                    <th>GAS STOCK(Mcf)</th>
                    <th>GAS SOLD(Mcf)</th>
                    <th>OIL PRODUCED(bbl)</th>
                    <th>GAS PRODUCED(Mcf)</th>
                    </thead>

                    @foreach ($InputdataForTanks as $InputdataForTank)
                        <tr>
                            <td>{{ $InputdataForTank['packages']->name }}</td>
                            <td>{{ $InputdataForTank['oilStock'] }}</td>
                            <td>{{ $InputdataForTank['oilsolds'] }}</td>
                            <td>{{ $InputdataForTank['gasStock'] }}</td>
                            <td>{{ $InputdataForTank['gasSoldes'] }}</td>
                            <td>{{ $InputdataForTank['oliProduced'] }}</td>
                            <td>{{ $InputdataForTank['gasProduced'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- .table-holder -->
        </div>
        <!-- .col-md-8 -->
        <div class="col-md-4 border-top">
            <div class="numbers col-lg-6">
                <div class="number-lg red-line">
                    <p>Total production</p>
                    <h1>${{$InputdataForTank['totalOilProduction'] * 1.08 }} </h1>
                </div>
                <!-- .number-lg -->
                <div class="number-sm red-line bg-img-2 oil-bg">
                    <p>Oil sales</p>
                    <h1>${{$InputdataForTank['totalOilSold'] * 1.08 }}</h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line">
                    <p>Oil production</p>
                    <h1>${{$InputdataForTank['oliProduced'] * 1.08 }}</h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line">
                    <p>Oil in stock</p>
                    <h1>${{ $InputdataForTank['totalOilStock'] * 1.08 }}</h1>
                </div>
                <!-- .number-sm -->
            </div>
            <!-- .numbers -->
            <div class="numbers col-lg-6">
                <div class="number-lg red-line">
                    <p>Total sales</p>
                    <h1>${{$InputdataForTank['totalGasProduction'] * 1.08 }}</h1>
                </div>
                <!-- .number-lg -->
                <div class="number-sm red-line bg-img-2 gas-bg">
                    <p>Gas sales</p>
                    <h1>${{$InputdataForTank['totalGasSold'] * 1.08 }}</h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line">
                    <p>Gas production</p>
                    <h1>${{$InputdataForTank['gasProduced'] * 1.08 }}</h1>
                </div>
                <!-- .number-sm -->
                <div class="number-sm red-line">
                    <p>Gas in stock</p>
                    <h1>${{$InputdataForTank['totalGasStock'] * 1.08 }}</h1>
                </div>
                <!-- .number-sm -->
            </div>
            <!-- .numbers -->
            <div class="custom-panel col-lg-12">
                <h4>Rise in production this year!</h4>
                <p>Compared to previous year, oil wells in your portfolio are overperforming!</p>
                <p>1.07% <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></p>
            </div>
            <!-- .custom-panel -->
        </div>
        <!-- .col-md-4 -->
    </div>
    <!-- .col-lg-12 -->
    <div class="col-lg-12">
        <div class="col-lg-12">
            <div class="table-holder custom-panel padding-lr">
                <header>
                    <h1 class="pull-left">Monthly Production & Sales Report</h1>
                    <div class="pull-right">
                        <select class="pull-left move-right">
                            <option value="bbl">bbl/Mcf</option>
                        </select>
                        <p class="pull-left">
                            Export as a
                            <select id="reportingExport1" type="reportingExport1" name="reportingExport1" >
                                <option value="pdf">Select</option>
                                <option id="reportingExport1" value="pdf">PDF</option>
                            </select>
                        </p>
                    </div>
                    <!-- .pull-right -->
                </header>
                <table class="table">
                    <thead>
                    <th>WELL PACKAGE</th>
                    <th>OIL STOCK(bbl)</th>
                    <th>OIL SOLD(bbl)</th>
                    <th>GAS STOCK(Mcf)</th>
                    <th>GAS SOLD(Mcf)</th>
                    <th>OIL PRODUCED(bbl)</th>
                    <th>GAS PRODUCED(Mcf)</th>
                    </thead>
                    @foreach ($InputdataForTanks as $InputdataForTank)
                        <tr>
                            <td>{{ $InputdataForTank['packages']->name }}</td>
                            <td>{{ $InputdataForTank['oilStock'] }}</td>
                            <td>{{ $InputdataForTank['oilsolds'] }}</td>
                            <td>{{ $InputdataForTank['gasStock'] }}</td>
                            <td>{{ $InputdataForTank['gasSoldes'] }}</td>
                            <td>{{ $InputdataForTank['oliProduced'] }}</td>
                            <td>{{ $InputdataForTank['gasProduced'] }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- .table-holder -->
        </div>
        <!-- .col-lg-12 -->
    </div>
    <!-- .col-lg-12 -->
    <div class="col-lg-12 border-top">
        <div class="col-md-8">
            <header class="sub">
                <h3 class="blue text-uppercase">Sales & Production Overview ($)</h3>
            </header>
            <div class="chart">
                <div class="reports-chart1"></div>
            </div>
            <!-- .chart -->
        </div>
        <!-- .col-md-8 -->
        <div class="col-md-4">
          <div class="custom-panel">
             <div class="col-md-6">
                <div class="chart">
                   <div class="reports-pie-chart-11"></div>
                </div>
                <!-- .chart -->
             </div>
             <!-- .col-md-6 -->
             <div class="col-md-6">
                <div class="red-line text-uppercase">
                    @if(($InputdataForTank['oliProduced']   + $InputdataForTank['gasProduced'] ) > 0)
                   <h4>Sold<br> <span class="blue">{{ sprintf('%0.2f', ((($InputdataForTank['oilsolds']  * 1.08) +  ($InputdataForTank['gasSoldes'] * 1.08 ))  * 100 ) / ($InputdataForTank['oliProduced']   + $InputdataForTank['gasProduced'] ) )  }}%</span><br> of production (%)</h4>
                    @else
                        <h4>Sold<br>$0 <span class="blue"></span></h4>
                    @endif
                </div>
                <!-- .red-line -->
             </div>
             <!-- .col-md-6 -->
          </div>
          <!-- .custom-panel -->
        </div>
        <!-- .col-md-4 -->
    </div>
    <!-- .col-lg-12 -->
    <div class="col-lg-12">
        <div class="col-md-4">
            <div class="table-holder custom-panel padding-lr">
                <header>
                    <h4 class="pull-left">Investor earning</h4>
                    <div class="pull-right">
                        <select class="pull-left move-right">
                            <option value="yearly">Yearly</option>
                        </select>
                        <p class="pull-left">
                            Export as
                            <select id="reportingExport">
                                <option value="pdf"></option>
                                <option value="pdf">PDF</option>
                            </select>
                        </p>
                    </div>
                    <!-- .pull-right -->
                </header>
                {{--<table class="table">--}}
                    {{--<thead>--}}
                    {{--<th>YEAR</th>--}}
                    {{--<th>SALES</th>--}}
                    {{--<th>INVESTOR INCOME ($)</th>--}}
                    {{--</thead>--}}
                    {{--<tr>--}}
                        {{--<td>2014</td>--}}
                        {{--<td>1,260,000</td>--}}
                        {{--<td>800,050</td>--}}
                    {{--</tr>--}}
                {{--</table>--}}
            </div>
            <!-- .table-holder -->
        </div>
        <!-- .col-md-4 -->
        <div class="col-md-8">
            <div class="custom-panel">
                <div class="col-md-8">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="red-line text-uppercase">
                           <h1 class="red">{{ $ROI }}% <small> <span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small></h1>
                           <p>Return on investment</p>
                        </div>
                        <!-- .red-line -->
                        <p>Based on investment of <strong>${{ $investments[0]->investment }}</strong> and projections for ROI in <strong>{{ $investments[0]->investmentYears }} years</strong>.</p>
                     </div>
                     <!-- .col-md-6 -->
                     <div class="col-md-6">
                        <div class="chart">
                           <div class="reports-pie-chart-22"></div>
                        </div>
                        <!-- .chart -->
                     </div>
                     <!-- .col-md-6 -->
                  </div>
                  <!-- .row -->
                </div>
                <!-- .col-md-8 -->
                <div class="col-md-4">
                    <div class="red-line text-uppercase">

                        <h1 class="blue">${{ ($InputdataForTank['oilsolds'] * 1.08) +  ($InputdataForTank['gasSoldes'] * 1.08 ) }}</h1>
                        <p>Total income</p>
                    </div>
                    <!-- .red-line -->
                    <div class="red-line text-uppercase">

                        <h1 class="blue">{{  sprintf('%0.2f', ((($InputdataForTank['oilsolds'] * 1.08) +  ($InputdataForTank['gasSoldes'] * 1.08 ))  * 100 ) / $investments[0]->investment  )}}%</h1>
                        <p>Income</p>
                    </div>
                    <!-- .red-line -->
                </div>
                <!-- .col-md-4 -->
            </div>
            <!-- .custom-panel -->
        </div>
        <!-- .col-md-8 -->
    </div>
    <!-- .col-lg-12 -->
  </div>
  <!-- .panel-inner -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            // Dashboard pie chart 22
            var string = "{{ ((($InputdataForTank['oilsolds']  * 1.08) +  ($InputdataForTank['gasSoldes'] * 1.08 ))  * 100 ) / $investments[0]->investment }}";

            var data = {
                series: [Number(string), 100]
            };

            var sum = function(a, b) { return a + b };

            new Chartist.Pie('.reports-pie-chart-22', data, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data.series.reduce(sum) * 100) + '%';
                }
            });

            var d = "{{ ((($InputdataForTank['oilsolds']  * 1.08) +  ($InputdataForTank['gasSoldes'] * 1.08 ))  * 100 )   }}";
            var devide = "{{  $InputdataForTank['oliProduced']   + $InputdataForTank['gasProduced']  }}";

            if( devide > 0) {
                        {{--var string = "{{ ((($InputdataForTank['oilsolds']  * 1.08) +  ($InputdataForTank['gasSoldes'] * 1.08 ))  * 100 ) / $InputdataForTank['oliProduced']   + $InputdataForTank['gasProduced'] }}";--}}
                var string = d / devide;
            }  else {
                var string = 0;
            }
            var data1 = {
                series: [Number(string), 100]
            };

            var sum = function(a, b) { return a + b };

            new Chartist.Pie('.reports-pie-chart-11', data1, {
                labelInterpolationFnc: function(value) {
                    return Math.round(value / data1.series.reduce(sum) * 100) + '%';
                }
            });


            // Reports sales and production overview
            new Chartist.Bar('.reports-chart1', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [ "{{ $InputdataForTanksMonths[0]['infos'][0]->oilSold +  $InputdataForTanksMonths[0]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[1]['infos'][0]->oilSold +  $InputdataForTanksMonths[1]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[2]['infos'][0]->oilSold +  $InputdataForTanksMonths[2]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[3]['infos'][0]->oilSold +  $InputdataForTanksMonths[3]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[4]['infos'][0]->oilSold +  $InputdataForTanksMonths[4]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[5]['infos'][0]->oilSold +  $InputdataForTanksMonths[5]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[6]['infos'][0]->oilSold +  $InputdataForTanksMonths[6]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[7]['infos'][0]->oilSold +  $InputdataForTanksMonths[7]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[8]['infos'][0]->oilSold +  $InputdataForTanksMonths[8]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[9]['infos'][0]->oilSold +  $InputdataForTanksMonths[9]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[10]['infos'][0]->oilSold +  $InputdataForTanksMonths[10]['infos'][0]->gasSold}}",
                        "{{ $InputdataForTanksMonths[11]['infos'][0]->oilSold +  $InputdataForTanksMonths[11]['infos'][0]->gasSold}}",
                    ]
                ]
            }, {
                stackBars: true,
                axisY: {
                    labelInterpolationFnc: function(value) {
                        return (value / 1) + '$';
                    }
                }
            }).on('draw', function(data) {
                if(data.type === 'bar') {
                    data.element.attr({
                        style: 'stroke-width: 30px'
                    });
                }
            });

            $('#reportingExport1').on('change', function() {
                setTimeout(window.location.href = "/exportPDF" ,10);
            });
        });
    </script>

    <script>
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer",
                {
                    title:{
                        text: "Sales & Production Overview"
                    },
                    animationEnabled: true,
                    axisY: {
                        title: "Reserves(MMbbl)"
                    },
                    legend: {
                        verticalAlign: "bottom",
                        horizontalAlign: "center"
                    },
                    theme: "theme2",
                    data: [

                        {
                            type: "column",
                            showInLegend: true,
                            legendMarkerColor: "grey",
                            legendText: "MMbbl = one million barrels",
                            dataPoints: [
                                {y: 297571, label: "January"},
                                {y: 267017,  label: "Februry" },
                                {y: 175200,  label: "March"},
                                {y: 154580,  label: "April"},
                                {y: 116000,  label: "May"},
                                {y: 97800, label: "June"},
                                {y: 20682,  label: "July"},
                                {y: 20350,  label: "August"},
                                {y: 20350,  label: "September"},
                                {y: 20350,  label: "October"},
                                {y: 20350,  label: "November"},
                                {y: 20350,  label: "December"}
                            ]
                        }
                    ]
                });

            chart.render();
        }
    </script>
    <script src="js/canvasjs.min.js"></script>
@endsection


