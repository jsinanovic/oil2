<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>EAF - Dashboard</title>
    <link href="{{ asset("css/style-z.css") }}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{asset("images/favicon.png")}}">
      {{--<link href="{{ asset('css/style-z.css') }}" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Charts JS CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
</head>
<body>
<div class="wrapper">
    <div class="sidebar pull-left">
        <div class="logo">
            <a href="" title="EAF Investor Portal"><img  src="{{ asset("images/logo.png") }}"  alt="logo"></a>
        </div>
        <!-- .logo -->
        <nav class="nav">
            <ul>
                <li><a href="/dashboard"><img src="images/icons/1.png"></a></li>
                <li><a href="/packages"><img src="images/icons/2.png"></a></li>
                <li><a href="/oil-wells"><img src="images/icons/3.png"></a></li>
                <li><a href="/input-data"><img src="images/icons/4.png"></a></li>
                <li><a href="/user-managment"><img src="images/icons/5.png"></a></li>
                <li><a href="/reporting"><img src="images/icons/6.png"></a></li>
                <li><a href="#"><img src="images/icons/7.png"></a></li>
                <li><a href="/help"><img src="images/icons/8.png"></a></li>
            </ul>
        </nav>
        <!-- .nav -->
    </div>
    <!-- .sidebar -->
    <div class="main-panel pull-left">
        <header class="main-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobileMenu" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- .navbar-toggle -->
            <nav id="navbar" class="top-nav pull-left">
                <ul class="nav navbar-nav">
                    <li><a href="#">+ Add User</a></li>
                    <li><a href="#">User Preferences</a></li>
                    <li><a href="#">Settings</a></li>
                </ul>
            </nav>
            <!-- #navbar -->
            <div id="user" class="pull-right">
                <div class="dropdown">
                    <a class="" data-toggle="dropdown"><img src="images/avatar.png" alt="Profile"> <span class="name">Zeljko Radic <i class="fa fa-caret-down" aria-hidden="true"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Notifications <span class="not">9</span></a></li>
                        <li><a href="#">Logout</a></li>
                    </ul>
                </div>
                <!-- .dropdown -->
            </div>
            <!-- #user -->
        </header>
        <!-- .main-header -->
        <nav id="mobileMenu" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="#">+ Add User</a></li>
                <li><a href="#">User Preferences</a></li>
                <li><a href="#">Settings</a></li>
            </ul>
        </nav>

            @yield('main_container')

    </div>
    <!-- .main-panel -->
</div>
<!-- .wrapper -->
<!-- jQuery lib -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<!-- Charts -->
<script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<script src="{{asset("js/charts.js")}}" ></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
