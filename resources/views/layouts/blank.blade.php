<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>EAF - Dashboard</title>
    <link href="{{ asset("css/style-z.css") }}" rel="stylesheet">

    <link href="{{ asset("css/daterangepicker.css") }}" rel="stylesheet">
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">

    <link rel="icon" type="image/png" href="{{asset("images/favicon.png")}}">
    {{--<link href="{{ asset('css/style-z.css') }}" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Charts JS CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

    <link href="{{ asset("css/style-z.css") }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
</head>
<body>
<div class="wrapper">
    <div class="sidebar pull-left">
        <div class="logo">
            <a href="/dashboard" title="EAF Investor Portal"><img  src="{{ asset("images/logo.png") }}"  alt="logo"></a>
        </div>
        <!-- .logo -->
        <nav class="nav">
            <ul>
                <li><a href="/dashboard"><img src="{{asset("images/icons/1.png")}}"></a></li>
                <li><a href="/packages"><img src="{{asset("images/icons/2.png")}}"></a></li>
                <li><a href="/oil-wells"><img src="{{asset("images/icons/3.png")}}"></a></li>
                <li><a href="/input-data"><img src="{{asset("images/icons/4.png")}}"></a></li>
                <li><a href="/reporting"><img src="{{asset("images/icons/5.png")}}"></a></li>
                <li><a href="/comparative"><img src="{{asset("images/icons/6.png")}}"></a></li>
                <li><a href="/editUser/{{ Auth::user()->id }}"><img src="{{asset("images/icons/7.png")}}"></a></li>
                <li><a href="/help"><img src="{{asset("images/icons/8.png")}}"></a></li>
            </ul>
        </nav>
        <!-- .nav -->
    </div>
    <!-- .sidebar -->
    <div class="main-panel pull-left">
        <header class="main-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobileMenu" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- .navbar-toggle -->
            <nav id="navbar" class="top-nav pull-left">
                <ul class="nav navbar-nav">
                    <li><a href="/user-managment">+ Add User</a></li>
                    <li><a href="/settings">Settings</a></li>
                </ul>
            </nav>
            <!-- #navbar -->
            <div id="user" class="pull-right">
                <div class="dropdown">
                    <a class="" data-toggle="dropdown"><img style="border-radius: 30px; " src="{{ asset("images/profile/resized/".  Auth::user()->profile_pic ) }}" alt="Profile"> <span class="name">{{ Auth::user()->name }}<i class="fa fa-caret-down" aria-hidden="true"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Notifications <span class="not"></span></a></li>
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </div>
                <!-- .dropdown -->
            </div>
            <!-- #user -->
        </header>
        <!-- .main-header -->
        <nav id="mobileMenu" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/user-managment">+ Add User</a></li>
                <li><a href="/settings">Settings</a></li>
            </ul>
        </nav>

        @yield('main_container')

    </div>
    <!-- .main-panel -->
</div>
<!-- .wrapper -->
<!-- jQuery lib -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<!-- Charts -->
<script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<script src="{{asset("js/charts.js")}}" ></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{ asset('js/user2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>


@stack('scripts')

</body>
</html>