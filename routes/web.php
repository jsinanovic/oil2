<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();

Route::get('/admin', 'AdminController@index');

Route::get('/registerUser', 'Auth\RegisterController@indexRegister');
Route::post('/registerUserNow', 'Auth\RegisterController@addUser');

Route::get('/', 'HomeController@indexDashboard');
Route::get('/404', 'HomeController@page404');
Route::get('/500', 'HomeController@page500');

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/adduser', 'UserController@index');
Route::get('/user-managment', 'UserController@indexUserManagment');
Route::get('/dashboard', 'HomeController@indexDashboard');
Route::get('/oil-wells', 'HomeController@indexOilwells');
Route::get('/input-data', 'HomeController@indexInputData');
Route::get('/reporting', 'HomeController@indexReporting');
Route::get('/comparative', 'HomeController@indexComperativeAnalysis');
Route::get('/help', 'HomeController@indexHelp');
Route::get('/settings', 'HomeController@settings');

Route::post('/addinvestment', 'UserController@addUserInvestment');
Route::get('/packages', 'HomeController@indexPackages');
Route::get('/editPackage/{id}', 'HomeController@editPackage');
Route::get('/addpackage', 'HomeController@addPackageView');
Route::get('/remove-package/{id}', 'HomeController@removePackage');

Route::get('/addOilWell', 'HomeController@addOilWell');
Route::get('/editoilwells/{id}', 'HomeController@editOilWells');
Route::get('/deliteoilwells/{id}', 'HomeController@deliteOilWells');
Route::get('/openOilwellProfile/{id}', 'HomeController@openOilwellProfile');

Route::get('/openPackage/{id}', 'HomeController@openPackage');
Route::get('/openOilwellTanks/{id}', 'HomeController@openOilWellTanks');
Route::get('/openOilWellTankForm/{id}', 'HomeController@openOilWellTankForm');
Route::get('/tank-details/{id}', 'HomeController@tankDetails');
Route::get('/openPackageProfile/{id}', 'HomeController@openPackageProfile');

Route::get('/oilWell-list', 'HomeController@tankDetails');
Route::get('/comperative0', 'HomeController@indexComperativeAnalysisOnChange0');

Route::get('/editUser/{id}', 'UserController@editUser');
Route::get('/input-data/{id}', 'HomeController@editInputData');

Route::get('exportPDF', 'ExportController@exportPDF');
Route::get('getOilWellByMonth', 'HomeController@getOilWellByMonth');
Route::get('getPackageByMonth', 'HomeController@getPackageByMonth');
Route::get('getdashboardByMonth', 'HomeController@getdashboardByMonth');
Route::get('getdashboardByMonthSales', 'HomeController@getdashboardByMonthSales');

/**
 * API V1
 */
Route::group(['prefix' => 'api/v1'], function () {

    /**
     * AUTH ROUTES
     */
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/logout', 'Auth\LoginController@logout');
    });



    Route::post('/addnewuser', 'UserController@addUser');
    Route::post('/addinvestment', 'UserController@addUserInvestment');
    Route::post('/addoilwells', 'HomeController@addOilWells');
    Route::post('/updateoilwells', 'HomeController@updateOilWells');
    Route::post('/updatepackage', 'HomeController@updatePackage');
    Route::post('/addoilwelltank', 'HomeController@addNewoilwellTank');
    Route::post('/packages', 'HomeController@addPackage');
    Route::post('/addinputdata', 'HomeController@addInputData');

    Route::post('/addsettings', 'HomeController@addSettings');

    Route::post('/updateUser', 'UserController@updateUser');
    Route::post('/updateinputdata', 'HomeController@updateInputData');
});