window.onload = function() {
  // Dashboard pie chart 1
  var data = {
    series: [3, 4]
  };

  var sum = function(a, b) { return a + b };

  new Chartist.Pie('.dash-chart-1', data, {
    labelInterpolationFnc: function(value) {
      return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
  });

  // Dashboard pie chart 2
  var data = {
    series: [3, 4]
  };

  var sum = function(a, b) { return a + b };

  new Chartist.Pie('.dash-chart-2', data, {
    labelInterpolationFnc: function(value) {
      return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
  });

  // Dashboard production chart
  new Chartist.Line('.dash-chart-3', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [2, 1, 3.5, 7, 3, 1, 5]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Dashboard sales chart
  new Chartist.Line('.dash-chart-4', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [12, 9, 7, 8, 5, 6, 6]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Comparative analysis chart
  new Chartist.Line('.ca-chart', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [12, 9, 7, 8, 5, 6, 6]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Reports sales and production overview
  new Chartist.Bar('.reports-chart', {
    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
    series: [
      [800000, 1200000, 1400000, 1300000, 110000, 110000, 110000, 110000, 110000, 110000, 110000, 110000]
    ]
  }, {
    stackBars: true,
    axisY: {
      labelInterpolationFnc: function(value) {
        return (value / 2000) + 'k';
      }
    }
  }).on('draw', function(data) {
    if(data.type === 'bar') {
      data.element.attr({
        style: 'stroke-width: 30px'
      });
    }
  });

  // Reports pie chart 1
  var data = {
    series: [3, 4]
  };

  var sum = function(a, b) { return a + b };

  new Chartist.Pie('.reports-pie-chart-1', data, {
    labelInterpolationFnc: function(value) {
      return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
  });

  // Reports pie chart 2
  var data = {
    series: [3, 4]
  };

  var sum = function(a, b) { return a + b };

  new Chartist.Pie('.reports-pie-chart-2', data, {
    labelInterpolationFnc: function(value) {
      return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
  });

  // Oil well profile oil production chart
  new Chartist.Line('.owp-chart-1', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [2, 1, 3.5, 7, 3, 1, 5]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Oil well profile gas production chart
  new Chartist.Line('.owp-chart-2', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [2, 1, 3.5, 7, 3, 1, 5]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Oil well profile (detailed view) oil production chart
  new Chartist.Line('.owpd-chart-1', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [2, 1, 3.5, 7, 3, 1, 5]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Oil well profile (detailed view) gas production chart
  new Chartist.Line('.owpd-chart-2', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [2, 1, 3.5, 7, 3, 1, 5]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

}
