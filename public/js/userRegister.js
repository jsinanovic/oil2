

/////////////////////////////////////add user REQUEST//////////////////////////////////////////////////////////////////
$('#registerUserForm').submit(function (event) {

    $.ajax({
        type: 'POST',
        url: '/registerUserNow',
        // dataType: 'json',
        dataType: 'text json',
        contentType: false,
        processData: false,
        data: new FormData($("#registerUserForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You are successfully registered. You will be redirected to login screen." + ' !</div>');

            // $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User');

            jQuery('html,body').animate({scrollTop:0},0);
            $('#registerUserForm')[0].reset();

            //
            // setTimeout(' location.location.href= "/login" ', 4000);
            // setTimeout(' location.reload("/login"); ', 4000);
            setTimeout(window.location.href = "/" ,5000);
            //setTimeout(window.location.href = "/login" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');
                setTimeout(window.location.href = "/" ,5000);


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }


            $('#registerUserForm')[0].reset();
        }
    });
    return false;
});