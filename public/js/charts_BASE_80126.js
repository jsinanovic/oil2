window.onload = function() {
    // Dashboard percentage chart
    var chart = new CanvasJS.Chart("chartContainer", {
        data: [{
            type: "doughnut",
            dataPoints: [{
                    y: 4181563
                },
                {
                    y: 2175498
                }
            ]
        }]
    });
    chart.render();

    // Dashboard percentage chart
    var chart2 = new CanvasJS.Chart("chartContainer2", {
        data: [{
            type: "doughnut",
            dataPoints: [{
                    y: 4181563
                },
                {
                    y: 2175498
                }
            ]
        }]
    });
    chart2.render();

    // Dashboard production chart
    var lineChart = new CanvasJS.Chart("lineChart", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    lineChart.render();

    // Dashboard sales chart
    var lineChart2 = new CanvasJS.Chart("lineChart2", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    lineChart2.render();

    // Oil well profile oil production chart
    var oilProductionprofile = new CanvasJS.Chart("oilProductionprofile", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    oilProductionprofile.render();

    // Oil well profile gas production chart
    var gasProductionprofile = new CanvasJS.Chart("gasProductionprofile", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    gasProductionprofile.render();

    // Oil well profile (detailed view) oil production chart
    var oilProductionprofile1 = new CanvasJS.Chart("oilProductionprofile1", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    oilProductionprofile1.render();

    // Oil well profile (detailed view) gas production chart
    var gasProductionprofile1 = new CanvasJS.Chart("gasProductionprofile1", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    gasProductionprofile1.render();

    // Comparative analysis chart
    var oilProduction = new CanvasJS.Chart("oilProduction", {
        axisX: {
            interval: 10
        },
        data: [{
            type: "line",
            dataPoints: [{
                    x: 10,
                    y: 45
                },
                {
                    x: 20,
                    y: 14
                },
                {
                    x: 30,
                    y: 20
                },
                {
                    x: 40,
                    y: 60
                },
                {
                    x: 50,
                    y: 50
                },
                {
                    x: 60,
                    y: 80
                },
                {
                    x: 70,
                    y: 40
                },
                {
                    x: 80,
                    y: 60
                },
                {
                    x: 90,
                    y: 10
                },
                {
                    x: 100,
                    y: 50
                },
                {
                    x: 110,
                    y: 40
                },
                {
                    x: 120,
                    y: 14
                },
                {
                    x: 130,
                    y: 70
                },
                {
                    x: 140,
                    y: 40
                },
                {
                    x: 150,
                    y: 90
                },
            ]
        }]
    });
    oilProduction.render();

    // Reports sales and production overview chart
    var reportsOverview = new CanvasJS.Chart("reportsOverview", {
        data: [{
            type: "column",
            dataPoints: [{
                    y: 45,
                    label: "Apple"
                },
                {
                    y: 31,
                    label: "Mango"
                },
                {
                    y: 52,
                    label: "Pineapple"
                },
                {
                    y: 10,
                    label: "Grapes"
                },
                {
                    y: 46,
                    label: "Lemon"
                },
                {
                    y: 30,
                    label: "Banana"
                },
                {
                    y: 50,
                    label: "Watermelon"
                },
                {
                    y: 28,
                    label: "Coconut"
                },
                {
                    y: 45,
                    label: "Lychee"
                },
                {
                    y: 15,
                    label: "Palm"
                },
                {
                    y: 48,
                    label: "Orange"
                },
                {
                    y: 38,
                    label: "Muskmelon"
                },
                {
                    y: 41,
                    label: "Strawberry"
                },
                {
                    y: 25,
                    label: "Kiwi"
                },
                {
                    y: 50,
                    label: "Guava"
                },
            ]
        }]
    });
    reportsOverview.render();

}
