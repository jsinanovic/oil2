
$('#addUserInvestment').submit(function (event) {
    var data = $("#addUserInvestment").serialize();
    var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addinvestment',
        data: data,
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new user investment" + ' !</div>');

            $(".btnInvestment").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User Investment');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addUserInvestment')[0].reset();
            $('.btnInvestment').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnInvestment").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add User Investment');
            $('#addUserInvestment')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add user REQUEST//////////////////////////////////////////////////////////////////
$('#addUserForm').submit(function (event) {

    $.ajax({
        type: 'POST',
        url: '/api/v1/addnewuser',
        // dataType: 'json',
        dataType: 'text json',
         contentType: false,
        processData: false,
        data: new FormData($("#addUserForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new user" + ' !</div>');

            // $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User');

            jQuery('html,body').animate({scrollTop:0},0);
            $('#addUserForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
             setTimeout(' location.reload(); ', 2000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add User');
            $('#addUserForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add user REQUEST//////////////////////////////////////////////////////////////////
$('#updateUserForm').submit(function (event) {

    $.ajax({
        type: 'POST',
        url: '/api/v1/updateUser',
        // dataType: 'json',
        dataType: 'text json',
        contentType: false,
        processData: false,
        data: new FormData($("#updateUserForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully updated user" + ' !</div>');

            // $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User');

            jQuery('html,body').animate({scrollTop:0},0);
            $('#updateUserForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            setTimeout(' location.reload(); ', 2000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update User');
            $('#updateUserForm')[0].reset();
        }
    });
    return false;
});


/////////////////////////////////////add oil wells  REQUEST//////////////////////////////////////////////////////////////////
$('#addOilWellsForm').submit(function (event) {
    //var data = $("#addOilWellsForm").serialize();
    //var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addoilwells',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addOilWellsForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add Oil Wells');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addOilWellsForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();

            setTimeout(window.location.href = "/oil-wells" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Oil Wells');
            $('#addOilWellsForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////update oil wells  REQUEST//////////////////////////////////////////////////////////////////
$('#updateOilWellsForm').submit(function (event) {
    // var data = $("#updateOilWellsForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/updateoilwells',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#updateOilWellsForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Update Oil Wells');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#updateOilWellsForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            setTimeout(window.location.href = "/oil-wells" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Oil Wells');
            $('#updateOilWellsForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////update oil wells  REQUEST//////////////////////////////////////////////////////////////////
$('#updatePackageForm').submit(function (event) {
    // var data = $("#updateOilWellsForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/updatepackage',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#updatePackageForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Update Oil Wells');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#updatePackageForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            setTimeout(window.location.href = "/packages" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Oil Wells');
            $('#updatePackageForm')[0].reset();
        }
    });
    return false;
});


/////////////////////////////////////add  oil wells TANK  REQUEST//////////////////////////////////////////////////////////////////
$('#addNewTank').submit(function (event) {
    //var data = $("#addNewTank").serialize();
    //var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addoilwelltank',
        contentType: false,
        processData: false,
        data: new FormData($("#addNewTank")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add oil well Tank');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addNewTank')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/oil-wells" ,1000);
            setTimeout(window.location.href = "/openOilwellTanks/"+ response.oilWellid ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add oil well Tank');
            $('#addNewTank')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add  package   REQUEST//////////////////////////////////////////////////////////////////
$('#addPackageForm').submit(function (event) {
    //var data = $("#addPackageForm").serialize();
    //var store = store || {};
    /*
     * Sets the jwt to the store object
     */
    $.ajax({
        type: 'POST',
        url: '/api/v1/packages',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addPackageForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added package" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add Package');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addPackageForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            setTimeout(window.location.href = "/packages" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Package');
            $('#addPackageForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add input data  REQUEST//////////////////////////////////////////////////////////////////
$('#addInputDataForm').submit(function (event) {
    // var data = $("#addInputDataForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addinputdata',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addInputDataForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added input data wells" + ' !</div>');

            $(".btnInputData").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add Input Data ');

              jQuery('html,body').animate({scrollTop:0},0);
            $('#addInputDataForm')[0].reset();
            $('.btnInputData').prop("disabled", true).hide();
            //
            setTimeout(' location.reload(); ', 1500);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnInputData").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Input Data ');
            $('#addInputDataForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////update input data  REQUEST//////////////////////////////////////////////////////////////////
$('#updateInputDataForm').submit(function (event) {
    // var data = $("#addInputDataForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/updateinputdata',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#updateInputDataForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added input data wells" + ' !</div>');

            jQuery('html,body').animate({scrollTop:0},0);
            $('#updateInputDataForm')[0].reset();

            //
            setTimeout(' location.reload(); ', 1500);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnInputData").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Input Data ');
            $('#updateInputDataForm')[0].reset();
        }
    });
    return false;
});


/////////////////////////////////////Comperative Analzsis on change [0] //////////////////////////////////////////////////////////////////
$('#oilwell0').on('change', function() {
    var data = $("#oilwell0").serialize();
    var store = store || {};

    $.ajax({
        type: 'GET',
        url: '/comperative0',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            $(".first-totalproduction").text(response.totalProduction );
            $(".first-totalsales").text(response.totalSales);
            $(".first-oilproduction").text(response.totalOilProduction);
            $(".first-avg-oilproduction").text(response.totalSales);
            $(".first-oilstock").text(response.totalOilStock);
            $(".first-gasproduction").text(response.totalGasProduction);
            $(".first-avg-gasproduction").text(response.totalSales);
            $(".first-gasstock").text(response.totalGasStock);


            // Comparative analysis chart
            new Chartist.Line('.ca-chart11', {
                labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                series: [
                    [ response.oilwellproductionByMonth[0]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[1]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[2]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[3]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[4]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[5]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[6]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[7]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[8]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[9]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[10]['infos'][0].oliProduced,
                        response.oilwellproductionByMonth[11]['infos'][0].oliProduced,

                    ],
                    [ response.oilwellproductionByMonth[0]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[1]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[2]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[3]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[4]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[5]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[6]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[7]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[8]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[9]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[10]['infos'][0].gasProduced,
                        response.oilwellproductionByMonth[11]['infos'][0].gasProduced,

                    ]
                ]
            }, {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
            });

        },
        error: function (error) {

            setTimeout(' location.reload(); ', 10);
        }
    });
    return false;
});

/////////////////////////////////////Comperative Analzsis on change [1] //////////////////////////////////////////////////////////////////
$('#oilwell1').on('change', function() {
    var data = $("#oilwell1").serialize();
    var store = store || {};


    if($(".first-totalproduction").text() != 0  && $(".first-totalproduction").text() != '$0'  ) {
        $.ajax({
            type: 'GET',
            url: '/comperative0',
            dataType: 'json',
            contentType: false,
            processData: false,
            data: data,
            success: function (response) {


                $(".second-totalproduction").text('$' + response.totalProduction)
                    .append(' <small>  ' + (((response.totalProduction - parseFloat($(".first-totalproduction").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

                $(".second-totalsales").text('$' + response.totalSales)
                    .append(' <small>  ' + (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalsales").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


                $(".second-oilproduction").text('$' + response.totalOilProduction)
                    .append(' <small>  ' + (((response.totalOilProduction - parseFloat($(".first-oilproduction").text())) / parseFloat($(".first-oilproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

                $(".second-avg-oilproduction").text('$' + response.totalSales)
                    .append('<small>  ' + (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalsales").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

                $(".second-oilstock").text('$' + response.totalOilStock)
                    .append(' <small>  ' + (((response.totalOilStock - parseFloat($(".first-oilstock").text())) / parseFloat($(".first-oilstock").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


                $(".second-gasproduction").text('$' + response.totalGasProduction)
                    .append(' <small>  ' + (((response.totalGasProduction - parseFloat($(".first-gasproduction").text())) / parseFloat($(".first-gasproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


                $(".second-avg-gasproduction").text('$' + response.totalSales)
                    .append(' <small>  ' + (((response.totalSales - parseFloat($(".first-avg-gasproduction").text())) / parseFloat($(".first-avg-gasproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


                $(".second-gasstock").text('$' + response.totalGasStock)
                    .append(' <small>  ' + (((response.totalGasStock - parseFloat($(".first-gasstock").text())) / parseFloat($(".first-gasstock").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

            },
            error: function (error) {
                alert("We got error during calculations! Please contact support.");
            }
        });
        return false;
    } else {
        alert('Please select oil well with data inside.');
    }
});

/////////////////////////////////////Comperative Analzsis on change [2] //////////////////////////////////////////////////////////////////
$('#oilwell2').on('change', function() {
    var data = $("#oilwell2").serialize();
    var store = store || {};
    if($(".first-totalproduction").text() != 0  && $(".first-totalproduction").text() != '$0'  ) {
    $.ajax({
        type: 'GET',
        url: '/comperative0',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            $(".third-totalproduction").text('$'+ response.totalProduction )
                .append( ' <small>  ' +  (((response.totalProduction - parseFloat($(".first-totalproduction").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

            $(".third-totalsales").text('$'+  response.totalSales)
                .append( ' <small>  ' +  (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalsales").text())) * 100).toFixed(2) + '%</small>');


            $(".third-oilproduction").text('$'+  response.totalOilProduction)
                .append( ' <small>  ' +  (((response.totalOilProduction - parseFloat($(".first-oilproduction").text())) / parseFloat($(".first-oilproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

            $(".third-avg-oilproduction").text('$'+  response.totalSales)
                .append( '<small>  ' +  (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalsales").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');

            $(".third-oilstock").text('$'+  response.totalOilStock)
                .append( ' <small>  ' +  (((response.totalOilStock - parseFloat($(".first-oilstock").text())) / parseFloat($(".first-oilstock").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


            $(".third-gasproduction").text('$'+  response.totalGasProduction)
                .append( ' <small>  ' +  (((response.totalGasProduction - parseFloat($(".first-gasproduction").text())) / parseFloat($(".first-gasproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


            $(".third-avg-gasproduction").text('$'+  response.totalSales)
                .append( ' <small>  ' +  (((response.totalSales - parseFloat($(".first-avg-gasproduction").text())) / parseFloat($(".first-avg-gasproduction").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span></small>');


            $(".third-gasstock").text('$'+  response.totalGasStock)
                .append( ' <small>  ' +  (((response.totalGasStock - parseFloat($(".first-gasstock").text())) / parseFloat($(".first-gasstock").text())) * 100).toFixed(2) + '%<span class="green"><i class="fa fa-caret-up" aria-hidden="true"></i></span>');


            //     $(".third-totalproduction").text('$'+ response.totalProduction )
            //     .append( ' <span>  ' +  (((response.totalProduction - parseFloat($(".first-totalproduction").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Total production</span>');
            //     $(".third-totalsales").text('$'+  response.totalSales)
            //         .append( ' <span>  ' +  (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Total Sales</span>');
            //
            //     $(".third-oilproduction").text('$'+  response.totalOilProduction).append( '<span>  1.07%</span><span class="prod">Oil production</span>');
            //     $(".third-avg-oilproduction").text('$'+  response.totalSales).append( '<span>  1.28%</span><span class="prod">Average oil production</span>');
            //     $(".third-oilstock").text('$'+  response.totalOilStock).append( '<span>  1.07%</span><span class="prod">Oil in stock</span>');
            //     $(".third-gasproduction").text('$'+  response.totalGasProduction).append( '<span> 1.07%</span><span class="prod">Gas production</span>');
            //     $(".third-avg-gasproduction").text('$'+  response.totalSales).append( '<span>  1.06%</span><span class="prod">Average gas production</span>');
            //     $(".third-gasstock").text('$'+  response.totalGasStock).append( '<span>  1.01%</span><span class="prod">Gas in stock</span>');
        },
        error: function (error) {
            alert("Please select first Oil well. If problem continues contact support");
        }
    });
    return false;
    } else {
        alert('Please select oil well with data inside.');
    }
});

/////////////////////////////////////removeFunction for package //////////////////////////////////////////////////////////////////
function removeFunction (val) {
    if (confirm('Are you sure you want to delete this Oil well!')) {
        var data = val;
        $.ajax({
            type: 'GET',
            url: '/remove-package/' + val,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: data,
            success: function (response) {
                $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                    + " You have successfully removed package" + ' !</div>');


                //
                setTimeout(' location.reload(); ', 500);
                //  setTimeout(window.location.href = "api/v1/index" ,1000);
                // setTimeout(window.location.href = "/packages" ,1000);
            },
            error: function (error) {
                if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                    var messages = [];
                    //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                    for (var key in error.responseJSON.error) {
                        if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                            var val = error.responseJSON.error[key];
                            messages.push(val + '<br/>');
                        }
                    }
                    $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                        messages.toString() + ' !</div>');


                } else {
                    $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                        'Error. Please try again.</div>' );
                }

                $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Oil Wells');
                $('#updatePackageForm')[0].reset();
            }
        });
        return false;
        // Save it!
    } else {
        // Do nothing!
    }

};

///////////////////addSettings///////////////////////////////
$('#addSettings').submit(function (event) {

    $.ajax({
        type: 'POST',
        url: '/api/v1/addsettings',
        // dataType: 'json',
        dataType: 'text json',
        contentType: false,
        processData: false,
        data: new FormData($("#addSettings")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new user" + ' !</div>');

            // $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User');

            jQuery('html,body').animate({scrollTop:0},0);
            $('#addSettings')[0].reset();
            //
            setTimeout(' location.reload(); ', 200);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $('#addSettings')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add user REQUEST//////////////////////////////////////////////////////////////////
$('#registerUserForm').submit(function (event) {

    $.ajax({
        type: 'POST',
        url: '/registerUser',
        // dataType: 'json',
        dataType: 'text json',
        contentType: false,
        processData: false,
        data: new FormData($("#registerUserForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + "You are successfully registered. You will be redirected to login screen." + ' !</div>');

            // $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User');

            jQuery('html,body').animate({scrollTop:0},0);
            $('#registerUserForm')[0].reset();

            //
            //setTimeout(' location.reload(); ', 2000);
            setTimeout(window.location.href = "/" ,4000);
            //setTimeout(window.location.href = "/login" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');
                setTimeout(window.location.href = "/" ,4000);


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add User');
            $('#registerUserForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////export on change [2] //////////////////////////////////////////////////////////////////
$('#reportingExport').on('change', function() {
    setTimeout(window.location.href = "/exportPDF" ,10);
});

/////////////////////////////////////filter oil Well on change [2] //////////////////////////////////////////////////////////////////
$('#oilWellSelect').on('change', function() {

    var stuff ={'selected':$("#oilWellSelect").val() ,'wellId': $("#wellId").val()};
    // alert(data);
    $.ajax({
        type: 'GET',
        url: '/getOilWellByMonth',
        dataType: 'json',
        data: stuff,
        success: function (response) {
            // Oil well profile oil production chart
            if($("#oilWellSelect").val() == 2) {
                new Chartist.Line('.owp-chart-11', {
                    labels: ['2012', '2013', '2014', '2015', '2016', '2017'],
                    series: [
                        [response.oilwellproductionBy[0]['infos'][0].oliProduced,
                            response.oilwellproductionBy[1]['infos'][0].oliProduced,
                            response.oilwellproductionBy[2]['infos'][0].oliProduced,
                            response.oilwellproductionBy[3]['infos'][0].oliProduced,
                            response.oilwellproductionBy[4]['infos'][0].oliProduced,
                            response.oilwellproductionBy[5]['infos'][0].oliProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });

                // Oil well profile gas production chart
                new Chartist.Line('.owp-chart-22', {
                    labels: ['2012', '2013', '2014', '2015', '2016', '2017'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].gasProduced,
                            response.oilwellproductionBy[1]['infos'][0].gasProduced,
                            response.oilwellproductionBy[2]['infos'][0].gasProduced,
                            response.oilwellproductionBy[3]['infos'][0].gasProduced,
                            response.oilwellproductionBy[4]['infos'][0].gasProduced,
                            response.oilwellproductionBy[5]['infos'][0].gasProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }

            if($("#oilWellSelect").val() == 1) {
                // Oil well profile oil production chart
                new Chartist.Line('.owp-chart-11', {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    series: [
                        [ response.oilwellproductionBy[0]['infos'][0].oliProduced,
                            response.oilwellproductionBy[1]['infos'][0].oliProduced,
                            response.oilwellproductionBy[2]['infos'][0].oliProduced,
                            response.oilwellproductionBy[3]['infos'][0].oliProduced,
                            response.oilwellproductionBy[4]['infos'][0].oliProduced,
                            response.oilwellproductionBy[5]['infos'][0].oliProduced,
                            response.oilwellproductionBy[6]['infos'][0].oliProduced,
                            response.oilwellproductionBy[7]['infos'][0].oliProduced,
                            response.oilwellproductionBy[8]['infos'][0].oliProduced,
                            response.oilwellproductionBy[9]['infos'][0].oliProduced,
                            response.oilwellproductionBy[10]['infos'][0].oliProduced,
                            response.oilwellproductionBy[11]['infos'][0].oliProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });

                // Oil well profile gas production chart
                new Chartist.Line('.owp-chart-22', {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    series: [
                        [ response.oilwellproductionBy[0]['infos'][0].gasProduced,
                            response.oilwellproductionBy[1]['infos'][0].gasProduced,
                            response.oilwellproductionBy[2]['infos'][0].gasProduced,
                            response.oilwellproductionBy[3]['infos'][0].gasProduced,
                            response.oilwellproductionBy[4]['infos'][0].gasProduced,
                            response.oilwellproductionBy[5]['infos'][0].gasProduced,
                            response.oilwellproductionBy[6]['infos'][0].gasProduced,
                            response.oilwellproductionBy[7]['infos'][0].gasProduced,
                            response.oilwellproductionBy[8]['infos'][0].gasProduced,
                            response.oilwellproductionBy[9]['infos'][0].gasProduced,
                            response.oilwellproductionBy[10]['infos'][0].gasProduced,
                            response.oilwellproductionBy[11]['infos'][0].gasProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }

        },
        error: function (error) {
            alert("If problem continues contact support");
        }
    });
    return false;
});

/////////////////////////////////////filter package on change [2] //////////////////////////////////////////////////////////////////
$('#packageSelect').on('change', function() {

    var stuff ={'selected':$("#packageSelect").val() ,'packageId': $("#packageId").val()};
    // alert(data);
    $.ajax({
        type: 'GET',
        url: '/getPackageByMonth',
        dataType: 'json',
        data: stuff,
        success: function (response) {
            // Oil well profile oil production chart
            if($("#packageSelect").val() == 2) {
                new Chartist.Line('.owpd-chart-11', {
                    labels: ['2012', '2013', '2014', '2015', '2016', '2017'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].oliProduced,
                            response.oilwellproductionBy[1]['infos'][0].oliProduced,
                            response.oilwellproductionBy[2]['infos'][0].oliProduced,
                            response.oilwellproductionBy[3]['infos'][0].oliProduced,
                            response.oilwellproductionBy[4]['infos'][0].oliProduced,
                            response.oilwellproductionBy[5]['infos'][0].oliProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });

                // Oil well profile gas production chart
                new Chartist.Line('.owpd-chart-22', {
                    labels: ['2012', '2013', '2014', '2015', '2016', '2017'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].gasProduced,
                            response.oilwellproductionBy[1]['infos'][0].gasProduced,
                            response.oilwellproductionBy[2]['infos'][0].gasProduced,
                            response.oilwellproductionBy[3]['infos'][0].gasProduced,
                            response.oilwellproductionBy[4]['infos'][0].gasProduced,
                            response.oilwellproductionBy[5]['infos'][0].gasProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }

            if($("#packageSelect").val() == 1) {
                // Oil well profile oil production chart
                new Chartist.Line('.owpd-chart-11', {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    series: [
                        [ response.oilwellproductionBy[0]['infos'][0].oliProduced,
                            response.oilwellproductionBy[1]['infos'][0].oliProduced,
                            response.oilwellproductionBy[2]['infos'][0].oliProduced,
                            response.oilwellproductionBy[3]['infos'][0].oliProduced,
                            response.oilwellproductionBy[4]['infos'][0].oliProduced,
                            response.oilwellproductionBy[5]['infos'][0].oliProduced,
                            response.oilwellproductionBy[6]['infos'][0].oliProduced,
                            response.oilwellproductionBy[7]['infos'][0].oliProduced,
                            response.oilwellproductionBy[8]['infos'][0].oliProduced,
                            response.oilwellproductionBy[9]['infos'][0].oliProduced,
                            response.oilwellproductionBy[10]['infos'][0].oliProduced,
                            response.oilwellproductionBy[11]['infos'][0].oliProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });

                // Oil well profile gas production chart
                new Chartist.Line('.owpd-chart-22', {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    series: [
                        [ response.oilwellproductionBy[0]['infos'][0].gasProduced,
                            response.oilwellproductionBy[1]['infos'][0].gasProduced,
                            response.oilwellproductionBy[2]['infos'][0].gasProduced,
                            response.oilwellproductionBy[3]['infos'][0].gasProduced,
                            response.oilwellproductionBy[4]['infos'][0].gasProduced,
                            response.oilwellproductionBy[5]['infos'][0].gasProduced,
                            response.oilwellproductionBy[6]['infos'][0].gasProduced,
                            response.oilwellproductionBy[7]['infos'][0].gasProduced,
                            response.oilwellproductionBy[8]['infos'][0].gasProduced,
                            response.oilwellproductionBy[9]['infos'][0].gasProduced,
                            response.oilwellproductionBy[10]['infos'][0].gasProduced,
                            response.oilwellproductionBy[11]['infos'][0].gasProduced,
                        ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }

        },
        error: function (error) {
            alert("If problem continues contact support");
        }
    });
    return false;
});

/////////////////////////////////////filter dashboard on change [2] //////////////////////////////////////////////////////////////////
$('#dashboardSalesSelect').on('change', function() {

    var stuff ={ 'selected':$("#dashboardSalesSelect").val() };
    // alert(data);
    $.ajax({
        type: 'GET',
        url: '/getdashboardByMonthSales',
        dataType: 'json',
        data: stuff,
        success: function (response) {

            if($("#dashboardSalesSelect").val() == 1) {
                new Chartist.Line('.dash-chart-44', {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[1]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[2]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[3]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[4]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[5]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[6]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[7]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[8]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[9]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[10]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[11]['infos'][0].oilSold *1.08,
                        ],
                            [
                            response.oilwellproductionBy[0]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[1]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[2]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[3]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[4]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[5]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[6]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[7]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[8]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[9]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[10]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[11]['infos'][0].gasSold *1.08,
                            ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }
            if($("#dashboardSalesSelect").val() == 2) {
                new Chartist.Line('.dash-chart-44', {
                    labels: ['2012', '2013', '2014', '2015', '2016', '2017'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[1]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[2]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[3]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[4]['infos'][0].oilSold *1.08,
                            response.oilwellproductionBy[5]['infos'][0].oilSold *1.08,
                        ],
                            [
                            response.oilwellproductionBy[0]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[1]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[2]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[3]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[4]['infos'][0].gasSold *1.08,
                            response.oilwellproductionBy[5]['infos'][0].gasSold *1.08
                            ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }

        },
        error: function (error) {
            alert("If problem continues contact support");
        }
    });
    return false;
});

/////////////////////////////////////filter dashboard on change [2] //////////////////////////////////////////////////////////////////
$('#dashboardProductionSelect').on('change', function() {

    var stuff ={ 'selected':$("#dashboardProductionSelect").val() };
    // alert(data);
    $.ajax({
        type: 'GET',
        url: '/getdashboardByMonth',
        dataType: 'json',
        data: stuff,
        success: function (response) {

            if($("#dashboardProductionSelect").val() == 1) {
                new Chartist.Line('.dash-chart-33', {
                    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AVG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].oliProduced,
                            response.oilwellproductionBy[1]['infos'][0].oliProduced,
                            response.oilwellproductionBy[2]['infos'][0].oliProduced,
                            response.oilwellproductionBy[3]['infos'][0].oliProduced,
                            response.oilwellproductionBy[4]['infos'][0].oliProduced,
                            response.oilwellproductionBy[5]['infos'][0].oliProduced,
                            response.oilwellproductionBy[6]['infos'][0].oliProduced,
                            response.oilwellproductionBy[7]['infos'][0].oliProduced,
                            response.oilwellproductionBy[8]['infos'][0].oliProduced,
                            response.oilwellproductionBy[9]['infos'][0].oliProduced,
                            response.oilwellproductionBy[10]['infos'][0].oliProduced,
                            response.oilwellproductionBy[11]['infos'][0].oliProduced,
                        ],

                            [
                            response.oilwellproductionBy[0]['infos'][0].gasProduced,
                                response.oilwellproductionBy[1]['infos'][0].gasProduced,
                                response.oilwellproductionBy[2]['infos'][0].gasProduced,
                                response.oilwellproductionBy[3]['infos'][0].gasProduced,
                                response.oilwellproductionBy[4]['infos'][0].gasProduced,
                                response.oilwellproductionBy[5]['infos'][0].gasProduced,
                                response.oilwellproductionBy[6]['infos'][0].gasProduced,
                                response.oilwellproductionBy[7]['infos'][0].gasProduced,
                                response.oilwellproductionBy[8]['infos'][0].gasProduced,
                                response.oilwellproductionBy[9]['infos'][0].gasProduced,
                                response.oilwellproductionBy[10]['infos'][0].gasProduced,
                                response.oilwellproductionBy[11]['infos'][0].gasProduced,
                            ]
                    ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }
            if($("#dashboardProductionSelect").val() == 2) {
                new Chartist.Line('.dash-chart-33', {
                    labels: ['2012', '2013', '2014', '2015', '2016', '2017'],
                    series: [
                        [
                            response.oilwellproductionBy[0]['infos'][0].oliProduced,
                            response.oilwellproductionBy[1]['infos'][0].oliProduced,
                            response.oilwellproductionBy[2]['infos'][0].oliProduced,
                            response.oilwellproductionBy[3]['infos'][0].oliProduced,
                            response.oilwellproductionBy[4]['infos'][0].oliProduced,
                            response.oilwellproductionBy[5]['infos'][0].oliProduced,
                        ],
                        [
                            response.oilwellproductionBy[0]['infos'][0].gasProduced,
                            response.oilwellproductionBy[1]['infos'][0].gasProduced,
                            response.oilwellproductionBy[2]['infos'][0].gasProduced,
                            response.oilwellproductionBy[3]['infos'][0].gasProduced,
                            response.oilwellproductionBy[4]['infos'][0].gasProduced,
                            response.oilwellproductionBy[5]['infos'][0].gasProduced,
                        ]
                     ]
                }, {
                    fullWidth: true,
                    chartPadding: {
                        right: 40
                    }
                });
            }

        },
        error: function (error) {
            alert("If problem continues contact support");
        }
    });
    return false;
});