window.onload = function() {
  // Dashboard pie chart 1
  var data = {
    series: [5, 3, 4]
  };

  var sum = function(a, b) { return a + b };

  new Chartist.Pie('.dash-chart-1', data, {
    labelInterpolationFnc: function(value) {
      return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
  });

  // Dashboard pie chart 2
  var data = {
    series: [5, 3, 4]
  };

  var sum = function(a, b) { return a + b };

  new Chartist.Pie('.dash-chart-2', data, {
    labelInterpolationFnc: function(value) {
      return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
  });

  // Dashboard production chart
  new Chartist.Line('.dash-chart-3', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [2, 1, 3.5, 7, 3, 1, 5]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

  // Dashboard sales chart
  new Chartist.Line('.dash-chart-4', {
    labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    series: [
      [12, 9, 7, 8, 5, 6, 6]
    ]
  }, {
    fullWidth: true,
    chartPadding: {
      right: 40
    }
  });

}
