/**
 * Created by x2-ja on 12/1/2016.
 */

/////////////////////////////////////add user investment REQUEST//////////////////////////////////////////////////////////////////
$('#addUserInvestment').submit(function (event) {
    var data = $("#addUserInvestment").serialize();
    var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addinvestment',
        data: data,
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new user investment" + ' !</div>');

            $(".btnInvestment").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User Investment');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addUserInvestment')[0].reset();
            $('.btnInvestment').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnInvestment").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add User Investment');
            $('#addUserInvestment')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add user REQUEST//////////////////////////////////////////////////////////////////
$('#addUserForm').submit(function (event) {

    $.ajax({
        type: 'POST',
        url: '/api/v1/addnewuser',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addUserForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new user" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add User');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addUserForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add User');
            $('#addUserForm')[0].reset();
        }
    });
    return false;
});



/////////////////////////////////////add oil wells  REQUEST//////////////////////////////////////////////////////////////////
$('#addOilWellsForm').submit(function (event) {
    //var data = $("#addOilWellsForm").serialize();
    //var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addoilwells',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addOilWellsForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add Oil Wells');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addOilWellsForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();

           setTimeout(window.location.href = "/oil-wells" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Oil Wells');
            $('#addOilWellsForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////update oil wells  REQUEST//////////////////////////////////////////////////////////////////
$('#updateOilWellsForm').submit(function (event) {
    // var data = $("#updateOilWellsForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/updateoilwells',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#updateOilWellsForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Update Oil Wells');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#updateOilWellsForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            setTimeout(window.location.href = "/oil-wells" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Oil Wells');
            $('#updateOilWellsForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////update oil wells  REQUEST//////////////////////////////////////////////////////////////////
$('#updatePackageForm').submit(function (event) {
    // var data = $("#updateOilWellsForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/updatepackage',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#updatePackageForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Update Oil Wells');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#updatePackageForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            setTimeout(window.location.href = "/packages" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Oil Wells');
            $('#updatePackageForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add  oil wells TANK  REQUEST//////////////////////////////////////////////////////////////////
$('#addNewTank').submit(function (event) {
    //var data = $("#addNewTank").serialize();
    //var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addoilwelltank',
        contentType: false,
        processData: false,
        data: new FormData($("#addNewTank")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added new oil wells" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add oil well Tank');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addNewTank')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/oil-wells" ,1000);
            setTimeout(window.location.href = "/openOilwellTanks/"+ response.oilWellid ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add oil well Tank');
            $('#addNewTank')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add  package   REQUEST//////////////////////////////////////////////////////////////////
$('#addPackageForm').submit(function (event) {
    //var data = $("#addPackageForm").serialize();
    //var store = store || {};
    /*
     * Sets the jwt to the store object
     */
    $.ajax({
        type: 'POST',
        url: '/api/v1/packages',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addPackageForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added package" + ' !</div>');

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add Package');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addPackageForm')[0].reset();
            $('.btnRequest').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            setTimeout(window.location.href = "/packages" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>' );
            }

            $(".btnRequest").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Package');
            $('#addPackageForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////add input data  REQUEST//////////////////////////////////////////////////////////////////
$('#addInputDataForm').submit(function (event) {
    // var data = $("#addInputDataForm").serialize();
    // var store = store || {};

    /*
     * Sets the jwt to the store object
     */

    $.ajax({
        type: 'POST',
        url: '/api/v1/addinputdata',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: new FormData($("#addInputDataForm")[0]),
        success: function (response) {
            $("#info").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '
                + " You have successfully added input data wells" + ' !</div>');

            $(".btnInputData").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp;  Add Input Data ');

            //  jQuery('html,body').animate({scrollTop:0},0);
            $('#addInputDataForm')[0].reset();
            $('.btnInputData').prop("disabled", true).hide();
            //
            // setTimeout(' location.reload(); ', 3000);
            //  setTimeout(window.location.href = "api/v1/index" ,1000);
            //setTimeout(window.location.href = "/dashboard" ,1000);
        },
        error: function (error) {
            if (error.status == '400' && error.responseJSON && error.responseJSON.error) {
                var messages = [];
                //NOTE: error is object inside responseJSON so for(var i=0... is not applicable
                for (var key in error.responseJSON.error) {
                    if (Object.prototype.hasOwnProperty.call(error.responseJSON.error, key)) {
                        var val = error.responseJSON.error[key];
                        messages.push(val + '<br/>');
                    }
                }
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    messages.toString() + ' !</div>');


            } else {
                $("#info").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' +
                    'Error. Please try again.</div>');
            }

            $(".btnInputData").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Input Data ');
            $('#addInputDataForm')[0].reset();
        }
    });
    return false;
});

/////////////////////////////////////Comperative Analzsis on change [0] //////////////////////////////////////////////////////////////////
$('#oilwell0').on('change', function() {
     var data = $("#oilwell0").serialize();
     var store = store || {};

    $.ajax({
        type: 'GET',
        url: '/comperative0',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            $(".first-totalproduction").text(response.totalProduction ).append( '<span class="prod">Total production</span>');
            $(".first-totalsales").text(response.totalSales).append( '<span class="prod">Total sales</span>');
            $(".first-oilproduction").text(response.totalOilProduction).append( '<span class="prod">Oil production</span>');
            $(".first-avg-oilproduction").text(response.totalSales).append( '<span class="prod">Average oil production</span>');
            $(".first-oilstock").text(response.totalOilStock).append( '<span class="prod">Oil in stock</span>');
            $(".first-gasproduction").text(response.totalGasProduction).append( '<span class="prod">Gas production</span>');
            $(".first-avg-gasproduction").text(response.totalSales).append( '<span class="prod">Average gas production</span>');
            $(".first-gasstock").text(response.totalGasStock).append( '<span class="prod">Gas in stock</span>');
        },
        error: function (error) {
           alert("We got error during calculations! Please contact support.");
        }
    });
    return false;
});

/////////////////////////////////////Comperative Analzsis on change [1] //////////////////////////////////////////////////////////////////
$('#oilwell1').on('change', function() {
    var data = $("#oilwell1").serialize();
    var store = store || {};

    $.ajax({
        type: 'GET',
        url: '/comperative0',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            $(".second-totalproduction").text('$'+ response.totalProduction )
                .append( ' <span>  ' +  (((response.totalProduction - parseFloat($(".first-totalproduction").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Total production</span>');

            $(".second-totalsales").text('$'+  response.totalSales)
                .append( ' <span>  ' +  (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalsales").text())) * 100).toFixed(2) + '%</span><span class="prod">Total Sales</span>');


            $(".second-oilproduction").text('$'+  response.totalOilProduction)
                .append( ' <span>  ' +  (((response.totalOilProduction - parseFloat($(".first-avg-oilproduction").text())) / parseFloat($(".first-avg-oilproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Oil production</span>');

            $(".second-avg-oilproduction").text('$'+  response.totalSales)
                .append( '<span>  1.10%</span><span class="prod">Average oil production</span>');

            $(".second-oilstock").text('$'+  response.totalOilStock)
                .append( ' <span>  ' +  (((response.totalOilStock - parseFloat($(".first-oilstock").text())) / parseFloat($(".first-oilstock").text())) * 100).toFixed(2) + '%</span><span class="prod">Oil in stock</span>');


            $(".second-gasproduction").text('$'+  response.totalGasProduction)
                .append( ' <span>  ' +  (((response.totalGasProduction - parseFloat($(".first-gasproduction").text())) / parseFloat($(".first-gasproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Gas production</span>');


            $(".second-avg-gasproduction").text('$'+  response.totalSales)
                .append( '<span>  1.07%</span><span class="prod">Average gas production</span>');


            $(".second-gasstock").text('$'+  response.totalGasStock)
                .append( ' <span>  ' +  (((response.totalGasStock - parseFloat($(".first-gasstock").text())) / parseFloat($(".first-gasstock").text())) * 100).toFixed(2) + '%</span><span class="prod">Gas in stock</span>');

        },
        error: function (error) {
            alert("We got error during calculations! Please contact support.");
        }
    });
    return false;
});

/////////////////////////////////////Comperative Analzsis on change [2] //////////////////////////////////////////////////////////////////
$('#oilwell2').on('change', function() {
    var data = $("#oilwell2").serialize();
    var store = store || {};

    $.ajax({
        type: 'GET',
        url: '/comperative0',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            $(".third-totalproduction").text('$'+ response.totalProduction )
                .append( ' <span>  ' +  (((response.totalProduction - parseFloat($(".first-totalproduction").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Total production</span>');

            $(".third-totalsales").text('$'+  response.totalSales)
                .append( ' <span>  ' +  (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalsales").text())) * 100).toFixed(2) + '%</span><span class="prod">Total Sales</span>');


            $(".third-oilproduction").text('$'+  response.totalOilProduction)
                .append( ' <span>  ' +  (((response.totalOilProduction - parseFloat($(".first-avg-oilproduction").text())) / parseFloat($(".first-avg-oilproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Oil production</span>');

            $(".third-avg-oilproduction").text('$'+  response.totalSales)
                .append( '<span>  1.10%</span><span class="prod">Average oil production</span>');

            $(".third-oilstock").text('$'+  response.totalOilStock)
                .append( ' <span>  ' +  (((response.totalOilStock - parseFloat($(".first-oilstock").text())) / parseFloat($(".first-oilstock").text())) * 100).toFixed(2) + '%</span><span class="prod">Oil in stock</span>');


            $(".third-gasproduction").text('$'+  response.totalGasProduction)
                .append( ' <span>  ' +  (((response.totalGasProduction - parseFloat($(".first-gasproduction").text())) / parseFloat($(".first-gasproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Gas production</span>');


            $(".third-avg-gasproduction").text('$'+  response.totalSales)
                .append( '<span>  1.07%</span><span class="prod">Average gas production</span>');


            $(".third-gasstock").text('$'+  response.totalGasStock)
                .append( ' <span>  ' +  (((response.totalGasStock - parseFloat($(".first-gasstock").text())) / parseFloat($(".first-gasstock").text())) * 100).toFixed(2) + '%</span><span class="prod">Gas in stock</span>');


        //     $(".third-totalproduction").text('$'+ response.totalProduction )
        //     .append( ' <span>  ' +  (((response.totalProduction - parseFloat($(".first-totalproduction").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Total production</span>');
        //     $(".third-totalsales").text('$'+  response.totalSales)
        //         .append( ' <span>  ' +  (((response.totalSales - parseFloat($(".first-totalsales").text())) / parseFloat($(".first-totalproduction").text())) * 100).toFixed(2) + '%</span><span class="prod">Total Sales</span>');
        //
        //     $(".third-oilproduction").text('$'+  response.totalOilProduction).append( '<span>  1.07%</span><span class="prod">Oil production</span>');
        //     $(".third-avg-oilproduction").text('$'+  response.totalSales).append( '<span>  1.28%</span><span class="prod">Average oil production</span>');
        //     $(".third-oilstock").text('$'+  response.totalOilStock).append( '<span>  1.07%</span><span class="prod">Oil in stock</span>');
        //     $(".third-gasproduction").text('$'+  response.totalGasProduction).append( '<span> 1.07%</span><span class="prod">Gas production</span>');
        //     $(".third-avg-gasproduction").text('$'+  response.totalSales).append( '<span>  1.06%</span><span class="prod">Average gas production</span>');
        //     $(".third-gasstock").text('$'+  response.totalGasStock).append( '<span>  1.01%</span><span class="prod">Gas in stock</span>');
         },
        error: function (error) {
            alert("We got error during calculations! Please contact support.");
        }
    });
    return false;
});