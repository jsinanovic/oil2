<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Inputdata;
use App\Investment;
use App\Oilwell;
use App\Role;
use App\Tank;
use App\User;
use App\Package;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Contracts\UserRepositoryInterface;
use Image;
use Excel;

class AdminController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @param UserRepositoryInterface $user
     */
    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
        $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }
}
