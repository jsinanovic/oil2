<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Request;
use App\Investment;
use App\Package;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers;
use Image;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/
	
	use RegistersUsers;
	
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $registerView = 'auth.register';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}
	
	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
        return Validator::make($data, [
            'name' => 'required|max:255',
            'lname' => 'required|max:255',
            'phone' => 'required|max:255',
            'zip' => 'required|max:255',
            'state' => 'required|max:255',
            'city' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
	}
	
	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data, $image)
	{
		$register =  User::create([
            'name' => $data['name'],
            'lname' => $data['lname'],
            'phone' => $data['phone'],
            'zip' => $data['zip'],
            'state' => $data['state'],
            'city' => $data['city'],
            'email' => $data['email'],
            'profile_pic' => $image,
            'password' => bcrypt($data['password']),
		]);

//		if ($register) {
//			$register->role()->attach($data['role']);
//			Mail::send('auth.email.register', ['link' => URL::route('user-activation', 'sa'),
//				'email' => $data['email'], 'password' => $data['password']], function ($message) {
//				$message->to($this->email, $this->name)->subject('Account Activation');
//			});
//		}
        $registerInvestment =  Investment::create([
            'investment' => $data['investment'],
            'investmentYears' => $data['investmentYears'],
        ]);


        if ($register && $registerInvestment) {
            $register->role()->attach($data["role"]);
            $registerInvestment->user()->attach($register->id);
            $register->package()->attach($data['packageid']);
        }

		return $register;
	}

    /**
     * Show the indexUserManagment
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRegister()
    {
//        $users= User::all();
        $roles = Role::all();
        $packages = Package::all();

        return view('auth.register', array('roles' => $roles, 'packages' => $packages ));
    }


	/**
	 * @api {post} /register Register user
	 * @apiName RegisterUser
	 * @apiGroup User
	 *
	 * @apiParam {String} name User name.
	 * @apiParam {String} email Users unique email.
	 * @apiParam {String} password Users login password.
	 * @apiParam {String} password_confirmation Password confirmation.
	 * @apiParam {Integer} role Add role to user, 1 - User, 2 - Developer, 3 - Admin
	 *
	 * @apiDescription Register user and retrive access token.
	 *
	 * @apiSuccessExample {json} Success-Response:
	 *   {
	 * "success": "Activation link sent to: "
	 * }
	 */
	public function addUser(\Illuminate\Http\Request $request)
	{
		$validator = $this->validator($request->all());

		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 400);
		}

        if ($request->hasFile('profile_pic')) {
            $rand = substr(md5(microtime()), rand(0, 26), 5);
            $imageName = $rand . '.' .
                $request->file('profile_pic')->getClientOriginalExtension();

            $image =  $request->file('profile_pic')->move(
                base_path() . '/public/images/profile/', $imageName
            );

            $img = Image::make($image->getRealPath());
            $img->resize(62, 62, function ($constraint) {
                //  $constraint->aspectRatio();
            })->save( base_path() . '/public/images/profile/resized/' . $imageName);
        }


		$this->create($request->all(), $imageName);

		return response()->json([
				'status' => 'Registration passed account'
			]
		);
	}

	protected function registerPage()
	{
		return view('user.index');
//		$roles = Role::all();
//		return view('user.index', array('roles' => $roles));
	}
}
