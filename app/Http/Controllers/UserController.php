<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Investment;
use App\Role;
use App\User;
use App\Package;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Contracts\UserRepositoryInterface;
use Image;

class UserController extends Controller
{
	private $user;

    /**
     * Create a new controller instance.
     *
     * @param UserRepositoryInterface $user
     */
	public function __construct(UserRepositoryInterface $user)
	{
		$this->user = $user;
		$this->middleware(['auth']);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'lname' => 'required|max:255',
			'phone' => 'required|max:255',
			'zip' => 'required|max:255',
			'state' => 'required|max:255',
			'city' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6|confirmed',
		]);
	}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @param $image
     * @return User
     * @internal param Request $request
     */
	protected function create(array $data, $image)
	{
            $register =  User::create([
			'name' => $data['name'],
			'lname' => $data['lname'],
			'phone' => $data['phone'],
			'zip' => $data['zip'],
			'state' => $data['state'],
			'city' => $data['city'],
			'email' => $data['email'],
            'profile_pic' => $image,
			'password' => bcrypt($data['password']),
		]);

		$registerInvestment =  Investment::create([
			'investment' => $data['investment'],
			'investmentYears' => $data['investmentYears'],
		]);


		if ($register && $registerInvestment) {
			$register->role()->attach($data["role"]);
			$registerInvestment->user()->attach($register->id);
            $register->package()->attach($data['packageid']);
		}

		return $register;
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$roles = Role::all();
		$users = User::all();
		return view('user.adduser', array('roles' => $roles, 'users' => $users));
	}

	/**
	 * Show the indexUserManagment
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function indexUserManagment()
	{
		$users= User::all();
        $roles = Role::all();
        $packages = Package::all();

        return view('user.index', array('roles' => $roles, 'users' => $users,'packages' => $packages ));
	}

    /**
     * Show the indexUserManagment
     *
     * @return \Illuminate\Http\Response
     */
    public function editUser($id)
    {

        $roles = Role::all();
        $packages = Package::all();
        $user= User::where('id', $id)->get();
        return view('user.edit', array( 'user' => $user,'roles' => $roles,'packages' => $packages));
    }

    /**addUser
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function addUser(\Illuminate\Http\Request $request)
	{
		$validator = $this->validator($request->all());

		if ($validator->fails()) {
			return response()->json(['User data are not valid!' => $validator->errors()], 400);
		}

        if ($request->hasFile('profile_pic')) {
            $rand = substr(md5(microtime()), rand(0, 26), 5);
            $imageName = $rand . '.' .
                $request->file('profile_pic')->getClientOriginalExtension();

            $image =  $request->file('profile_pic')->move(
                base_path() . '/public/images/profile/', $imageName
            );

            $img = Image::make($image->getRealPath());
            $img->resize(62, 62, function ($constraint) {
                //  $constraint->aspectRatio();
            })->save( base_path() . '/public/images/profile/resized/' . $imageName);
        }

		$this->create($request->all(), $imageName);

        return response()->json([
                'success' => 'You added new input data!',
                'status' => 'New input data'
            ]
        );
	}

    /**addUser
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(\Illuminate\Http\Request $request)
    {
        $id = $request['id'];
        $package = User::find($id);

        if ($request->hasFile('profile_pic')) {
            $rand = substr(md5(microtime()), rand(0, 26), 5);
            $imageName = $rand . '.' .
                $request->file('profile_pic')->getClientOriginalExtension();

            $image =  $request->file('profile_pic')->move(
                base_path() . '/public/images/profile/', $imageName
            );

            $img = Image::make($image->getRealPath());
            $img->resize(62, 62, function ($constraint) {
                //  $constraint->aspectRatio();
            })->save( base_path() . '/public/images/profile/resized/' . $imageName);
        } else {
            $imageName  =  $request['profile_picID'];
        }

        if(isset( $request['password']))
            $password = bcrypt($request['password']);
        else
            $password = $request['passwordID'];


        if(isset($id)) {
            $update = User::find($id)->update([
                    'name' => $request['name'],
                    'lname' => $request['lname'],
                    'phone' => $request['phone'],
                    'zip' => $request['zip'],
                    'state' => $request['state'],
                    'city' => $request['city'],
                    'profile_pic' => $imageName,
                    'password' => $password
            ]);



            $updateInvestment =  Investment::find($request['investmentID'])->update([
                'investment' => $request['investment'],
                'investmentYears' => $request['investmentYears'],
            ]);


            if ($update && $updateInvestment) {
//                DB::table('role_user')
//                    ->where('role_id', $request["roleID"])
//                    ->delete();
                User::find($id)->role()->detach($request["roleID"]);
                User::find($id)->role()->attach($request["role"]);
//
               Investment::find($request['investmentID'])->user()->detach($request['investmentID']);
                Investment::find($request['investmentID'])->user()->attach($id);
//
                if($request['packageID']) {
                    User::find($id)->package()->detach($request['packageID']);
                    User::find($id)->package()->attach($request['packageid']);
                }

            }

            return response()->json([
                    'success' => 'User is updated. !',
                    'status' => 'User is updated'
                ]
            );
        }

        return response()->json([
                'error' => 'User is not updated. !',
            ]
        );


    }

    /**
     * addUserInvestment
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function addUserInvestment(\Illuminate\Http\Request $request)
    {
        $this->createInvestment($request->all());
        return response()->json(
            [
                'success' => 'You added new user investment !',
                'status' => 'New investment on account added'
            ]
        );
    }

	/**
	 * Create a new user investment instance after a valid user investment.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function createInvestment(array $data)
	{
		$registerInvestment =  Investment::create([
			'investment' => $data['investment'],
			'investmentYears' => $data['investmentYears'],
		]);

		if ($registerInvestment) {
			$registerInvestment->user()->attach(Auth::user()->id);
		}

		return $registerInvestment;
	}

}
