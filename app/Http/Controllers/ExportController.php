<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Inputdata;
use App\Investment;
use App\Oilwell;
use App\Role;
use App\Tank;
use App\User;
use App\Package;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Contracts\UserRepositoryInterface;
use Image;
use Excel;

class ExportController extends Controller
{
	private $user;

    /**
     * Create a new controller instance.
     *
     * @param UserRepositoryInterface $user
     */
	public function __construct(UserRepositoryInterface $user)
	{
		$this->user = $user;
		$this->middleware(['auth']);
	}

    public function exportPDF(){

        $InputdataForTanks = array();

        $packages = Package::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();


        if($packages->count() > 0) {
            foreach ($packages as $package) {
                $oliSolde = 0;
                $gasSolde = 0;
                $oliProduced = 0;
                $gasProduced = 0;
                $oilStock = 0;
                $gasStock = 0;
                $totalOilProduction = 0;
                $totalGasProduction = 0;
                $totalOilSold = 0;
                $totalGasSold = 0;
                $totalOilStock = 0;
                $totalGasStock = 0;

                $oilwells = Oilwell::whereHas('package', function ($q) use ($package) {
                    $q->where('package_id', $package->id);
                })->get();

                foreach ($oilwells as $oilwell) {
                    $oilTanks = Tank::whereHas('oilwell', function ($q) use ($oilwell) {
                        $q->where('oilwell_id', $oilwell->id);
                    })->get();

                    foreach ($oilTanks as $oilTank) {
                        $inputdatas = Inputdata::where('tankId', $oilTank->id)->get();

                        foreach ($inputdatas as $inputdata) {
                            $oliSolde += $inputdata->oilSold;
                            $gasSolde += $inputdata->gasSold;
                            $oliProduced += $inputdata->oilLvlFT + $inputdata->oilSold;
                            $gasProduced += $inputdata->gaslvl + $inputdata->gasSold;
                            $oilStock += $inputdata->oilLvlFT;
                            $gasStock += $inputdata->gaslvl;
                        }
                    }
                    $totalOilProduction += $oliProduced;
                    $totalGasProduction += $gasProduced;
                    $totalOilSold += $oliSolde;
                    $totalGasSold += $gasSolde;
                    $totalOilStock += $oilStock;
                    $totalGasStock += $gasStock;

                }
                array_push($InputdataForTanks, array(
                    'packages' => $package->name,
                    'oilsolds' => $oliSolde, 'gasSoldes' => $gasSolde, 'oliProduced' => $oliProduced,
                    'gasProduced' => $gasProduced, 'oilStock' => $oilStock, 'gasStock' => $gasStock,
                    'totalOilProduction' => $totalOilProduction, 'totalGasProduction' => $totalGasProduction,
                    'totalOilSold' => $totalOilSold, 'totalGasSold' => $totalGasSold,
                    'totalOilStock' => $totalOilStock, 'totalGasStock' => $totalGasStock
                ));
            }

//            $data = Package::get()->toArray();
            $data = $InputdataForTanks;
            return Excel::create('reports', function ($excel) use ($data) {
                $excel->sheet('mySheet', function ($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->download("pdf");
        }
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

}
