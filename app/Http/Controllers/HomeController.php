<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests;
use App\InputData;
use App\Investment;
use App\OilWell;
use App\Settings;
use App\State;
use App\User;
use App\Tank;
use Carbon\Carbon;
use File;
use App\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Validator;
use App\Contracts\UserRepositoryInterface;
use Symfony\Component\Console\Input\Input;
use Image;

class HomeController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @param UserRepositoryInterface $user
     */
    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
        $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $settings = Settings::all();
        return view('dashboard.index', array('settings' => $settings));
    }

    /**
     * Show the application dashboard.
     *frep
     * @return \Illuminate\Http\Response
     */
    public function settings() {

        $settings = Settings::all();
        return view('settings.index', array('settings' => $settings));
    }

    public function addSettings(Request $request) {


        if($request['settingsID'] != ''){
            Settings::find($request['settingsID'])->update([
                'currency' => $request['currency'],
                'currentGasRate' => $request['currentGasRate'],
                'currentOilRate' => $request['currentOilRate']
            ]);

            return response()->json([
                    'success' => 'You added new Oil wells!',
                    'status' => 'New oil wells added'
                ]
            );
        }

        $validator = $this->validatorSettings($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $this->createSettings($request->all());

        return response()->json([
                'success' => 'You added new Oil wells!',
                'status' => 'New oil wells added'
            ]
        );
    }

    protected function validatorSettings(array $data) {
        return \Illuminate\Support\Facades\Validator::make($data, [
            'currency' => 'required|max:255',
            'currentGasRate' => 'required|max:255',
            'currentOilRate' => 'required|max:255'
        ]);
    }

    protected function createSettings(array $data)
    {
        $settings =  Settings::create([
            'currency' => $data['currency'],
            'currentGasRate' => $data['currentGasRate'],
            'currentOilRate' => $data['currentOilRate']
        ]);

        return $settings;
    }

    /**
     * Show the application indexDashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDashboard() {
        $oilProductionSums  = InputData::all();

        $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id );


        $date = Carbon::now()->toDateString();
        $datas = DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and i.created_at >= " . "'" . $date . "'");

        $productionByMonth = array();
//        and i.created_at >= " . "'" . $date . "'");
        $date = Carbon::now()->toDateString();
        for($i = 1; $i<= 12; $i++) {
            $data1= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot,oilwell_package op ,packages p
                      where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

            array_push($productionByMonth, array('infos' => $data1));
        }

        $investments = Investment::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        $productionDaly = $datas[0]->oliProduced + $datas[0]->gasProduced;
        $calc = $productionDaly - (($productionDaly / 100) * 20);
        $grasTotal = $calc * (40 - 3);
        $netTakeHome = $grasTotal - 10000;
        $investor = 75 *($netTakeHome / 100);

        $ROI = ($investor * 12 * $investments[0]->investmentYears / $investments[0]->investment) * 100;

        $settings = Settings::all();
        return view('dashboard.index', array('investments' => $investments, 'settings' => $settings,
            'productionByMonth' => $productionByMonth, 'ROI' => $ROI, 'data' => $data ));
    }

    protected function getdashboardByMonth(Request $request)
    {
        $selected  =  $request["selected"];

        if($selected == 1) {
            $oilwellproductionBy = array();
            for ($i = 1; $i <= 12; $i++) {
                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);


                array_push($oilwellproductionBy, array('infos' => $data));
            }
        } elseif ($selected == 2 ){
            $oilwellproductionBy = array();
            for ($i = 2012; $i <= 2017; $i++) {
                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and YEAR(i.created_at) =" . $i);

                array_push($oilwellproductionBy, array('infos' => $data));
            }
        }
//        elseif($selected == 0 )
//        {
//            $oilwellproductionBy = array();
//            for ($i = 2012; $i <= new DateTime('now'); $i++) {
//                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold',
//                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock',
//                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
//                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
//                  where u.id = ou.user_id
//                  and o.id = ou.oilwell_id
//                  and ot.tank_id = t.id
//                  and ou.oilwell_id = ot.oilwell_id
//                  and t.id = i.tankId
//                  and u.id =" . Auth::User()->id . " and YEAR(i.created_at) =" . $i);
//
//                array_push($oilwellproductionBy, array('infos' => $data));
//            }
//
//            $s = "sarajevo";
//
//
//        }

        return response()->json([
            'success' => 'Dashboard is Sorted!',
            'oilwellproductionBy' => $oilwellproductionBy

        ]);
//        return view('oilwells.oil-well-profile', array('oilwells' => $oilwells,'tankNumber' => $tankNumber, 'oilwellproductionByMonth' => $oilwellproductionBy));
    }

    protected function getdashboardByMonthSales(Request $request)
    {
        $selected  =  $request["selected"];

        if($selected == 1) {
            $oilwellproductionBy = array();
            for ($i = 1; $i <= 12; $i++) {
                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);


                array_push($oilwellproductionBy, array('infos' => $data));
            }
        } elseif($selected == 2 ){
            $oilwellproductionBy = array();
            for ($i = 2012; $i <= 2017; $i++) {
                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and YEAR(i.created_at) =" . $i);

                array_push($oilwellproductionBy, array('infos' => $data));
            }
        }
        return response()->json([
            'success' => 'Dashboard is Sorted!',
            'oilwellproductionBy' => $oilwellproductionBy

        ]);
//        return view('oilwells.oil-well-profile', array('oilwells' => $oilwells,'tankNumber' => $tankNumber, 'oilwellproductionByMonth' => $oilwellproductionBy));
    }

      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addOilWell() {
        $countryes = Country::all();
        $states = State::all();
        $oilwells = Oilwell::all();

        return view('oilwells.addOilWells', array('oilwells' => $oilwells,'countryes' => $countryes,'states' => $states ));
    }

    /**
     * Show the application dashboard.
     *
     * @param $oilWellid
     * @return \Illuminate\Http\Response
     */
    public function openOilWellTankForm($oilWellid) {

        return view('oilwells.addOilWellsTank', array('oilWellid' => $oilWellid));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexInputData() {
        $inputDatas = InputData::all();
        $oilWells = Oilwell::all();
        $oilWellsTanks = Tank::all();

        $oilSold = 0; $oillvl= 0;
        $gasSold = 0; $gaslvl= 0;
        foreach ($inputDatas as $inputData){
            $oillvl += $inputData->oilLvlFT;
            $gaslvl += $inputData->gaslvl;
                $oilSold += $inputData->oilSold;
                $gasSold += $inputData->gasSold;
        }

        return view('inputdata.index', array('inputDatas' => $inputDatas,'oilWells' => $oilWells,
            'oilSold' => $oilSold,'gasSold' => $gasSold,
            'oillvl' => $oillvl, 'gaslvl' => $gaslvl, 'oilWellsTanks' => $oilWellsTanks));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexReporting() {
        $package = array();
        $InputdataForTanks = array();

        $packages = Package::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        $InputdataForTanksMonths = array();
//        and i.created_at >= " . "'" . $date . "'");
          $date = Carbon::now()->toDateString();
          $data = DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and i.created_at >= " . "'" . $date . "'");

        $investments = Investment::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

          $productionDaly = $data[0]->oliProduced + $data[0]->gasProduced;
          $calc = $productionDaly - (($productionDaly / 100) * 20);
        $grasTotal = $calc * (40 + 3);
        $netTakeHome = $grasTotal - 10000;
        $investor = 75 *($netTakeHome / 100);

        $ROI = ($investor * 12 * $investments[0]->investmentYears / $investments[0]->investment) * 100;

//        for($i = 1; $i<= date('n'); $i++) {
            for($i = 1; $i<= 12; $i++) {
            $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

            array_push($InputdataForTanksMonths, array('infos' => $data));
        }

        if($packages->count() > 0) {
            foreach($packages as $package) {
                $oliSolde = 0;$gasSolde = 0;
                $oliProduced = 0;$gasProduced = 0;
                $oilStock = 0; $gasStock = 0;
                $totalOilProduction = 0; $totalGasProduction = 0;
                $totalOilSold = 0;$totalGasSold = 0;
                $totalOilStock = 0;$totalGasStock = 0;

                $oilwells = Oilwell::whereHas('package', function ($q) use ($package) {
                    $q->where('package_id', $package->id);
                })->get();

                foreach ($oilwells as $oilwell) {
                    $oilTanks = Tank::whereHas('oilwell', function ($q) use ($oilwell) {
                        $q->where('oilwell_id', $oilwell->id);
                    })->get();

                    foreach ($oilTanks as $oilTank) {
                        $inputdatas = InputData::where('tankId', $oilTank->id)->get();

                        foreach ($inputdatas as $inputdata) {
                            $oliSolde += $inputdata->oilSold;
                            $gasSolde += $inputdata->gasSold;
                            $oliProduced += $inputdata->oilLvlFT + $inputdata->oilSold;
                            $gasProduced += $inputdata->gaslvl + $inputdata->gasSold;
                            $oilStock += $inputdata->oilLvlFT;
                            $gasStock += $inputdata->gaslvl;
                        }
                    }
                    $totalOilProduction += $oliProduced;
                    $totalGasProduction += $gasProduced;
                    $totalOilSold += $oliSolde;
                    $totalGasSold += $gasSolde;
                    $totalOilStock += $oilStock;
                    $totalGasStock += $gasStock;

                }
                array_push($InputdataForTanks, array(
                    'packages' => $package,
                    'oilsolds' => $oliSolde, 'gasSoldes' => $gasSolde, 'oliProduced' => $oliProduced,
                    'gasProduced' => $gasProduced, 'oilStock' => $oilStock, 'gasStock' => $gasStock,
                    'totalOilProduction' => $totalOilProduction, 'totalGasProduction' => $totalGasProduction,
                    'totalOilSold' => $totalOilSold, 'totalGasSold' => $totalGasSold,
                    'totalOilStock' => $totalOilStock, 'totalGasStock' => $totalGasStock
                ));
            }

            $investments = Investment::whereHas('user', function ($q) {
                $q->where('user_id', Auth::User()->id);
            })->get();

                return view('reporting.index',array( 'InputdataForTanks' => $InputdataForTanks, 'InputdataForTanksMonths' => $InputdataForTanksMonths,'ROI' => $ROI, 'investments' => $investments ));

            } else {
            return view('reporting.indexNoPackages');
        }
    }

    /**
     * Show the indexComperativeAnalysis.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexComperativeAnalysis() {

        $packages = Package::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        $oilwells = Oilwell::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        return view('comperative.index', array('oilwells' => $oilwells, 'packages' => $packages));
    }

    /**
     * Show the indexComperativeAnalysisOnChange0.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function indexComperativeAnalysisOnChange0(Request $request) {

        if(isset($request['oilwell0']))
            return $this->calculationsForComparativ($request['oilwell0']);

        if(isset($request['oilwell1']))
            return $this->calculationsForComparativ($request['oilwell1']);

        if(isset($request['oilwell2']))
            return $this->calculationsForComparativ($request['oilwell2']);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculationsForComparativ($id) {


        $oilTanks = Tank::whereHas('oilwell', function ($q) use ($id){
            $q->where('oilwell_id', $id);
        })->get();

        foreach($oilTanks as $oilTank) {
            $inputdatas = InputData::where('tankId', $oilTank->id)->get();

            $oliSolde = 0 ;$gasSolde = 0;
            $oliProduced = 0;$gasProduced = 0;
            $oilStock = 0;$gasStock = 0;
            $totalOilProduction = 0;$totalGasProduction = 0;
            $totalOilSold = 0;$totalGasSold = 0;
            $totalOilStock = 0;$totalGasStock = 0;

            foreach($inputdatas as $inputdata) {
                $oliSolde += $inputdata->oilSold;
                $gasSolde += $inputdata->gasSold;
                $oliProduced += $inputdata->oilLvlFT + $inputdata->oilSold;
                $gasProduced += $inputdata->gaslvl + $inputdata->gasSold;
                $oilStock += $inputdata->oilLvlFT;
                $gasStock += $inputdata->gaslvl;
            }
        }
        $totalOilProduction += $oliProduced;
        $totalGasProduction += $gasProduced;
        $totalOilSold +=  $oliSolde;
        $totalGasSold +=  $gasSolde;
        $totalOilStock += $oilStock;
        $totalGasStock += $gasStock;


        $oilwellproductionByMonth = array();
//        and i.created_at >= " . "'" . $date . "'");
        $date = Carbon::now()->toDateString();
        for($i = 1; $i<= 12; $i++) {
            $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where  o.id = $id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ot.oilwell_id = $id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

            array_push($oilwellproductionByMonth, array('infos' => $data));
        }

        return response()->json([
            'success' => 'You added new Oil wells!',
            'status' => 'New oil wells added',
            'totalProduction' => ($totalOilProduction + $totalGasProduction) * 1.23 ,
            'totalSales' => ($totalOilSold + $totalGasSold) * 1.10,
            'totalOilProduction' => $totalOilProduction,
            'totalGasProduction' => $totalGasProduction,
            'totalOilSold' => $totalOilSold,
            'totalGasSold' => $totalGasSold,
            'totalOilStock' => $totalOilStock,
            'totalGasStock' => $totalGasStock,
            'oilwellproductionByMonth' => $oilwellproductionByMonth,
        ]);
    }

    /**
     * Show the indexHelp
     *
     * @return \Illuminate\Http\Response
     */
    public function indexHelp() {
        return view('help.index');
    }

    /**
     * Show the indexOilwells
     *
     * @return \Illuminate\Http\Response
     */
    public function indexOilwells() {
        $oilwells = Oilwell::all();
        $countryes = Country::all();
        $states = State::all();

        $oilWells = Oilwell::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        $inputdatas = array();
        foreach($oilWells as $oilWell){
            $data = null;
            $data= DB::select("select distinct o.*, COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced',
                                                    COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = $oilWell->id
                  and ot.tank_id = t.id
                  and ot.oilwell_id =$oilWell->id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id);

            array_push($inputdatas, $data);
        }


        return view('oilwells.index', array('oilwells' => $oilwells,'countryes' => $countryes,'states' => $states, 'inputdatas' => $inputdatas));
    }

    /**
     * Show the application show packages.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPackages() {
        $packages = Package::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        $oilWells = Oilwell::all();

        $inputdatas = array();

       foreach($packages as $package){

           $wellnumber = OilWell::whereHas('package', function ($q) use ($package) {
               $q->where('package_id', $package->id);
           })->count();


           $data = DB::select("select distinct  p.* , COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', 
                          COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot, oilwell_package op, packages p
                  where p.id = $package->id
                  and op.package_id = $package->id
                  and ot.tank_id = t.id
                  and op.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id);

            array_push($inputdatas, ['data' => $data ,'size' => $wellnumber] );
        }

        if(!isset($packages) && !isset($oilWells))
            return view('package.index');

        return view('package.index', array('packages' => $packages, 'oilWells' => $oilWells, 'inputdatas' => $inputdatas));
    }

    /**
     * validatorOilWells
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\
     */
    protected function validatorOilWells(array $data) {
        return \Illuminate\Support\Facades\Validator::make($data, [
            'name' => 'required|max:255|unique:oilwells',
            'country' => 'required|max:255',
            'state' => 'required|max:255',
            'active' => 'required|max:1'
        ]);
    }

    /**
     * Create a new createOilWells instance.
     *
     * @param  array $data
     * @param $image
     * @return OilWell
     */
    protected function createOilWells(array $data,  $image)
    {
        $oilWell =  Oilwell::create([
            'name' => $data['name'],
            'country' => $data['country'],
            'state' => $data['state'],
            'active' => $data['active'],
            'image' =>  $image,
            'thmb' => $image

        ]);

        $this->createNewTank2($data, $oilWell->id);

        if ($oilWell) {
            $oilWell->user()->attach(Auth::user()->id);
            //$oilWell->package()->attach($data['packageid']);
        }

        return $oilWell;
    }

    /**
     * api call. addOilWells
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function addOilWells(Request $request) {
        $validator = $this->validatorOilWells($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if ($request->hasFile('image')) {
            $rand = substr(md5(microtime()), rand(0, 26), 5);
            $imageName = $rand . '.' .
                $request->file('image')->getClientOriginalExtension();

            $image =  $request->file('image')->move(
                base_path() . '/public/images/oilwells/', $imageName
            );

          //  $image = $request->file('image');
            $img = Image::make($image->getRealPath());
            $img->resize(220, 150, function ($constraint) {
              //  $constraint->aspectRatio();
            })->save( base_path() . '/public/images/oilwells/resized/' . $imageName);

            $this->createOilWells($request->all(), $imageName);

        return response()->json([
                'success' => 'You added new Oil wells!',
                'status' => 'New oil wells added'
            ]
        );
       }

        return response()->json([
                'success' => 'You faied to add new Oil wells!',
                'status' => 'New oil wells is not added'
            ]
        );
    }

    /**
     * @param array $data
     * @param $id
     * @return static
     */
    protected function createNewTank2(array $data, $id) {
        foreach($data['oilWellTankId'] as $oilwell)  {
            $oilWellTank =  Tank::create([
                'tankId' => $oilwell,
                'active' => '1',
        ]);

        if ($oilWellTank) {

          $oilWellTank->oilWell()->attach($id);
//            foreach($data['WellId'] as $oilwell) {
//            $oilWellTank->oilwell()->attach($oilwell);
//            }
            }
        }
        return $oilWellTank;
    }

    /**
     * edit oil wells.
     *
     * @param $id
     * @return OilWell
     * @internal param array $data
     */
    public function editOilWells($id) {
        $oilwell = Oilwell::find($id);

        $inputdata = InputData::where('oilWellId', $id)->get();
        if(is_null($oilwell)){
            return Redirect::route('/');
        }

        $countryes = Country::all();
        $states = State::all();
        return view('oilwells.editOilWells',  array('oilwell' => $oilwell,'countryes' => $countryes,'states' => $states,'inputdata' => $inputdata));
    }

    /**
     * edit oil package.
     *
     * @param $id
     * @return Package
     * @internal param array $data
     */
    public function editPackage($id) {
        $package = Package::find($id);
        $oilWells = Oilwell::all();

        if(is_null($package)) {
            return Redirect::route('/');
        }
        return view('package.editPackage',  array('package' => $package, 'oilWells' => $oilWells));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOilWells(Request $request) {
        $id = $request['id'];

        if(isset($id)) {
            OilWell::find($id)->update([
                'name' => $request['name'],
                'country' => $request['country'],
                'state' => $request['state'],
                'active' => $request['active'],
                'image' => $request['image']
            ]);
        }
        return response()->json([
                'success' => 'You updated Oil wells!',
                'status' => 'Updated oil wells'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePackage(Request $request) {
        $id = $request['id'];
        $package = Package::find($id);

        if ($request->hasFile('images')) {
            $rand = substr(md5(microtime()), rand(0, 26), 5);
            $imageName = $rand . '.' .
                $request->file('images')->getClientOriginalExtension();

            $image =  $request->file('images')->move(
                base_path() . '/public/images/catalog/', $imageName
            );

            $img = Image::make($image->getRealPath());
            $img->resize(220, 150, function ($constraint) {
                //  $constraint->aspectRatio();
            })->save( base_path() . '/public/images/catalog/resized/' . $imageName);
        } else {
            $imageName  =  $request['imageID'];
        }

        if(isset($id)) {
            Package::find($id)->update([
                'name' => $request['name'],
                'image' => $imageName,
                'thmb' => $imageName
            ]);
        }

        if( $request['oilWellId'] != null) {
            foreach($request['oilWellId'] as $oilwell) {

                $exists = DB::table('oilwell_package')
                        ->where('oilwell_id', $oilwell)
                        ->where('package_id', $package->id)
                        ->count() > 0;

                if(!$exists)
                    $package->oilwell()->attach($oilwell);
            }
        }

        return response()->json([
                'success' => 'You updated Package!',
                'status' => 'Updated Package'
            ]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPackageView (\Symfony\Component\HttpFoundation\Request $request) {
        $oilWells = Oilwell::all();
        $packages = Package::whereHas('user', function ($q) {
            $q->where('user_id', Auth::User()->id);
        })->get();

        if(!isset($packages) && !isset($oilWells))
            return view('package.index');

        return view('package.addPackage', array('packages' => $packages, 'oilWells' => $oilWells));
    }

    /**
     * Show the application addPackage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function addPackage(Request $request) {

        if ($request->hasFile('image')) {
            $rand = substr(md5(microtime()), rand(0, 26), 5);
            $imageName = $rand . '.' .
                $request->file('image')->getClientOriginalExtension();

            $image = $request->file('image')->move(
                base_path() . '/public/images/catalog/', $imageName
            );

            $img = Image::make($image->getRealPath());
            $img->resize(220, 150, function ($constraint) {
                //  $constraint->aspectRatio();
            })->save( base_path() . '/public/images/catalog/resized/' . $imageName);

            $this->createPackage($request->all(), $imageName);

            return response()->json([
                    'success' => 'You added new Packege!',
                    'status' => 'New Package added!'
                ]
            );
        }

        return response()->json([
                'success' => 'Faild!',
                'status' => 'Faild!'
            ]
        );
    }

    /**
     * Create a new createPackage
     *
     * @param  array $data
     * @param $imageName
     * @return static $package
     */
    protected function createPackage(array $data, $imageName) {

        $package =  Package::create([
            'name' => $data['name'],
            'image' => $imageName,
            'thmb' => $imageName
        ]);

        if ($package) {
            $package->user()->attach(Auth::user()->id);

            foreach ($data['oilWellId'] as $oilwell) {

                $exists = DB::table('oilwell_package')
                        ->where('oilwell_id', $oilwell)
                        ->where('package_id', $package->id)
                        ->count() > 0;

                    if(!$exists)
                        $package->oilwell()->attach($oilwell);
            }
        }
        return $package;
    }

    /**
     * Create a new package instance after a valid registration.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View $package
     * @internal param array $data
     */
    protected function openPackage($id)
    {
        $countryes = Country::all();
        $states = State::all();
        $packageInforamtions = Package::where('id',$id)->get();

        $oilwells = Oilwell::whereHas('package', function ($q) use ($id) {
            $q->where('package_id', $id);
        })->get();

        $inputdatas = array();
        foreach($oilwells as $oilWell){
            $data = null;

            $data= DB::select("select distinct o.*, COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced',
                                                    COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where u.id = ou.user_id
                  and o.id = $oilWell->id
                  and ot.tank_id = t.id
                  and ot.oilwell_id =$oilWell->id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id);

//            $data= DB::select("select distinct o.*, COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced',
//                                                    COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', count(t.id) as 'tanks'
//                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
//                  where  o.id = $oilWell->id
//                  and o.id = ou.oilwell_id
//                  and ot.tank_id = t.id
//                  and ot.oilwell_id = $oilWell->id
//                  and t.id = i.tankId
//                  and u.id =" . Auth::User()->id );

            array_push($inputdatas, $data);
        }

        return view('package.OilWellsInPackage', array('oilwells' => $oilwells, 'countryes' => $countryes,
            'packageInforamtions' => $packageInforamtions,
            'states' => $states,
            'inputdatas' => $inputdatas,
            'packageid' => $id));
    }

    /**
     * Create a new package instance after a valid registration.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View $package
     * @internal param array $data
     */
    protected function openOilwellProfile($id)
    {
        $date = Carbon::now()->toDateString();

        $oilwelldata= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', count(t.id) as 'tanks'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where  o.id = $id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ot.oilwell_id = $id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id );

        $oilwellproductionByMonth = array();
//        and i.created_at >= " . "'" . $date . "'");
        $date = Carbon::now()->toDateString();
        for($i = 1; $i<= 12; $i++) {
            $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where  o.id = $id
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ot.oilwell_id = $id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

            array_push($oilwellproductionByMonth, array('infos' => $data));
        }

        $tankNumber = Tank::whereHas('oilwell', function ($q) use ($id) {
            $q->where('oilwell_id', $id);
        })->count();

        $oilwells = Oilwell::where('id', $id)->get();
        return view('oilwells.oil-well-profile', array('oilwells' => $oilwells,'tankNumber' => $tankNumber, 'oilwellproductionByMonth' => $oilwellproductionByMonth, 'oilwelldata' => $oilwelldata));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getOilWellByMonth(Request $request)
    {
      //  $request['paramName']
        $date = Carbon::now()->toDateString();
        $wellId   = $request["wellId"];
        $selected  =  $request["selected"];

        if($selected == 1) {
            $oilwellproductionBy = array();
            for ($i = 1; $i <= 12; $i++) {
                $data = DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where  o.id = $wellId
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ot.oilwell_id = $wellId
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

                array_push($oilwellproductionBy, array('infos' => $data));
            }
        } elseif($selected == 2 ){
            $oilwellproductionBy = array();
            for ($i = 2012; $i <= 2017; $i++) {
                $data = DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'year'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot
                  where  o.id = $wellId
                  and o.id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ot.oilwell_id = $wellId
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and YEAR(i.created_at) =" . $i);

                array_push($oilwellproductionBy, array('infos' => $data));
            }



        }

        $tankNumber = Tank::whereHas('oilwell', function ($q) use ($wellId) {
            $q->where('oilwell_id', $wellId);
        })->count();

        $oilwells = Oilwell::where('id', $wellId)->get();
        return response()->json([
            'success' => 'Sorted By!',
            'oilwells' => $oilwells,
            'tankNumber' => $tankNumber,
            'oilwellproductionBy' => $oilwellproductionBy

        ]);
//        return view('oilwells.oil-well-profile', array('oilwells' => $oilwells,'tankNumber' => $tankNumber, 'oilwellproductionByMonth' => $oilwellproductionBy));
    }

    /**
     * Create a new package instance after a valid registration.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View $package
     * @internal param array $data
     */
    protected function openPackageProfile($id)
    {
        $date = Carbon::now()->toDateString();

//        $tanknumber = Tank::whereHas('oilwell', function ($q) use ($id) {
//            $q->where('oilwell_id', $id);
//        })->count();

        $oilwelldataPackage= DB::select("select distinct  COALESCE(sum(i.oilLvlFT),0) as 'oilLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot, oilwell_package op ,packages p
                  where op.package_id = $id
                  and op.oilwell_id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id );
//            . "
//
////                  and i.created_at >= " . "'" . $date . "'");

        $packageproductionByMonth = array();
//        and i.created_at >= " . "'" . $date . "'");
        $date = Carbon::now()->toDateString();
        for($i = 1; $i<= 12; $i++) {
            $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot,oilwell_package op ,packages p
                  where op.package_id = $id
                  and op.oilwell_id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

            array_push($packageproductionByMonth, array('infos' => $data));
        }


        $oilwells = Package::where('id', $id)->get();

        $wells = OilWell::whereHas('package', function ($q) use ($id) {
            $q->where('package_id', $id);
        })->get();

        $tanks = 0;
        foreach ( $wells as $oilwell) {
            $tanks += Tank::whereHas('oilwell', function ($q) use ($oilwell) {
                $q->where('oilwell_id', $oilwell->id);
            })->count();
        }



        return view('package.package-profile', array('oilwells' => $oilwells, 'packageproductionByMonth' => $packageproductionByMonth,'tanks' => $tanks, 'oilwelldataPackage' => $oilwelldataPackage));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getPackageByMonth(Request $request)
    {
        //  $request['paramName']
        $date = Carbon::now()->toDateString();
        $packageId   = $request["packageId"];
        $selected  =  $request["selected"];

        if($selected == 1) {
            $oilwellproductionBy = array();
            for ($i = 1; $i <= 12; $i++) {
                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot,oilwell_package op ,packages p
                  where op.package_id = $packageId
                  and op.oilwell_id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and MONTH(i.created_at) =" . $i);

                array_push($oilwellproductionBy, array('infos' => $data));
            }
        } elseif($selected == 2 ){
            $oilwellproductionBy = array();
            for ($i = 2012; $i <= 2017; $i++) {
                $data= DB::select("select distinct i.tankId, COALESCE(sum(i.oilLvlFT),0) as 'gasLvlFT', COALESCE(sum(i.oilSold),0) as 'oilSold', 
                  COALESCE(sum(i.gaslvl),0) as 'gaslvl', COALESCE(sum(i.gasSold),0) as 'gasSold', COALESCE(sum(i.gasSold),0) as 'gasStock', COALESCE(sum(i.oilLvlFT),0) as 'oilStock', 
                  COALESCE(sum(i.oilLvlFT + i.oilSold),0) as 'oliProduced', COALESCE(sum(i.gaslvl + i.gasSold),0) as 'gasProduced', $i as 'month'
                  from users u, oilwells o, tanks t, inputdatas i, oilwell_user ou, oilwell_tank ot,oilwell_package op ,packages p
                  where op.package_id = $packageId
                  and op.oilwell_id = ou.oilwell_id
                  and ot.tank_id = t.id
                  and ou.oilwell_id = ot.oilwell_id
                  and t.id = i.tankId
                  and u.id =" . Auth::User()->id . " and YEAR(i.created_at) =" . $i);

                array_push($oilwellproductionBy, array('infos' => $data));
            }



        }

        $package = Package::where('id', $packageId)->get();

        $wells = OilWell::whereHas('package', function ($q) use ($packageId) {
            $q->where('package_id', $packageId);
        })->get();

        $tanks = 0;
        foreach ( $wells as $oilwell) {
            $tanks += Tank::whereHas('oilwell', function ($q) use ($oilwell) {
                $q->where('oilwell_id', $oilwell->id);
            })->count();
        }

        return response()->json([
            'success' => 'Package is Sorted!',
            'package' => $package,
            'wells' => $wells,
            'oilwellproductionBy' => $oilwellproductionBy

        ]);
//        return view('oilwells.oil-well-profile', array('oilwells' => $oilwells,'tankNumber' => $tankNumber, 'oilwellproductionByMonth' => $oilwellproductionBy));
    }

    /**
     * Create a openOilWellTanks
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View $package
     * @internal param array $data
     */
    protected function openOilWellTanks($id) {
        $countryes = Country::all();
        $states = State::all();

        $oilwellsPTanks = Tank::whereHas('oilWell', function ($q) use ($id){
            $q->where('oilwell_id', $id);
        })->get();

        return view('package.TanksInOilWell', array('oilwellsPTanks' => $oilwellsPTanks, 'countryes' => $countryes,'states' => $states,'oilwellid' => $id ));
    }

    /**
     * removefrompackage
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse $package
     * @internal param array $data
     */
    protected function removefrompackage($id) {
        $oilWell = Oilwell::whereHas('package', function ($q) use ($id) {
            $q->where('oilwell_id', $id);
        })->get();

        $oilWell->package()->detach(9);
        return response()->json([
                'success' => 'You added new Tank for this oil wells!',
                'status' => 'New tank for this oil wells is added'
            ]
        );
   }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tankDetails($id) {

        $tank = Tank::where('id', $id)->get();
        $inputdata = InputData::where('tankId', $tank[0]['tankId'])->get();

        return view('oilwells.tank-details', array( 'tank' => $tank, 'inputdata' => $inputdata));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewoilwellTank(Request $request) {

        $this->createNewTank($request->all());

        return response()->json([
                'success' => 'You added new Tank for this oil wells!',
                'status' => 'New tank for this oil wells is added',
                'oilWellid' =>  $request->oilWellid
            ]
        );
    }

    /**
     * Create a new Tank.
     *
     * @param  array  $data
     * @return createNewTank
     */
    protected function createNewTank(array $data) {

        $oilWellTank =  Tank::create([
            'tankId' => $data['tankId'],
            'active' => $data['active'],
        ]);

        if ($oilWellTank) {
            $oilWellTank->oilWell()->attach($data['oilWellid']);
        }
        return $oilWellTank;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deliteOilwells($id) {
        if (isset($id))
            OilWell::find($id)->delete();

        $oilwells = Oilwell::all();
        $countryes = Country::all();
        $states = State::all();
        return view('oilwells.index', array('oilwells' => $oilwells, 'countryes' => $countryes, 'states' => $states));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function removePackage($id)
    {        if ($id != null) {
            Package::find($id)->delete();

            return response()->json([
                'success' => 'You removed package!',
            ]);
        }
        return response()->json([
            'error' => 'Error while removing package!',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorInputData(array $data)
    {
        return \Illuminate\Support\Facades\Validator::make($data, [
            'active' => 'required|max:255',
            'dateDay' => 'required|max:255',
            'dateMonth' => 'required|max:255',
            'dateYear' => 'required|max:255',
            'gaslvl' => 'required|max:255',
            'gasSold' => 'required|max:255',
            'note' => 'required|max:255',
            'oilLvlFT' => 'required|max:255',
//            'oilLvlIN' => 'required|max:255',
            'oilSold' => 'required|max:255',
            'waterlvl' => 'required|max:255',
//            'oilWellId' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function createInputData(array $data)
    {
        $date =  $data['dateDay'] . "-" .  $data['dateMonth'] . "-" . $data['dateYear'];
        $inputData =  InputData::create([
            'active' => $data['active'],
            'date' => $date,
            'gaslvl' => $data['gaslvl'],
            'gasSold' => $data['gasSold'],
            'note' => $data['note'],
            'oilLvlFT' => $data['oilLvlFT'],
//            'oilLvlIN' => $data['oilLvlIN'],
            'oilSold' => $data['oilSold'],
            'waterlvl' => $data['waterlvl'],
            'tankId' => $data['tankId'],
            'oilWellId' => 0,
        ]);

//        if ($inputData) {
//            $inputData->user()->attach(Auth::user()->id);
//        }
        return $inputData;
    }

    /** addInputData
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addInputData(Request $request)
    {
        $this->createInputData($request->all());

        return response()->json(
            [
                'success' => 'You added new input data!',
                'status' => 'New input data'
            ]
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editInputData($id)
    {
        $inputdata = InputData::find($id);
        if(is_null($inputdata)){
            return Redirect::route('/input-data');
        }
        $oilWellsTanks = Tank::all();
        return view('inputdata.edit',  array('oilWellsTanks' => $oilWellsTanks, 'inputdata' => $inputdata));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateInputData(Request $request)
    {
        $id = $request['id'];
        if(isset($id)) {
            InputData::find($id)->update([

                'active' => $request['active'],
                'date' => $request['date'],
                'gaslvl' => $request['gaslvl'],
                'gasSold' => $request['gasSold'],
                'note' => $request['note'],
                'oilLvlFT' => $request['oilLvlFT'],
//            'oilLvlIN' => $request['oilLvlIN'],
                'oilSold' => $request['oilSold'],
                'waterlvl' => $request['waterlvl'],
//                'tankId' => $request['tankId'],
                'oilWellId' => 0,
            ]);
        }
        return response()->json([
                'success' => 'You updated input data!',
                'status' => 'Updated input ata'
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function page404()
    {
        return view('errors.404');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function page500()
    {
        return view('errors.500');
    }
}
