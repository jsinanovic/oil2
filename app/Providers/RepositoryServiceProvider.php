<?php
/**
 * Created by PhpStorm.
 * User: x2-ja
 * Date: 11/30/2016
 * Time: 12:12 PM
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(
            'App\Contracts\UserRepositoryInterface', 'App\Repositories\UserRepository'
        );
    }
}