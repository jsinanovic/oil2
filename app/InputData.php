<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inputdata extends Model
{

    protected $fillable = [
        'date',
        'tankId',
        'oilLvlFT',
        'oilLvlIN',
        'oilSold',
        'gaslvl',
        'gasSold',
        'waterlvl',
        'note',
        'active',
        'oilWellId'

    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
           ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function oilwell()
    {
        return $this->belongsToMany('App\OilWell');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tank()
    {
        return $this->belongsToMany('App\Tank');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\Tank');
    }

}
