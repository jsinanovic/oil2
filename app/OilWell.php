<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Oilwell extends Model
{
    protected $fillable = [
        'name',
        'country',
        'state',
        'active',
        'image',
        'thmb',
        'packageid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public function state()
    {
        return $this->belongsToMany('App\State');
    }

    public function country()
    {
        return $this->belongsToMany('App\Country');
    }

    public function package()
    {
        return $this->belongsToMany('App\Package');
    }

    public function tank()
    {
        return $this->belongsToMany('App\Tank');
    }

}
