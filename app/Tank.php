<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tank extends Model
{

    protected $fillable = [
        'tankId',
        'active'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function oilWell()
    {
        return $this->belongsToMany('App\Oilwell');
    }

}
