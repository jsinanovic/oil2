<?php

namespace App\Contracts;
/**
 * Created by PhpStorm.
 * User: x2-ja
 * Date: 11/30/2016
 * Time: 11:54 AM
 */
interface UserRepositoryInterface
{
    public function isRole();
}