<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Package extends Model
{
    protected $fillable = [
        'name',
        'image',
        'thmb'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function oilwell()
    {
        return $this->belongsToMany('App\Oilwell');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function oilwellcount($id)
    {
        return ;
    }

}
