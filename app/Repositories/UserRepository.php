<?php

namespace App\Repositories;

use App\Contracts\UserRepositoryInterface;
use App\User;
/**
 * Created by PhpStorm.
 * User: x2-ja
 * Date: 11/30/2016
 * Time: 11:56 AM
 */
class UserRepository implements UserRepositoryInterface
{
    private $name;

    /**
 *
 * Return role name
 *
 * @return string
 */
    public function isRole()
    {
        $role = Auth::user()->role()->first();
        return ($role) ? $role->name : false;
    }

    /**
     *
     * Return role name
     *
     * @return string
     */
    public function getRole()
    {
        $role = Auth::user()->role()->first();
        return ($role) ? $role->name : false;
    }

    /**
     * @return mixed
     */
    public function notifications()
    {
        return Notification::UnreadMessagesForUser(Auth::id())->get();
    }
}